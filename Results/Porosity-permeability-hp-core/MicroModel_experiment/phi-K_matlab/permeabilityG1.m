clear;
% Berechnung permeability(porosity(time))
load ('1MM.mat');

for i=3:length(FlowPorosity)
    
    KozCarAno(i-2) = (FlowPorosity(i)/FlowPorosity(3))^3;
    KozCarAno100(i-2) = (FlowPorosity(i)/FlowPorosity(3))^100;
    
%Variieren des Exponenten
    KozCarAno3(i-2) = (FlowPorosity(i)/FlowPorosity(3))^3.8; % in Anlehnung an CARLOS F. JOVE COLON (2003)
    KozCarAno10(i-2) = (FlowPorosity(i)/FlowPorosity(3))^10;
    KozCarAno30(i-2) = (FlowPorosity(i)/FlowPorosity(3))^30;
    KozCarAno60(i-2) = (FlowPorosity(i)/FlowPorosity(3))^60;
    KozCarAno90(i-2) = (FlowPorosity(i)/FlowPorosity(3))^90;
    KozCarAno120(i-2) = (FlowPorosity(i)/FlowPorosity(3))^120;
    
    KozCarAno60(i-2) = (FlowPorosity(i)/FlowPorosity(3))^60;
    KozCarAno70(i-2) = (FlowPorosity(i)/FlowPorosity(3))^70;
    KozCarAno80(i-2) = (FlowPorosity(i)/FlowPorosity(3))^80;
    KozCarAno90(i-2) = (FlowPorosity(i)/FlowPorosity(3))^90;
    
% Kozeny-Carman + Biofilm
    extKC(i-2) = KozCarAno(i-2)*exp(-80*Biofilm(i)/VoidVolume(3));
    KozCarAno2(i-2) = ( (FlowPorosity(i)/FlowPorosity(3))^3*((1-FlowPorosity(3))/(1-FlowPorosity(i))))^2;

        
end


figure;
h1_plot = plot(TimePerm, kk0, 'b+', TimePerm, KozCarAno, 'r*', TimePerm, KozCarAno100, 'g+', TimePerm, extKC, 'k+-.',TimePerm, KozCarAno2, 'm.');
h1_leg = legend('K/K_0 calculated from pressure drop','K/K_0 calculated with K-C like Anozie','K/K_0, K-C exponent 3.8','K/K_0, Anozies K-C*exp(-80*Bio/Void)', 'K/K0 from KC2', 1);
h1_xlab = xlabel('Time [h]');
h1_ylab = ylabel('Permeability factor [-]');
h1_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);

set(gca,'FontSize',10)
set(h1_leg,'FontSize',10)
set(h1_xlab,'FontSize',10)
set(h1_ylab,'FontSize',10)
set(h1_plot,'LineWidth',2);

figure;
h2_plot = plot(TimePerm, kk0, 'r*',TimePerm, extKC, 'k+-', TimePerm, KozCarAno, 'g.', TimePerm, KozCarAno3, 'b.',TimePerm, KozCarAno30, 'y+' ,TimePerm, KozCarAno60, 'g+',TimePerm, KozCarAno90, 'm.',TimePerm, KozCarAno120, 'k.');
h2_leg = legend('K/K_0 calculated from pressure drop','K/K_0 extended','K/K_0, exponent 3','K/K_0 exponent 3.8', 'K/K_0 exponent 30', 'K/K_0 exponent 60', 'K/K_0 exponent 90','K/K_0 exponent 120' );
h2_xlab = xlabel('Time [h]');
h2_ylab = ylabel('Permeability factor [-]');
h2_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);

figure;
h3_plot = plot(TimePoro, FlowPorosity, 'r*');
h3_leg = legend('Porosity');
h3_xlab = xlabel('Time [h]');
h3_ylab = ylabel(' [-]');
h3_title = title('Micro model: Porosity development');
%axis([0 0.61 0 0.30]);

figure;
h2_plot = plot(TimePerm, kk0, 'r*',TimePerm, extKC, 'k+-', TimePerm, KozCarAno60, 'm.', TimePerm, KozCarAno70, 'b.', TimePerm, KozCarAno80, 'g.', TimePerm, KozCarAno90, 'c.');
h2_leg = legend('K/K_0 calculated from pressure drop','K/K_0 extended','K/K_0, exponent 60','K/K_0 exponent 70', 'K/K_0 exponent 80', 'K/K_0 exponent 90');
h2_xlab = xlabel('Time [h]');
h2_ylab = ylabel('Permeability factor [-]');
h2_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);
