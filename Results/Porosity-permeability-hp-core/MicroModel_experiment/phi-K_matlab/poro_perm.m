clear;
% Ver�nderung der x-Achse: Zeitunabh�ngige Darstellung permeability-porosity:
% nn0_tot: TotalPorosity/TotalPorosity(t=0), gesamte Flie�zelle
% nn0_flow: FlowPorosity/FlowPorosity(t=0): ohne Aus- und Einlauf; Durch anf�ngliches Absterben von Biofilm entsteht ein Verh�ltnis > 100% 

load ('MM2.mat');

for i=3:length(FlowPorosity)
    
    KozCarAno(i-2) = (FlowPorosity(i)/FlowPorosity(3))^3;
    KozCarAno100(i-2) = (FlowPorosity(i)/FlowPorosity(3))^100;
    
    KozCarAnoT(i-2)= (TotalPorosity(i)/TotalPorosity(3))^3;
    KozCarAnoT100(i-2)= (TotalPorosity(i)/TotalPorosity(3))^100;
    
    extKC(i-2) = KozCarAno(i-2)*exp(-80*Biofilm(i)/VoidVolume(3));
    extKCT(i-2) = KozCarAnoT(i-2)*exp(-80*Biofilm(i)/VoidVolume(3));
end


figure;
%  Vergleich der Darstellung Flow Porosity, Total Porosity
h1_plot = plot(nn0_flow, kk0, 'b+', nn0_tot, kk0, 'm+');
h1_leg = legend('K/K_0 calculated from pressure drop over FlowPorosity','K/K_0 calculated from pressure drop over TotalPorosity',1);
h1_xlab = xlabel('Porosity n/n0 [-]');
h1_ylab = ylabel('Permeability factor K/K0[-]');
h1_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);

set(gca,'FontSize',10)
set(h1_leg,'FontSize',10)
set(h1_xlab,'FontSize',10)
set(h1_ylab,'FontSize',10)
set(h1_plot,'LineWidth',2);

figure; 
% Darstellung Permeability-Flow Porosity
h2_plot = plot( nn0_flow, kk0, 'b+', nn0_flow, KozCarAno, 'm.', nn0_flow, KozCarAno100, 'g.', nn0_flow, extKC, 'b.');
h2_leg = legend('K/K_0 calculated from pressure drop over FlowPorosity','Kozeny-Carmen: Exponent 3','Kozeny-Carmen: Exponent 100', 'Anozies K-C*exp(-80*Bio/Void)', 1);
h2_xlab = xlabel('Flow Porosity n/n0 [-]');
h2_ylab = ylabel('Permeability factor K/K0[-]');
h2_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);


figure;
% Darstellung Permeability- Total Porosity
h1_plot = plot (nn0_tot, kk0, 'b+', nn0_tot, KozCarAnoT, 'm.', nn0_tot, KozCarAnoT100, 'g.', nn0_tot, extKCT, 'b.');
h1_leg = legend('K/K_0 calculated from pressure drop : FlowPorosity','Kozeny-Carmen: Exponent 3','Kozeny-Carmen: Exponent 100', 'Anozies K-C*exp(-80*Bio/Void)', 1);
h1_xlab = xlabel('Total Porosity n/n0 [-]');
h1_ylab = ylabel('Permeability factor K/K0[-]');
h1_title = title('Micro model: Permeability factors calculated from pressure drop and porosity');
%axis([0 0.61 0 0.30]);