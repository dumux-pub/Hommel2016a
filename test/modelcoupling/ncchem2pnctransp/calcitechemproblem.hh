// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License. or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BIOCHEM_PROBLEM_HH
#define DUMUX_BIOCHEM_PROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfalu.hh>

#include<dumux/material/fluidsystems/calcitefluidsystem.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
//#include <dumux/implicit/2pncmin/2pncminmodel.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pcalcitechemmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominfluxvariables.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pbiochemlocalresidual.hh>
#include <dumux/implicit/2pco2calcite/2pallincvolumevariables.hh>
#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include <dumux/material/chemistry/geochemistry/testcarbonicacidvarsalinity.hh>

//#include "biospatialparams.hh"
#include "biospatialparams.hh"
#include "bioco2tables.hh"

#include "dumux/linear/seqsolverbackend.hh"


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class CalciteChemProblem;

namespace Properties
{
NEW_TYPE_TAG(CalciteChemProblem, INHERITS_FROM(BoxTwoPNCMin, BioMinSpatialParams));
//NEW_TYPE_TAG(CalciteChemProblem, INHERITS_FROM(BoxTwoBioMin, BioMinSpatialParams));

// Set the grid type
SET_PROP(CalciteChemProblem, Grid)
{
    typedef Dune::UGGrid<2> type;
//   typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(CalciteChemProblem, Problem)
{
    typedef Dumux::CalciteChemProblem<TTAG(CalciteChemProblem)> type;
};

SET_PROP(CalciteChemProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::CalciteFluid<Scalar, CO2Tables, H2O_Tabulated, useComplexRelations> type;
};

//! the BioMin Flux VolumeVariables and LocalResidual properties

SET_TYPE_PROP(CalciteChemProblem, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);
SET_TYPE_PROP(CalciteChemProblem, VolumeVariables, TwoPAllIncVolumeVariables<TypeTag>);
SET_TYPE_PROP(CalciteChemProblem, LocalResidual, TwoPBioChemLocalResidual<TypeTag>);
SET_TYPE_PROP(CalciteChemProblem, Model, TwoPCalciteChemModel<TypeTag>);

//SET_TYPE_PROP(CalciteChemProblem, LinearSolver, SuperLUBackend<TypeTag>);

SET_PROP(CalciteChemProblem, Chemistry)
{
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::CarbonicAcidVarSalinity<TypeTag, CO2Tables> type;

};

// Set the spatial parameters
SET_TYPE_PROP(CalciteChemProblem,
              SpatialParams,
              Dumux::BioMinSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(CalciteChemProblem, ProblemEnableGravity, false);             //TODO

// Write newton convergence
SET_BOOL_PROP(CalciteChemProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(CalciteChemProblem, ImplicitEnablePartialReassemble, false);

//SET_SCALAR_PROP(CalciteChemProblem, LinearSolverResidualReduction, 1e-4);//1e-12);

}

/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(CalciteChemProblem) >
class CalciteChemProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef CalciteChemProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numComponents = FluidSystem::numComponents,
        numSecComponents = FluidSystem::numSecComponents,
        numSPhases = FluidSystem::numSPhases,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xlNaIdx = FluidSystem::NaIdx,
        xlClIdx = FluidSystem::ClIdx,
        xlCaIdx = FluidSystem::CaIdx,
        phiCalciteIdx = numComponents,//FluidSystem::CalciteIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        OHIdx = FluidSystem::OHIdx,

        CalciteIdx = FluidSystem::CalciteIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
   // typedef Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

     typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, Dumux::BioMin::CO2Tables, true> Brine_CO2;

//    typedef Dune::FieldVector<Scalar, numComponents + numSecComponents> CompVector;

public:
    CalciteChemProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        FluidSystem::init();
        try
        {

            //initial values
            densityW_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initDensityW);
            initPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPressure);

            initxlTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTC);
            initxlNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlNa);
            initxlCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCl);
            initxlCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCa);
            initCalcite_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initCalcite);

            xlNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlNaCorr);
            xlClCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlClCorr);

            timeIntegrationIdx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TimeIntegrationIdx);

            numOutputSteps_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, numOutputSteps);
            tEpisode_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,   Scalar, TimeManager, TEnd);
            tEpisode_ /= numOutputSteps_;
            }


    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }

        FluidSystem::init();
        this->timeManager().startNextEpisode(tEpisode_);            //one episode = 2hours
    }
    /*!
      * \brief Returns true if a output file should be written to
      *        disk.
      *
      * The default behaviour is to write one output file every time
      * step.
      */
    bool shouldWriteOutput() const
        {
            return
//                  this->timeManager().timeStepIndex() % 1 == 0 ||         //output every timestep
//                  this->timeManager().timeStepIndex() % 1000 == 0 ||          //output every 1000 timesteps
//              this->timeManager().timeStepIndex() == 0 ||
//              this->timeManager().episodeWillBeOver() ||
//              this->timeManager().willBeFinished();

                this->timeManager().episodeWillBeOver();
        }

    bool shouldWriteRestartFile() const
    {
        return false;
//                  this->timeManager().timeStepIndex() % 1 == 0 ||         // every timestep
//              this->timeManager().timeStepIndex() % 1000 == 0 ||          // every 1000 timesteps
//          this->timeManager().episodeWillBeOver();
    }

    void preTimeStep()
    {
       previousTimeStepSize = this->timeManager().previousTimeStepSize();
    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    { return "CalciteChem"; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 25; // -> 25 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
//        const GlobalPosition globalPos = vertex.geometry().center();
//        Scalar xmax = this->bBoxMax()[0];
        values.setAllNeumann();

    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
            initial_(values,globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                              const Element &element,
                              const FVElementGeometry &fvGeometry,
                              const Intersection &is,
                              int scvIdx,
                              int boundaryFaceIdx) const
    {
            values = 0.0; //mol/m²/s
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &q,
              const Element &element,
              const FVElementGeometry &fvGeometry,
              int scvIdx) const
    {
        q = 0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void reactionSource(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                const VolumeVariables &volVars,
                const Scalar dummy) const
    {

        q = 0;

        Chemistry chemistry;
        chemistry.reactionSource(q,
                        volVars);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
//      return bothPhases;
    }

    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        //if(this->timeManager().episodeIndex() ==1 )

     this->timeManager().startNextEpisode(tEpisode_);
     //     this->timeManager().setTimeStepSize(1.0);
     this->timeManager().setTimeStepSize(this->timeManager().previousTimeStepSize());
    }

//    void calculatePorosity()
//    {
//      std::cout<<"Enter cal porosity!.........."<<std::endl;
//      FVElementGeometry fvGeometry;
//      ElementIterator elemIt = this->gridView_().template begin<0>();
//      ElementIterator elemEndIt = this->gridView_().template end<0>();
//
//      for (; elemIt != elemEndIt; ++elemIt)
//      {
//        fvGeometry.update(this->gridView_(), *elemIt);
//
//        int numVerts = elemIt->template count<dim> ();
//
//        for (int i = 0; i< numVerts; ++i)
//        {
//            int globalIdx = this->vertexMapper().map(*elemIt, i, dim);
//
//            std::cout<<"global idx %d"<<globalIdx<<std::endl;
//        }
//      }
//
//    }

    /*!
     * \brief Return the number of components of the system.
     */
    int getNumComponents()
    {
        return numComponents;
    }

    /*!
     * \brief Return the number of solid phases of the system.
     */
    int getNumSPhases()
    {
        return numSPhases;
    }

    Scalar getPreviousTimeStepSize()
    {
        return previousTimeStepSize;
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[1];
        values[pressureIdx] = initPressure_ ; //70e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        values[switchIdx] = initxlTC_;
        values[xlNaIdx] = initxlNa_ + xlNaCorr_;
        values[xlClIdx] = initxlCl_ + xlClCorr_;
        values[xlCaIdx] = initxlCa_;
        values[phiCalciteIdx] = initCalcite_;//0.00000; // [m^3/m^3]

//        std::cout<<"initial values chemistry = "<<values<<std::endl;

#if !ISOTHERMAL
        values[temperatureIdx] = 283.0 + (depthBOR_ - globalPos[1])*0.03;
#endif

    }

    static constexpr Scalar eps_ = 1e-6;
//    static constexpr Scalar densityW_ = 1087; //kg/m3// rhow=1087;
//    static constexpr Scalar initPressure_ = 70e5; //[Pa]
//
//    static constexpr Scalar initxlTC_ = 2.3864e-7;        // [mol/mol]
//    static constexpr Scalar initxlNa_ = 0;
//    static constexpr Scalar initxlCl_ = 0;
//    static constexpr Scalar initxlCa_ = 0;
//    static constexpr Scalar initxlUrea_ = 0;
//    static constexpr Scalar initxlTNH_ = 3.341641e-3;
//    static constexpr Scalar initxlO2_ = 4.4686e-6;
//    static constexpr Scalar initxlBiosub_ = 2.97638e-4;
//    static constexpr Scalar initxlBiosusp_ = 0;
//    static constexpr Scalar xlNaCorr_ = 2.9466e-6;
//    static constexpr Scalar xlClCorr_ = 0;
//
//    static constexpr Scalar injTC_ = 5.8e-7;              // [kg/kg]
//    static constexpr Scalar injNa_ = 0.00379;             // [kg/m³]
//    static constexpr Scalar injCa_ = 13.3593333333;           // [kg/m³]      //computed from 0.333mol/l CaCl2
//    static constexpr Scalar injUrea_ = 20;                    // [kg/m³]
//    static constexpr Scalar injTNH_ = 3.183840574;//3.184;                // [kg/m³]      //computed from 10 g/l NH4Cl
//    static constexpr Scalar injO2_ = 0.008;                   // [kg/m³]
//    static constexpr Scalar injSub_ = 3;                  // [kg/m³]
//    static constexpr Scalar injBiosusp_ = 0.0675;         // [kg/m³]      //2.7e8 cfu/ml (40e8cfu/ml~1g/l)


    Scalar initPressure_;
    Scalar densityW_;//1087; // rhow=1087;

    Scalar initxlTC_= 0.;//2.3864e-7;       // [mol/mol]
    Scalar initxlNa_= 0.;//0;
    Scalar initxlCl_= 0.;//0;
    Scalar initxlCa_= 0.;//0;
    Scalar xlNaCorr_= 0.;//2.9466e-6;
    Scalar xlClCorr_= 0.;//0;
    Scalar initCalcite_= 0.;

    int timeIntegrationIdx_;

    Scalar tEpisode_;
    Scalar numOutputSteps_;
    Scalar previousTimeStepSize;
    Scalar minRelativeRate;
};
} //end namespace

#endif
