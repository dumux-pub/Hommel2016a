// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_BIOCHEM_2PTRANSP_PROBLEM_HH
#define DUMUX_BIOCHEM_2PTRANSP_PROBLEM_HH

#include <dumux/modelcoupling/common/simplecoupledproblem.hh>
//#include <dumux-devel/appl/co2/biomin/biominproblem.hh>
//#include <dumux-devel/appl/co2/biomin/biochemproblem.hh>

//#include "gorgastranspproblem2D.hh"
#include "gorgastranspproblem_2_28.hh"
//#include "gorgastranspproblem.hh"
#include "biochemproblem.hh"

namespace Dumux
{
template <class TypeTag>
class BioChem2PTranspProblem;

namespace Properties
{
NEW_TYPE_TAG(BioChem2PTranspProblem, INHERITS_FROM(SimpleCoupled));

// Set the problem property
SET_TYPE_PROP(BioChem2PTranspProblem, Problem,
              Dumux::BioChem2PTranspProblem<TypeTag>);

// Set the two sub-problems of the global problem
//SET_TYPE_PROP(BioChem2PTranspProblem, SubProblem1TypeTag, TTAG(BioTranspProblem));
//SET_TYPE_PROP(BioChem2PTranspProblem, SubProblem2TypeTag, TTAG(BioChemProblem));
SET_TYPE_PROP(BioChem2PTranspProblem, SubProblem1TypeTag, TTAG(BioChemProblem));
SET_TYPE_PROP(BioChem2PTranspProblem, SubProblem2TypeTag, TTAG(BioTranspProblem));

SET_PROP(BioTranspProblem, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(BioChem2PTranspProblem), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};

SET_PROP(BioChemProblem, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(BioChem2PTranspProblem), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};
}

template <class TypeTag>
class BioChem2PTranspProblem : public SimpleCoupledProblem<TypeTag>
{
    typedef SimpleCoupledProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // obtain the type tags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, Problem) BioChemProblem;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Problem) BioTranspProblem;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

public:
    BioChem2PTranspProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {

        try
        {
            name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                 std::string,
                                                 Problem,
                                                 Name);
            maxCouplingError_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                            Scalar,
                                                            Problem,
                                                            MaxCouplingError);
            timeIntegrationIdx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                                    Scalar,
                                                                    Problem,
                                                                    TimeIntegrationIdx);
        }

        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
//        switch (timeIntegrationIdx_)
//        {
        if (timeIntegrationIdx_ == 1)
//      case timeIntegrationIdx_ == 1:
        {
            sequentialIterative_ = true;
            std::cout<<"sequential iterative time integration selected. This will take forever! \n"<<std::endl;
        }
        else if (timeIntegrationIdx_ == 2)
//      case timeIntegrationIdx_ == 2:
        {
            sequentialNonIterative_ = true;
            std::cout<<"sequential non iterative time integration selected. This is might be inaccurate! \n"<<std::endl;
        }
        else if (timeIntegrationIdx_ == 2012)
//      case timeIntegrationIdx_ == 2:
        {
            sequentialNonIterativeChemSourceFTransp_ = true;
            std::cout<<"sequential non iterative time integration with chemistry as a source for transport selected. \n"<<std::endl;
        }
        else if (timeIntegrationIdx_ == 212)
//      case timeIntegrationIdx_ == 2:
        {
            sequentialNonIterativeChemFirst_ = true;
            std::cout<<"sequential non iterative time integration with chemistry being calculated before transport selected. This is might be inaccurate! \n"<<std::endl;
        }
        else if (timeIntegrationIdx_ == 221)
//      case timeIntegrationIdx_ == 2:
        {
            sequentialNonIterativeTranspFirst_ = true;
            std::cout<<"sequential non iterative time integration with transport being calculated before chemistry selected. This is might be inaccurate! \n"<<std::endl;
        }
        else if (timeIntegrationIdx_ == 99)
//      case timeIntegrationIdx_ == 99:
        {
            plainDecoupled_ = true;
            std::cout<<"plain decoupled time integration selected. This is boring!!! \n"<<std::endl;

        }
//        break;
//        default:
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid TimeIntegrationIdx: " << timeIntegrationIdx_<<"\n Check input file!");
//      break;
//        }

        uCur_ = ChemProblem().model().curSol();
        uPrev_ = uCur_;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }
    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    {       //todo
        uPrevDt_ =  uCur_;
//      std::cout<<"PRETIMESTEP timestepsize: "<<ParentType::timeManager_.timeStepSize()<<std::endl;
        if (ParentType::timeManager_.timeStepIndex()>1)
        {
            std::cout<<"test # preTimeTtep \n"<<std::endl;
            if(sequentialNonIterativeChemSourceFTransp_ || sequentialNonIterative_ || plainDecoupled_||sequentialNonIterativeTranspFirst_||sequentialNonIterativeChemFirst_)
                timeStepSizeFactor_ = std::min(maxCouplingError_/(couplingError_+eps_), 2.);
            else if(sequentialIterative_)
                timeStepSizeFactor_ = std::min(maxNoIterations_/(NoIterations_+1), 2);
            else
                timeStepSizeFactor_ = 1;
//          if(sequentialNonIterativeChemSourceFTransp_ || sequentialNonIterative_ || plainDecoupled_ || sequentialIterative_)
//          {
                ParentType::timeManager_.setTimeStepSize(ParentType::timeManager_.timeStepSize()*timeStepSizeFactor_);
                std::cout<<"PRETIMESTEP timestepsize factor: "<< timeStepSizeFactor_ <<std::endl;
//          }

//          else if (sequentialNonIterativeChemFirst_)
//          {
//              ParentType::timeManager_.setTimeStepSize(ParentType::subTimeManager1_.previousTimeStepSize());
//              std::cout<<"PRETIMESTEP timestepsize chemistry: "<< ParentType::subTimeManager1_.previousTimeStepSize() <<std::endl;
//          }
//          else if (sequentialNonIterativeTranspFirst_)
//          {
//              ParentType::timeManager_.setTimeStepSize(ParentType::subTimeManager2_.previousTimeStepSize());
//              std::cout<<"PRETIMESTEP timestepsize transport: "<< ParentType::subTimeManager2_.previousTimeStepSize() <<std::endl;
//          }

        }
        if (ParentType::timeManager_.timeStepSize()>10000)
        {
              ParentType::timeManager_.setTimeStepSize(10000);
        }
        std::cout<<"PRETIMESTEP timestepsize: "<<ParentType::timeManager_.timeStepSize()<<std::endl;

        if (ParentType::timeManager_.timeStepSize()<10)
        {
              ParentType::timeManager_.setTimeStepSize(10);
        std::cout<<"PRETIMESTEP timestepsize: "<<ParentType::timeManager_.timeStepSize()<<std::endl;
    }


//      qReactPrev_ = qReactCur_;
//      qTranspPrev_ = qTranspCur_;
    }

        /*!
     * \brief Called by Dumux::TimeManager in order to do a time
     *        integration on the model.
     */
    void timeIntegration()
    {
        std::cout << "coupled timeIntegration t = " << ParentType::timeManager_.time() << std::endl;
        std::cout<<"test timeIntegration \n"<<std::endl;

        //choose the timeIntegration scheme *************************************************************
        if(sequentialIterative_)
        {
            sequentialIterative();

            int NoTimeStepReductions = 1;
            //if the coupling error is too big, the timestep size is reduced and timeintegration is retried.
            if(couplingError_>maxCouplingError_)
            while (NoTimeStepReductions<10)
            {
                if (NoIterations_ == 2*maxNoIterations_)
                {
                    ChemProblem().model().curSol() = uPrevDt_;
                    TranspProblem().model().curSol() = uPrevDt_;

                    ParentType::timeManager_.setTimeStepSize(0.5*ParentType::timeManager_.timeStepSize());
                    sequentialIterative();
                }
                ++NoTimeStepReductions;
            }
        }
        else if(sequentialNonIterative_)
            sequentialNonIterativeChemistryIsSourceForTransport();

        else if(sequentialNonIterativeChemSourceFTransp_)
            sequentialNonIterativeChemistryIsSourceForTransport();

        else if(sequentialNonIterativeChemFirst_)
            sequentialNonIterativeChemistryFirst();

        else if(sequentialNonIterativeTranspFirst_)
            sequentialNonIterativeTransportFirst();

        else if(plainDecoupled_)
        {
//          qReactCur_ = uCur_;
//          qReactCur_ -= uCur_;
//          TranspProblem().reactiveSource() = qReactCur_;  //qReactCur_ is set to zero during initialisation
            std::cout<<"test # timeIntegration, plainDecoupled_ # 1"<<std::endl;
//          ParentType::timeIntegration();
            runChemistry();
            std::cout<<"test # timeIntegration, plainDecoupled_ # 2"<<std::endl;
            runTransport();
            std::cout<<"test # timeIntegration, plainDecoupled_ # 3"<<std::endl;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid TimeIntegrationIdx " << timeIntegrationIdx_<<", check input file!");

        uCur_ = ChemProblem().model().curSol();     //since the timeIntegration succeeded, we have a new solution!
    }

    void sequentialIterative()
    {
        std::cout<<"test # sequentialIterative \n"<<std::endl;

        uPrev_ = uPrevDt_;
        // run first model      //chemistry
        runChemistry();

        //compute some average reaction rates as source terms for the transport
        deltaUReact_ = ChemProblem().model().curSol();
        deltaUReact_ -= uPrev_;
        qReactCur_ = deltaUReact_;
        qReactCur_ /= ParentType::timeManager_.timeStepSize();

        TranspProblem().reactiveSource() = qReactCur_;

        // run second model     //transport
        runTransport();

        //compute some average Transport as source terms for the chemistry
        deltaUTransp_ = TranspProblem().model().curSol();
        deltaUTransp_ -= uPrev_;
//        uTranspCur_ = TranspProblem().model().curSol();

        couplingError();

        NoIterations_ = 0;
        while (couplingError_>maxCouplingError_ && NoIterations_< 2*maxNoIterations_)
        {
            std::cout<<"Iteration # = "<<NoIterations_<<", couplingError = "<<couplingError_<<", maxCouplingError = "<< maxCouplingError_<<std::endl;
            //rerun the timestep --> new Transport source in Chemistry, new reaction Source in Transport
            qReactPrev_ = qReactCur_;

//          std::cout<<"test # 1: deltaUTransp_ = "<<deltaUTransp_<<std::endl;
            //Set the chemistry solution back to the initial solution of the timestep
            ChemProblem().model().curSol() = uPrev_;
            //Transport only, without reactions
//          std::cout<<"test # 1.1: deltaUTransp_ = "<<deltaUTransp_<<std::endl;
            deltaUTransp_ -= deltaUReact_;
            //add half of the transport-only change to the initial solution of the timestep
//          std::cout<<"test # 1.2: deltaUTransp_ = "<<deltaUTransp_<<std::endl;
            deltaUTransp_ /= 2;
//          std::cout<<"test # 1.3: deltaUTransp_ = "<<deltaUTransp_<<std::endl;
            ChemProblem().model().curSol() += deltaUTransp_;
//          std::cout<<"test # 2"<<std::endl;
            //rerun the Chemistry
            runChemistry();
            //compute some new average reaction rates as source terms for the transport
            deltaUReact_ = ChemProblem().model().curSol();
            deltaUReact_ -= uPrev_;
            qReactCur_ = deltaUReact_;
            qReactCur_ /= ParentType::timeManager_.timeStepSize();

            //set the reactive source in the transport problem
            TranspProblem().reactiveSource() = qReactCur_;

            //Set the transport solution back to the initial solution of the timestep
            TranspProblem().model().curSol() = uPrev_;
//          std::cout<<"test # 3"<<std::endl;
            //rerun the Transport
            runTransport();
            //compute some average Transport as source terms for the chemistry
            deltaUTransp_ = TranspProblem().model().curSol();
            deltaUTransp_ -= uPrev_;
//          uTranspCur_ = TranspProblem().model().curSol();

//          std::cout<<"test # 4"<<std::endl;
            couplingError();
            ++NoIterations_;
        }

        uCur_ = ChemProblem().model().curSol();
        std::cout<<"Finished after Iteration # = "<<NoIterations_<<", couplingError = "<<couplingError_<<", maxCouplingError = "<< maxCouplingError_<<std::endl;
    }

    void sequentialNonIterativeChemistryIsSourceForTransport()
    {
        std::cout<<"test # Seq non iterative \n"<<std::endl;
        // run first model      //chemistry
        runChemistry();
        //compute some new average reaction rates as source terms for the transport
        deltaUReact_ = ChemProblem().model().curSol();
        deltaUReact_ -= uPrevDt_;
        qReactCur_ = deltaUReact_;
        qReactCur_ /= ParentType::timeManager_.timeStepSize();

        //set the reactive source in the transport problem
        TranspProblem().reactiveSource() = qReactCur_;

        //Set the transport solution back to the initial solution of the timestep
        TranspProblem().model().curSol() = uPrevDt_;
        // run second model     //transport
        runTransport();

    }

    void sequentialNonIterativeChemistryFirst()
    {
        std::cout<<"test # Seq non iterative chemistry first \n"<<std::endl;
        // run first model      //chemistry
        runChemistry();
        //update the transport problem with the changes due to the chemistry
        TranspProblem().model().curSol() = ChemProblem().model().curSol();
        // run second model     //transport
        runTransport();
    }

    void sequentialNonIterativeTransportFirst()
    {
        std::cout<<"test # Seq non iterative transport first \n"<<std::endl;
        // run second model     //transport
        runTransport();
        //update the chemistry problem with the changes due to the transport
        ChemProblem().model().curSol() = TranspProblem().model().curSol();
        testSolutions();
        // run first model      //chemistry
        runChemistry();
    }

    void testSolutions()
    {
        int numDofs = ChemProblem().model().numDofs();
        for(int dofIdx=0; dofIdx<numDofs; ++dofIdx)
        {
            for(int eqIdx=0; eqIdx<12; ++eqIdx)
            {
                if (ChemProblem().model().curSol()[dofIdx][eqIdx] != TranspProblem().model().curSol()[dofIdx][eqIdx])
                {

                    std::cout<<"Solutions do not match!\n "
                            "at dofIdx: "<<dofIdx<<"\n"
                            <<"eqIdx: "<<eqIdx<<"\n"
                            <<"ChemProblem solution is: " <<ChemProblem().model().curSol()[dofIdx][eqIdx]<<"\n"
                            <<"TranspProblem solution is: " <<TranspProblem().model().curSol()[dofIdx][eqIdx]<<"\n"
                            <<std::endl;
                }
            }
        }
    }

    void runChemistry()
    {
        runSubProblem1();
    }
    void runTransport()
    {
        runSubProblem2();
    }

    void runSubProblem1()   //Chemistry
    {
        ParentType::subTimeManager1_.setTime(ParentType::timeManager_.time());
        ParentType::subTimeManager1_.setEndTime(ParentType::timeManager_.time() + ParentType::timeManager_.timeStepSize());
        ParentType::subTimeManager1_.setTimeStepSize(ParentType::subTimeManager1_.previousTimeStepSize());
        ParentType::subTimeManager1_.run();
    }

    void runSubProblem2()   //Transport
    {
        ParentType::subTimeManager2_.setTime(ParentType::timeManager_.time());
        ParentType::subTimeManager2_.setEndTime(ParentType::timeManager_.time() + ParentType::timeManager_.timeStepSize());
        ParentType::subTimeManager2_.setTimeStepSize(ParentType::subTimeManager2_.previousTimeStepSize());
        ParentType::subTimeManager2_.run();
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
//      std::cout<<"test # postTimeStep \n"<<std::endl;
        couplingError();

        // exchange data between subproblems with regard to the time integration scheme
        updateSubProblems();
        testSolutions();
    }

    void updateSubProblems()
    {

        if(sequentialIterative_ || plainDecoupled_)
        {
            uCur_ = TranspProblem().model().curSol();
            uCur_ += ChemProblem().model().curSol();
            uCur_ /= 2;

            ChemProblem().model().curSol() = uCur_;
            TranspProblem().model().curSol() = uCur_;
        }

        if(sequentialNonIterative_ || sequentialNonIterativeChemSourceFTransp_)
        {
            ChemProblem().model().curSol() = TranspProblem().model().curSol();
        }

        if(sequentialNonIterativeChemFirst_)    //transport is calculated after chemistry. Thus, after everything is done, the transport solution is "the right" solution
        {
            ChemProblem().model().curSol() = TranspProblem().model().curSol();
        }
        if(sequentialNonIterativeTranspFirst_)  //chemistry is calculated after transport. Thus, after everything is done, the chemistry solution is "the right" solution
        {
            TranspProblem().model().curSol() = ChemProblem().model().curSol();
        }
    }

    void couplingError()
    {
        unsigned numDofs = ChemProblem().model().numDofs();
        Scalar couplingError = 0.0;
        for(int dofIdx=0; dofIdx<numDofs; ++dofIdx)
        {
            Scalar result = 0.0;

            result = ChemProblem().model().relativeErrorDof(dofIdx,
                                            TranspProblem().model().curSol()[dofIdx],
                                            ChemProblem().model().curSol()[dofIdx]);

            couplingError = std::max(result, couplingError);
//          std::cout<<"dofIdx: "<<dofIdx<<", couplingError: "<<couplingError<<std::endl;
            couplingError_ = couplingError;
        }
    }


    BioChemProblem& ChemProblem()
//    { return this->subProblem1_; }
    { return this->subProblem1(); }
    const BioChemProblem& ChemProblem() const
    { return this->subProblem1(); }

    BioTranspProblem& TranspProblem()
//    { return this->subProblem2_; }
    { return this->subProblem2(); }
    const BioTranspProblem& TranspProblem() const
    { return this->subProblem2(); }

private:
    std::string name_;

//    SolutionVector uTemp_;

//    //TODO
    SolutionVector uPrevDt_;
    SolutionVector uPrev_;
    SolutionVector uCur_;
    SolutionVector qReactPrev_;
    SolutionVector uTranspPrev_;
    SolutionVector qReactCur_;
    SolutionVector uTranspCur_;

    SolutionVector deltaUReact_;
    SolutionVector deltaUTransp_;

    Scalar timeStepSizeFactor_;

    int timeIntegrationIdx_;
    int NoIterations_;
    int maxNoIterations_ = 20;
    bool sequentialIterative_ = false;
    bool sequentialNonIterative_ = false;
    bool sequentialNonIterativeChemSourceFTransp_ = false;
    bool sequentialNonIterativeChemFirst_ = false;
    bool sequentialNonIterativeTranspFirst_ = false;
    bool plainDecoupled_ = false;

    Scalar couplingError_ = 1e-10;
    Scalar maxCouplingError_ = 1e-4;
    static constexpr Scalar eps_ = 1e-6;

};

} //end namespace

#endif
