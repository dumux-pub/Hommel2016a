// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License. or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TestBioTranspProblem_HH
#define DUMUX_TestBioTranspProblem_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#include <dumux/material/fluidsystems/biofluidsystem.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
//#include <dumux/implicit/2pncmin/2pncminmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominmodel.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pbiotranspmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominfluxvariables.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pbiotransplocalresidual.hh>
// #include <dumux/implicit/2pbiomin/2pbiominlocalresidual.hh>
//#include <dumux/implicit/2pnc/2pnclocalresidual.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pbiotranspvolumevariables.hh>
#include <dumux/implicit/2pbiomin/2pbiominvolumevariables.hh>
#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include <dumux/material/chemistry/biogeochemistry/biocarbonicacid.hh>

#include "biospatialparams.hh"
#include "bioco2tables.hh"

#include "dumux/linear/seqsolverbackend.hh"


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class TestBioTranspProblem;

namespace Properties
{
NEW_TYPE_TAG(TestBioTranspProblem, INHERITS_FROM(BoxTwoPNCMin, BioMinSpatialParams));
//NEW_TYPE_TAG(TestBioTranspProblem, INHERITS_FROM(BoxTwoBioMin, BioMinSpatialParams));

// Set the grid type
SET_PROP(TestBioTranspProblem, Grid)
{
    typedef Dune::UGGrid<2> type;
//   typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(TestBioTranspProblem, Problem)
{
    typedef Dumux::TestBioTranspProblem<TTAG(TestBioTranspProblem)> type;
};

SET_PROP(TestBioTranspProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::BioFluid<TypeTag, Scalar, CO2Tables, H2O_Tabulated, useComplexRelations> type;
};

//! the BioMin Flux VolumeVariables and LocalResidual properties

SET_TYPE_PROP(TestBioTranspProblem, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);///TODO
//SET_TYPE_PROP(TestBioTranspProblem, VolumeVariables, TwoPBioMinVolumeVariables<TypeTag>);
SET_TYPE_PROP(TestBioTranspProblem, VolumeVariables, TwoPBioTranspVolumeVariables<TypeTag>);
SET_TYPE_PROP(TestBioTranspProblem, LocalResidual, TwoPBioTranspLocalResidual<TypeTag>);
// SET_TYPE_PROP(TestBioTranspProblem, LocalResidual, TwoPBioMinLocalResidual<TypeTag>);
//SET_TYPE_PROP(TestBioTranspProblem, LocalResidual, TwoPNCLocalResidual<TypeTag>);
//SET_TYPE_PROP(TestBioTranspProblem, Model, TwoPBioMinModel<TypeTag>);
SET_TYPE_PROP(TestBioTranspProblem, Model, TwoPBioTranspModel<TypeTag>);

//SET_TYPE_PROP(TestBioTranspProblem, LinearSolver, SuperLUBackend<TypeTag>);

SET_PROP(TestBioTranspProblem, Chemistry)
{
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::BioCarbonicAcid<TypeTag, CO2Tables> type;
};

// Set the spatial parameters
SET_TYPE_PROP(TestBioTranspProblem,
              SpatialParams,
              Dumux::BioMinSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(TestBioTranspProblem, ProblemEnableGravity, false);               //TODO

// Write newton convergence
SET_BOOL_PROP(TestBioTranspProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(TestBioTranspProblem, ImplicitEnablePartialReassemble, false);

SET_SCALAR_PROP(TestBioTranspProblem, LinearSolverResidualReduction, 1e-4);//1e-12);

// To decouple solid phase from the transport model. NumEq in transport problem
//should be set to NumComponents with the namespace CalciteTranspProblem.
SET_PROP(TestBioTranspProblem, NumEq)
{
  private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

  public:
    //static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
    static const int value = FluidSystem::numComponents; // + FluidSystem::numSPhases;
};

}

/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(TestBioTranspProblem) >
class TestBioTranspProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef TestBioTranspProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numComponents = FluidSystem::numComponents,

        //set the unnecessary numSecComponents and numSPhases to zero:
//        numSecComponents = FluidSystem::numSecComponents,
        numSecComponents = 0,
//        numSPhases = FluidSystem::numSPhases,
        numSPhases = 0,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xlNaIdx = FluidSystem::NaIdx,
        xlClIdx = FluidSystem::ClIdx,
        xlCaIdx = FluidSystem::CaIdx,
        xlUreaIdx = FluidSystem::UreaIdx,
        xlTNHIdx = FluidSystem::TNHIdx,
        xlO2Idx = FluidSystem::O2Idx,
        xlBiosubIdx = FluidSystem::BiosubIdx,
        xlBiosuspIdx = FluidSystem::BiosuspIdx,
//        phiBiofilmIdx = numComponents,//FluidSystem::BiofilmIdx,
//        phiCalciteIdx = numComponents + 1,//FluidSystem::CalciteIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        UreaIdx = FluidSystem::UreaIdx,
        TNHIdx = FluidSystem::TNHIdx,
        O2Idx = FluidSystem::O2Idx,
        BiosubIdx = FluidSystem::BiosubIdx,
        BiosuspIdx = FluidSystem::BiosuspIdx,

        NH4Idx = FluidSystem::NH4Idx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        OHIdx = FluidSystem::OHIdx,

//        BiofilmIdx = FluidSystem::BiofilmIdx,
//        CalciteIdx = FluidSystem::CalciteIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

//    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
   // typedef Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, Dumux::BioMin::CO2Tables, true> Brine_CO2;

//    typedef Dune::FieldVector<Scalar, numComponents + numSecComponents> CompVector;

public:
    TestBioTranspProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {

        try
    {

            dtmax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtMax);

            //initial values
            densityW_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initDensityW);
            initPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPressure);

            initxlTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTC);
            initxlNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlNa);
            initxlCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCl);
            initxlCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCa);
            initxlUrea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlUrea);
            initxlTNH_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTNH);
            initxlO2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlO2);
            initxlBiosub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlSubstrate);
            initxlBiosusp_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlBiosusp);

//          initBiofilm_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initBiofilm);
//          initCalcite_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initCalcite);
//
//          initPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);

            xlNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlNaCorr);
            xlClCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlClCorr);

            //injection values
            injQ_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injVolumeflux);
            angle_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, angle);

            injTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTC);
            injNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injNa);
//          injCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCl);
            injCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCa);
            injUrea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injUrea);
            injTNH_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTNH);
            injO2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injO2);
            injSub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injSub);
            injBiosusp_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injBiosusp);

            injNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injNaCorr);
    }

    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }

        FluidSystem::init();
        this->timeManager().startNextEpisode(EpisodeEnd(1));
        this->newtonController().setMaxSteps(15);

    }

    bool shouldWriteOutput() const
        {
       return
             this->timeManager().episodeWillBeOver();
//       this->timeManager().willBeFinished();
        }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    void preTimeStep()
    {
       previousTimeStepSize_ = this->timeManager().previousTimeStepSize();
            if (this->timeManager().timeStepSize()>dtmax_)
        {
            this->timeManager().setTimeStepSize(dtmax_);
        }
    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    //const char *name() const
    const std::string name() const
    {
        return  "TestBioTransport";
    }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 25; // -> 25 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar rmax = this->bBoxMax()[0];
        values.setAllNeumann();

//        if(globalPos[0] > rmax - eps_)
        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1]) > rmax - eps_)
        {
            values.setAllDirichlet();
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
            initial_(values,globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      int scvIdx,
                      int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        Scalar rmin = 0.0381;
        Scalar height = 0.0254*15/16;
        Scalar area = rmin*angle_*height;       //1/8 th --> b= rmin * pi/4

        int episodeIdx = this->timeManager().episodeIndex()-1;
        Scalar waterflux = injQ_/area; //[m/s]

        Scalar InjProcess = InjectionProcess(episodeIdx);

//         negative values for injection
        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1]) <= rmin+eps_)
        {
        //basic rinse injection (InjProcess == -1 )
        values[conti0EqIdx + wCompIdx] = -waterflux * 996/FluidSystem::molarMass(wCompIdx);
        values[conti0EqIdx + nCompIdx] = -waterflux * injTC_*996 /FluidSystem::molarMass(nCompIdx);
        values[conti0EqIdx + xlCaIdx] = 0;
        values[conti0EqIdx + xlBiosuspIdx] = 0;
        values[conti0EqIdx + xlBiosubIdx] = -waterflux * injSub_ /FluidSystem::molarMass(xlBiosubIdx);
        values[conti0EqIdx + xlO2Idx] = -waterflux * injO2_ /FluidSystem::molarMass(O2Idx);
        values[conti0EqIdx + xlUreaIdx] = 0;
        values[conti0EqIdx + xlTNHIdx] = -waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx);
//      values[conti0EqIdx + phiCalciteIdx] = 0;
//      values[conti0EqIdx + phiBiofilmIdx] = 0;
        values[conti0EqIdx + xlNaIdx] = -waterflux * (injNa_ + injNaCorr_) /FluidSystem::molarMass(NaIdx);
        values[conti0EqIdx + xlClIdx] = -waterflux *injTNH_ /FluidSystem::molarMass(TNHIdx)     //NH4Cl --->  mol Cl = mol NH4
                                        -waterflux *injNa_ /FluidSystem::molarMass(NaIdx);      //NaCl ---> mol Cl = mol Na

        if (InjProcess == -1)   // rinse, used as standard injection fluid
        {
            //          do not change anything.
        }

        else if (InjProcess == -99) // no injection
        {
            values = 0.0; //mol/m²/s
        }

        else if (InjProcess == 1)       //ca-rich injection: ca and urea injected additionally to rinse-fluid, Na (pH) and Cl are also different(CaCl2)
        {
            values[conti0EqIdx + wCompIdx] = - waterflux * 0.8716 * densityW_ /FluidSystem::molarMass(wCompIdx);    //TODO 0.8716 check factor!!!
            values[conti0EqIdx + nCompIdx] = - waterflux * injTC_ * densityW_ /FluidSystem::molarMass(nCompIdx);
            values[conti0EqIdx + xlCaIdx] = - waterflux * injCa_/FluidSystem::molarMass(CaIdx);
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
            values[conti0EqIdx + xlNaIdx] = - waterflux * injNa_ /FluidSystem::molarMass(NaIdx)
                                            - waterflux * injNaCorr_ /FluidSystem::molarMass(NaIdx)* 0.032;
            values[conti0EqIdx + xlClIdx] = - waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx)               //NH4Cl --->  mol Cl = mol NH4
                                            - waterflux * 2 * injCa_/FluidSystem::molarMass(CaIdx)              //+CaCl2 --->  mol Cl = mol Ca*2
                                            - waterflux *injNa_ /FluidSystem::molarMass(NaIdx);         //NaCl ---> mol Cl = mol Na
        }

        else if (InjProcess == 0 || InjProcess == 3 )   //urea-injections: urea is injected additionally to rinse-fluid
        {
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
        }

        else if(InjProcess == 2)        //inoculation: same as rinse, but with bacteria
        {
            values[conti0EqIdx + xlBiosuspIdx] = -waterflux * injBiosusp_ /FluidSystem::molarMass(xlBiosuspIdx);
        }
            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Invalid injection process " << InjProcess);
            }
        }
        else
        {
            values = 0.0; //mol/m²/s
        }
  }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &q,
              const Element &element,
              const FVElementGeometry &fvGeometry,
                int scvIdx) const
    {
        q = 0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void reactionSource(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                const VolumeVariables &volVars,
                const Scalar absgradpw) const
    {

        q = 0;

    }

    /*!
       * \brief Get the solidity of a node in the transport problem.
    */
    Scalar getSolidity(const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx) const
    {
        //set the initial value for the 0th timestep to zero.
        if(this->timeManager().timeStepIndex()==0.0)
        {
            return 0.0;
        }
        else
        {
            int dofIdx = this->model().vertexMapper().map(element, scvIdx, dim);
//          std::cout << "getSolidity dofIdx" << dofIdx << "solidity " << solidity_[dofIdx] << std::endl;
            return solidity_[dofIdx];
        }
    }

    /*!
     * \brief Get the solidity vector of the transport problem.
     */
    std::vector<Scalar> &solidity()
    { return solidity_; }


    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
//      return bothPhases;
    }

    Scalar refValue(int EqIdx)
    {
//      Scalar refValue = refValue_[EqIdx];
        Scalar refValue = 0;
        Scalar mH2O = 55.508; //molH2O/kg(l)H2O
        switch (EqIdx) {
            case pressureIdx: refValue = initPressure_;break;
            case switchIdx: refValue = injTC_ /(FluidSystem::molarMass(nCompIdx) * mH2O);break;
            case CaIdx: refValue = injCa_/(1000 * FluidSystem::molarMass(CaIdx) * mH2O);break;
            case NaIdx: refValue = (injNa_+injNaCorr_)/(1000 * FluidSystem::molarMass(NaIdx) * mH2O);break;
            case ClIdx: refValue = 2*injCa_/(1000 * FluidSystem::molarMass(CaIdx) * mH2O)
                                    + injTNH_/(1000 * FluidSystem::molarMass(TNHIdx) * mH2O)
                                    + injNa_/(1000 * FluidSystem::molarMass(NaIdx) * mH2O);break;
            case BiosuspIdx: refValue = injBiosusp_/(1000 * FluidSystem::molarMass(BiosuspIdx) * mH2O);break;
            case BiosubIdx: refValue = injSub_/(1000 * FluidSystem::molarMass(BiosubIdx) * mH2O);break;
            case O2Idx: refValue = injO2_/(1000 * FluidSystem::molarMass(O2Idx) * mH2O);break;
            case UreaIdx: refValue = injUrea_/(1000 * FluidSystem::molarMass(UreaIdx) * mH2O);break;
            case TNHIdx: refValue = injTNH_/(1000 * FluidSystem::molarMass(TNHIdx) * mH2O);break;
//          case phiCalciteIdx: refValue = initPorosity_;break;
//          case phiBiofilmIdx: refValue = initPorosity_;break;
        }
//        std::cout<< "reference value for EqIdx = "<<EqIdx<< ": "<<refValue<<std::endl;
        return refValue;
    }


    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        //if(this->timeManager().episodeIndex() ==1 )


     int episodeIdx = this->timeManager().episodeIndex()-1;
     Scalar tEpisode= EpisodeEnd(episodeIdx + 1)-EpisodeEnd(episodeIdx);

        this->timeManager().startNextEpisode(tEpisode);
//            this->timeManager().setTimeStepSize(tEpisode / 100);
//        if (InjectionProcess(episodeIdx + 1) == -1)
//              this->timeManager().setTimeStepSize(0.01);
//        else if (InjectionProcess(episodeIdx + 1) == 0 || InjectionProcess(episodeIdx + 1) == 3)
                this->timeManager().setTimeStepSize(0.1);
//        else
//          this->timeManager().setTimeStepSize(10);

//                 std::cout<< "\n  episode number  " << episodeIdx << " done, starting next episode  " <<episodeIdx + 1 << ", InjectionProcess scheme is : "<<  injType_[episodeIdx + 1] << "\n"  <<std::endl;
       std::cout<< "\n  episode number  " << episodeIdx << " done, starting next episode  " <<episodeIdx + 1 << ", InjectionProcess scheme is : "<<  InjectionProcess(episodeIdx + 1) << "\n"  <<std::endl;

    }
    Scalar getPreviousTimeStepSize()
    {
        return previousTimeStepSize_;
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar rmax = this->bBoxMax()[0];
    Scalar r = sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1]);
        values[pressureIdx] = initPressure_ ; //70e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        values[switchIdx] = initxlTC_;
        values[xlNaIdx] = initxlNa_ + xlNaCorr_;
        values[xlClIdx] = initxlCl_ + initxlTNH_ + 2*initxlCa_ + xlClCorr_;
        values[xlCaIdx] = initxlCa_;
        values[xlUreaIdx] = initxlUrea_;
        values[xlTNHIdx] = initxlTNH_;
        values[xlO2Idx] = initxlO2_;
        values[xlBiosubIdx] = initxlBiosub_;
        values[xlBiosuspIdx] = initxlBiosusp_;
//        values[phiBiofilmIdx] = (rmax-r)*initBiofilm_; // [m^3/m^3]
//        values[phiCalciteIdx] = r*initCalcite_; // [m^3/m^3]

#if !ISOTHERMAL
        values[temperatureIdx] = 283.0 + (depthBOR_ - globalPos[1])*0.03;
#endif

    }
//    static const Scalar injQ (int episodeIdx) //[ml/min]
//    {
//      return 57.2/60/1e9; //[m³/s] = [ml/min] /[s/min] /[ml/m²]
//    }


   static const int InjectionProcess (int episodeIdx)
   {        // 2 = inoculation, 1 = ca-rich injection, -1 = no urea & no ca, 3 = urea-rich, -99 no injection
    int Inj [10]=
        {-99,                                                           //0
        1,
        -99,
        2,
        -99,
        1,
        -99,

        -99};

   return Inj[episodeIdx];
   }

    static const  Scalar EpisodeEnd (int episodeIdx)
    {
    //End times of the episodes in hours.
     Scalar EpiEnd[10] = {0,                                        //0
             5, //calcium
             60, //no flow
             65, //inoculation
             120, //no flow
             125, //calcium
             180, //no flow
     10e10};

   return  60 * EpiEnd[episodeIdx];
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar dtmax_;

    Scalar initPressure_;
    Scalar densityW_;//1087; // rhow=1087;

    Scalar initxlTC_;//2.3864e-7;       // [mol/mol]
    Scalar initxlNa_;//0;
    Scalar initxlCl_;//0;
    Scalar initxlCa_;//0;
    Scalar initxlUrea_;//0;
    Scalar initxlTNH_;//3.341641e-3;
    Scalar initxlO2_;//4.4686e-6;
    Scalar initxlBiosub_;//2.97638e-4;
    Scalar initxlBiosusp_;//0;
    Scalar xlNaCorr_;//2.9466e-6;
    Scalar xlClCorr_;//0;

//    Scalar initBiofilm_;
//    Scalar initCalcite_;

    Scalar injQ_;
    Scalar angle_;

    Scalar injTC_;//5.8e-7;             // [kg/kg]
    Scalar injNa_;//0.00379;                // [kg/m³]
    Scalar injCa_;//13.3593333333;          // [kg/m³]      //computed from 0.333mol/l CaCl2
    Scalar injUrea_;//20;                   // [kg/m³]
    Scalar injTNH_;//3.183840574;//3.184;               // [kg/m³]      //computed from 10 g/l NH4Cl
    Scalar injO2_;//0.008;                  // [kg/m³]
    Scalar injSub_;//3;                 // [kg/m³]
    Scalar injBiosusp_;//0.0675;            // [kg/m³]      //2.7e8 cfu/ml (40e8cfu/ml~1g/l)
    Scalar injNaCorr_;

    std::vector<Scalar> solidity_;

    Scalar previousTimeStepSize_;
};
} //end namespace

#endif
