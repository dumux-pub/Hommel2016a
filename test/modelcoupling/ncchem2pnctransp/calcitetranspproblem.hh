// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BIOTRANSP_PROBLEM_HH
#define DUMUX_BIOTRANSP_PROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfalu.hh>

#include<dumux/material/fluidsystems/calcitefluidsystem.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/2pncmin/2pncminmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominmodel.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pcalcitetranspmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominfluxvariables.hh>
#include <dumux/implicit/2pbiomin/2pbiominlocalresidual.hh>
#include <test/modelcoupling/ncchem2pnctransp/modelfiles/2pcalcitetranspvolumevariables.hh>
#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include <dumux/material/chemistry/geochemistry/testcarbonicacidvarsalinity.hh>

//#include "biospatialparams.hh"
#include "biospatialparams.hh"
#include "bioco2tables.hh"

#include "dumux/linear/seqsolverbackend.hh"
#include <vector>


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class CalciteTranspProblem;

namespace Properties
{
NEW_TYPE_TAG(CalciteTranspProblem, INHERITS_FROM(BoxTwoPNCMin, BioMinSpatialParams));
//NEW_TYPE_TAG(CalciteTranspProblem, INHERITS_FROM(BoxTwoBioMin, BioMinSpatialParams));

// Set the grid type
SET_PROP(CalciteTranspProblem, Grid)
{
    typedef Dune::UGGrid<2> type;
//   typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(CalciteTranspProblem, Problem)
{
    typedef Dumux::CalciteTranspProblem<TTAG(CalciteTranspProblem)> type;
};

SET_PROP(CalciteTranspProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::CalciteFluid<Scalar, CO2Tables, H2O_Tabulated, useComplexRelations> type;
};

//! the BioMin Flux VolumeVariables and LocalResidual properties

SET_TYPE_PROP(CalciteTranspProblem, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);
SET_TYPE_PROP(CalciteTranspProblem, VolumeVariables, TwoPCalciteTranspVolumeVariables<TypeTag>);
SET_TYPE_PROP(CalciteTranspProblem, LocalResidual, TwoPBioMinLocalResidual<TypeTag>);
//SET_TYPE_PROP(CalciteTranspProblem, Model, TwoPBioMinModel<TypeTag>);
SET_TYPE_PROP(CalciteTranspProblem, Model, TwoPCalciteTranspModel<TypeTag>);


//SET_TYPE_PROP(CalciteTranspProblem, LinearSolver, SuperLUBackend<TypeTag>);

SET_PROP(CalciteTranspProblem, Chemistry)
{
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::CarbonicAcidVarSalinity<TypeTag, CO2Tables> type;

//    typedef Dumux::NoChemistry<TypeTag> type;

};

// Set the spatial parameters
SET_TYPE_PROP(CalciteTranspProblem,
              SpatialParams,
              Dumux::BioMinSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(CalciteTranspProblem, ProblemEnableGravity, false);               //TODO

// Write newton convergence
SET_BOOL_PROP(CalciteTranspProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(CalciteTranspProblem, ImplicitEnablePartialReassemble, false);

//SET_SCALAR_PROP(CalciteTranspProblem, LinearSolverResidualReduction, 1e-12);

// To decouple solid phase from the transport model. NumEq in transport problem
//should be set to NumComponents with the namespace CalciteTranspProblem.
SET_PROP(CalciteTranspProblem, NumEq)
{
  private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

  public:
    //static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
    static const int value = FluidSystem::numComponents; // + FluidSystem::numSPhases;
};

}

/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(CalciteTranspProblem) >
class CalciteTranspProblem : public ImplicitPorousMediaProblem<TypeTag>
//class CalciteTranspProblem : public TwoPNCMinProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef CalciteTranspProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        //        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        // In this situation porosity is a constant.
        numComponents = FluidSystem::numComponents,
        numEq = numComponents,
        numSecComponents = FluidSystem::numSecComponents,
        numSPhases = FluidSystem::numSPhases,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xlNaIdx = FluidSystem::NaIdx,
        xlClIdx = FluidSystem::ClIdx,
        xlCaIdx = FluidSystem::CaIdx,
//        phiCalciteIdx = numComponents,//FluidSystem::CalciteIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        OHIdx = FluidSystem::OHIdx,

//        CalciteIdx = FluidSystem::CalciteIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
   // typedef Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, Dumux::BioMin::CO2Tables, true> Brine_CO2;

//    typedef Dune::FieldVector<Scalar, numComponents + numSecComponents> CompVector;

public:
    CalciteTranspProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        FluidSystem::init();
        try
        {

            //initial values
            densityW_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initDensityW);
            initPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPressure);

            initxlTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTC);
            initxlNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlNa);
            initxlCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCl);
            initxlCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCa);
            initCalcite_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initCalcite);

            xlNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlNaCorr);
            xlClCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlClCorr);

            //injection values
            injTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTC);
            injNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injNa);
//          injCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCl);
            injCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCa);

            timeIntegrationIdx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Problem, TimeIntegrationIdx);

            if (timeIntegrationIdx_ == 1 || timeIntegrationIdx_ == 2 || timeIntegrationIdx_ == 2012 )
                useReactiveSource_ = true;

            numOutputSteps_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Problem, numOutputSteps);
            tEpisode_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,   Scalar, TimeManager, TEnd);
            tEpisode_ /= numOutputSteps_;
            }


    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }

        FluidSystem::init();
        this->timeManager().startNextEpisode(tEpisode_);
        this->newtonController().setMaxSteps(15);
    }
    /*!
      * \brief Returns true if a output file should be written to
      *        disk.
      *
      * The default behaviour is to write one output file every time
      * step.
      */
    bool shouldWriteOutput() const
        {
            return
//                  this->timeManager().timeStepIndex() % 1 == 0 ||         //output every timestep
//                  this->timeManager().timeStepIndex() % 1000 == 0 ||          //output every 1000 timesteps
//              this->timeManager().timeStepIndex() == 0 ||
//              this->timeManager().episodeWillBeOver() ||
//              this->timeManager().willBeFinished();

                this->timeManager().episodeWillBeOver();
        }

    bool shouldWriteRestartFile() const
    {
        return false;
//                  this->timeManager().timeStepIndex() % 1 == 0 ||         // every timestep
//              this->timeManager().timeStepIndex() % 1000 == 0 ||          // every 1000 timesteps
//          this->timeManager().episodeWillBeOver();
    }

    void preTimeStep()
    {

    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    { return "CalciteTransport"; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 25; // -> 25 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        //clock_t startTime, stopTime;
        //startTime = clock();

        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar xmax = this->bBoxMax()[0];
        values.setAllNeumann();
        if(globalPos[0] > xmax - eps_)
        {
            values.setAllDirichlet();
//          values.setDirichlet(pressureIdx);
//          for (int i=switchIdx; i< numEq - numSPhases; ++i)
//          {
////                values.setOutflow(i);
//              values.setDirichlet(i);
//          }
        }
        //stopTime = clock();
        //std::cout<<"test #exec time for boundaryTypes transport porblem" << (double(stopTime - startTime) / CLOCKS_PER_SEC) << std::endl;


    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
            initial_(values,globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      int scvIdx,
                      int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;


//         negative values for injection
        if(globalPos[0]<= eps_)
        {

            Scalar mtot =500.0/3600.0/24.0; //kg/s   //0.00289;
            mtot /=2;                   //area
            //co2-rich brine
             values[conti0EqIdx + wCompIdx] = -mtot * 0.87 / FluidSystem::molarMass(wCompIdx);  //mol/m²/s
             values[conti0EqIdx + nCompIdx] = -mtot * 0.03 / FluidSystem::molarMass(nCompIdx);
             values[conti0EqIdx + xlNaIdx] =  -mtot * 0.1 / (FluidSystem::molarMass(ClIdx)+FluidSystem::molarMass(NaIdx));
             values[conti0EqIdx + xlClIdx] =  -mtot * 0.1 / (FluidSystem::molarMass(NaIdx)+FluidSystem::molarMass(ClIdx));
             values[conti0EqIdx + xlCaIdx] = 0.0;
//             values[conti0EqIdx + phiCalciteIdx] = 0;

            //co2-only
//             values[conti0EqIdx + wCompIdx] = -0 * mtot / FluidSystem::molarMass(wCompIdx); //mol/m²/s
//             values[conti0EqIdx + nCompIdx] = -1* mtot / FluidSystem::molarMass(nCompIdx);
//             values[conti0EqIdx + xlNaIdx] = -0 * mtot / (FluidSystem::molarMass(NaIdx) + FluidSystem::molarMass(ClIdx));
//             values[conti0EqIdx + xlClIdx] = -0 * mtot / (FluidSystem::molarMass(NaIdx) + FluidSystem::molarMass(ClIdx));
//             values[conti0EqIdx + xlCaIdx] = 0.0;
//             values[conti0EqIdx + phiCalciteIdx] = 0;
        }

        else
        {
            values = 0.0; //mol/m²/s
        }

  }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &q,
              const Element &element,
              const FVElementGeometry &fvGeometry,
              int scvIdx) const
    {
        q = 0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void reactionSource(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                const VolumeVariables &volVars,
                const Scalar dummy) const
    {

        q = 0;

        if (useReactiveSource_)
        {
            int dofIdx = this->model().vertexMapper().map(element, scvIdx, dim);
            q = reactiveSource_[dofIdx];
//          std::cout <<"q ["<<dofIdx<<"] = "<<q<<std::endl;
        }
    }

    /*!
     * \brief Reference to the current reactive Source as computed by the Chemistry as a block vector.
     */
    SolutionVector &reactiveSource()
    { return reactiveSource_; }

    /*!
       * \brief Get the solidity of a node in the transport problem.
    */
    Scalar getSolidity(const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx) const
    {
        //set the initial value for the 0th timestep to zero.
        if(this->timeManager().timeStepIndex()==0.0)
        {
            return 0.0;
        }
        else
        {
            int dofIdx = this->model().vertexMapper().map(element, scvIdx, dim);
            return solidity_[dofIdx];
        }
    }

    /*!
     * \brief Get the solidity vector of the transport problem.
     */
    std::vector<Scalar> &solidity()
    { return solidity_; }


    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        //clock_t startTime, endTime;
        //startTime = clock();
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
        //endTime = clock();
        //std::cout<<"test #exec time for intial transport porblem" << (double(endTime - startTime) / CLOCKS_PER_SEC) << std::endl;
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
//      return bothPhases;
    }

    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        //if(this->timeManager().episodeIndex() ==1 )

     this->timeManager().startNextEpisode(tEpisode_);
//     this->timeManager().setTimeStepSize(1.0);
     this->timeManager().setTimeStepSize(this->timeManager().previousTimeStepSize());
    }

    /*!
     * \brief Return the maximal relative error of the current round of calculation. .
     */
    Scalar relativeErrorDof(const int dofIdx,
                                const PrimaryVariables &priVars1,
                                const PrimaryVariables &priVars2)
        {
            Scalar result = 0.0;
            for (int j = 0; j < numEq; ++j) {
                Scalar eqErr = std::abs(priVars1[j] - priVars2[j]);
                eqErr /= std::max<Scalar>(1.0, std::abs(priVars1[j] + priVars2[j])/2);

                result = std::max(result, eqErr);
            }
            return result;
        }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[1];
        values[pressureIdx] = initPressure_ ; //70e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        values[switchIdx] = initxlTC_;
        values[xlNaIdx] = initxlNa_ + xlNaCorr_;
        values[xlClIdx] = initxlCl_ + xlClCorr_;
        values[xlCaIdx] = initxlCa_;
//        values[phiCalciteIdx] = initCalcite_;//0.00000; // [m^3/m^3]

//        std::cout<<"initial values transport = "<<values<<std::endl;

#if !ISOTHERMAL
        values[temperatureIdx] = 283.0 + (depthBOR_ - globalPos[1])*0.03;
#endif

    }

    static constexpr Scalar eps_ = 1e-6;


    Scalar initPressure_;
    Scalar densityW_;//1087; // rhow=1087;

    Scalar initxlTC_= 0.;//2.3864e-7;       // [mol/mol]
    Scalar initxlNa_= 0.;//0;
    Scalar initxlCl_= 0.;//0;
    Scalar initxlCa_= 0.;//0;
    Scalar xlNaCorr_= 0.;//2.9466e-6;
    Scalar xlClCorr_= 0.;//0;
    Scalar initCalcite_= 0.;

    Scalar injTC_= 0.;//5.8e-7;             // [kg/kg]
    Scalar injNa_= 0.;//0.00379;                // [kg/m³]
    Scalar injCa_= 0.;//13.3593333333;          // [kg/m³]      //computed from 0.333mol/l CaCl2

    int timeIntegrationIdx_;
    bool useReactiveSource_ = false;

    Scalar tEpisode_;
    int numOutputSteps_;

    SolutionVector reactiveSource_;
    //Vector<Scalar> solidity_;
    std::vector<Scalar> solidity_;

    // Parameters for performance test
    //clock_t startTime, endTime;
};
} //end namespace

#endif
