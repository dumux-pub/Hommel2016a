// $Id: 2p2cvolumevariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008,2009 by Vishal Jambhekar,
 *                              Alexzander Kissinger,
 *                              Klaus Mosthaf,                               *
 *                              Andreas Lauser,                              *
 *                              Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
#ifndef DUMUX_2PCALCITETRANSP_VOLUME_VARIABLES_HH
#define DUMUX_2PCALCITETRANSP_VOLUME_VARIABLES_HH

#include <dumux/implicit/common/implicitmodel.hh>
//#include <dumux/material/fluidstates/compositionalfluidstate.hh>
#include <dumux/material/fluidstates/compositionalseccompfluidstate.hh>
#include <dumux/common/math.hh>
#include <dune/common/parallel/collectivecommunication.hh>
#include <vector>
#include <iostream>
#include <iomanip>

//#include "dumux/implicit/2pncmin/2pncminvolumevariables.hh"
#include "dumux/implicit/2pnc/2pncvolumevariables.hh"

#include <dumux/material/constraintsolvers/computefromreferencephase2pnc.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>

//#include <dumux-devel/appl/co2/biomin/chemistry/biocarbonicacid.hh>
#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include "bioco2tables.hh"

namespace Dumux
{

/*!
 * \ingroup TwoPNCMinModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
template <class TypeTag>
class TwoPCalciteTranspVolumeVariables : public TwoPNCVolumeVariables<TypeTag>
{
    typedef TwoPNCVolumeVariables<TypeTag> ParentType;
    typedef ImplicitVolumeVariables<TypeTag> BaseClassType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, CO2Tables, true> Brine_CO2;

    enum {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNCComposition<Scalar, FluidSystem> miscible2pNCComposition;
    typedef Dumux::ComputeFromReferencePhase2pNCMin<Scalar, FluidSystem> computeFromReferencePhase2pNCMin;

public:

      //! The type of the object returned by the fluidState() method
//    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;
    typedef CompositionalSecCompFluidState<Scalar, FluidSystem> FluidState;
    /*!
     * \copydoc 2pncMin::update
     *
     */
    void update(const PrimaryVariables &primaryVariables,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(primaryVariables,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        completeFluidState(primaryVariables, problem, element, fvGeometry, scvIdx, fluidState_, isOldSol);

        /////////////
        // calculate the remaining quantities
        /////////////

    const MaterialLawParams &materialParams =
      problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);

    // Second instance of a parameter cache.
        // Could be avoided if diffusion coefficients also
        // became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState_);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                // relative permeabilities
                Scalar kr;
                if (phaseIdx == wPhaseIdx)
                    kr = MaterialLaw::krw(materialParams, fluidState_.saturation(wPhaseIdx));
                else // ATTENTION: krn requires the liquid saturation
                    // as parameter!
                    kr = MaterialLaw::krn(materialParams, fluidState_.saturation(wPhaseIdx));
                ParentType::mobility_[phaseIdx] = kr / fluidState_.viscosity(phaseIdx);
                Valgrind::CheckDefined(ParentType::mobility_[phaseIdx]);
               int compIIdx = phaseIdx;
               for(int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    int compJIdx = compIdx;
                    // binary diffusion coefficents
                    ParentType::diffCoeff_[phaseIdx][compIdx] = 0.0;
                    if(compIIdx!= compJIdx)
                      ParentType::diffCoeff_[phaseIdx][compIdx] = FluidSystem::binaryDiffusionCoefficient(fluidState_,
                                                                                    paramCache,
                                                                                    phaseIdx,
                                                                                    compIIdx,
                                                                                    compJIdx);
                    Valgrind::CheckDefined(ParentType::diffCoeff_[phaseIdx][compIdx]);
                }
            }
        // porosity and solidity evaluation
        initialPorosity_ = problem.spatialParams().porosity(element,
                                                            fvGeometry,
                                                            scvIdx); //1 - InitialSolidity;

        Scalar decoupledPorosity_= initialPorosity_
                                 -problem.getSolidity(element,
                                                            fvGeometry,
                                                            scvIdx);
        porosity_= decoupledPorosity_;
        Scalar critPoro = problem.spatialParams().critPorosity(element,
                    fvGeometry,
                    scvIdx);

    for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
    {
        solidity_[sPhaseIdx] = 0;
    }

    salinity_= 0.0;
    moleFracSalinity_ = 0.0;
    for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
    {
        if(fluidState_.moleFraction(wPhaseIdx, compIdx)>0)
        {
            salinity_+= fluidState_.massFraction(wPhaseIdx, compIdx);
            moleFracSalinity_ += fluidState_.moleFraction(wPhaseIdx, compIdx);
        }
    }
        moleFracSalinity_ = massToMoleFrac_(salinity_);

    // Kozeny-Carman relation
     Scalar KozenyCarmanExponent = 3;       //TODO
     permFactor_ = std::pow(((porosity_ - critPoro)/(initialPorosity_ - critPoro)), KozenyCarmanExponent);

    }

      /*!
    * \copydoc BoxModel::completeFluidState
    * \the secondary components are calculated here by calculateEquilibriumChemistry
    */
  static void completeFluidState(const PrimaryVariables& primaryVariables,
                  const Problem& problem,
                  const Element& element,
                  const FVElementGeometry& fvGeometry,
                  int scvIdx,
                  FluidState& fluidState,
                  bool isOldSol = false)

    {
        Scalar t = Implementation::temperature_(primaryVariables, problem, element,
                                                fvGeometry, scvIdx);
        fluidState.setTemperature(t);

      int globalVertIdx = problem.model().dofMapper().map(element, scvIdx, dim);
        int phasePresence = problem.model().phasePresence(globalVertIdx, isOldSol);

    /////////////
        // set the saturations
        /////////////

    Scalar Sg;
        if (phasePresence == nPhaseOnly)
            Sg = 1.0;
        else if (phasePresence == wPhaseOnly) {
            Sg = 0.0;
        }
        else if (phasePresence == bothPhases) {
            if (formulation == plSg)
                Sg = primaryVariables[switchIdx];
            else if (formulation == pgSl)
                Sg = 1.0 - primaryVariables[switchIdx];
            else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }
    else DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
        fluidState.setSaturation(nPhaseIdx, Sg);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sg);

            /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams
        = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pC = MaterialLaw::pc(materialParams, 1 - Sg);

        // extract the pressures
        if (formulation == plSg) {
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx]);
            if (primaryVariables[pressureIdx] + pC < 0.0)
                            DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too low");
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx] + pC);
        }
        else if (formulation == pgSl) {
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx]);
// Here we check for (p_g - pc) in order to ensure that (p_l > 0)
            if (primaryVariables[pressureIdx] - pC < 0.0)
            {
                std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pC << std::endl;
                DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too high");
            }
//          std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pC << std::endl;
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx] - pC);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

        /////////////
        // calculate the phase compositions
        /////////////

        // set the known mole fractions in the fluidState so that they
        // can be used by the Miscible2pNcComposition constraint solver
        // and can be used to compute the fugacity coefficients
        for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
        {
            fluidState.setMoleFraction(wPhaseIdx, compIdx, primaryVariables[compIdx]);
        }

    typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases) {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

            Miscible2pNcComposition::solve(fluidState,
                                           paramCache,
                                           wPhaseIdx,   //known phaseIdx
                                           /*setViscosity=*/true,
                                           /*setInternalEnergy=*/false);
        }
        else if (phasePresence == nPhaseOnly){

            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;
            Dune::FieldVector<Scalar, numComponents> fugCoeffL;
            Dune::FieldVector<Scalar, numComponents> fugCoeffG;

            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fugCoeffL[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                wPhaseIdx,
                                                                compIdx);
                fugCoeffG[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                nPhaseIdx,
                                                                compIdx);
            }
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = primaryVariables[compIdx]*fugCoeffL[compIdx]*fluidState.pressure(wPhaseIdx)
                                        /(fugCoeffG[compIdx]*fluidState.pressure(nPhaseIdx));
            }
            moleFrac[wCompIdx] =  primaryVariables[switchIdx];
            Scalar sumMoleFracNotGas = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracNotGas+=moleFrac[compIdx];
            }
            sumMoleFracNotGas += moleFrac[wCompIdx];
            moleFrac[nCompIdx] = 1 - sumMoleFracNotGas;

//            for (int compIdx=numComponents; compIdx<numSecComponents; ++compIdx)
//            {
//              moleFrac[compIdx] = 0;          //no secondary component in the gas phase!!
//            }

            // convert mass to mole fractions and set the fluid state
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);
            }
            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)        //TODO
            {
                fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, 0);
                if (compIdx == CO2Idx)
                    fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
            }
            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "ComputeFromReferencePhase2pNc" constraint solver
             ComputeFromReferencePhase2pNc::solve(fluidState,
                                             paramCache,
                                             nPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setInternalEnergy=*/false);

         }
        else if (phasePresence == wPhaseOnly){
        // only the liquid phase is present, i.e. liquid phase
        // composition is stored explicitly.
        // extract _mass_ fractions in the gas phase
            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;

            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = primaryVariables[compIdx];
            }
            moleFrac[nCompIdx] = primaryVariables[switchIdx];
            Scalar sumMoleFracNotWater = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracNotWater+=moleFrac[compIdx];
            }
            sumMoleFracNotWater += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 - sumMoleFracNotWater;

            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                moleFrac[compIdx] = 0;
            }

//           Scalar XlSalinity = fluidState.massFraction(wPhaseIdx, NaIdx) + fluidState.massFraction(wPhaseIdx, ClIdx) + fluidState.massFraction(wPhaseIdx, CaIdx); //Salinity= XNa+XCl+XCa
         Scalar XlSalinity = fluidState.massFraction(wPhaseIdx, NaIdx) + fluidState.massFraction(wPhaseIdx, ClIdx); //Salinity= XNa+XCl+XCa

                        Scalar xgH2O;
                        Scalar xlCO2;
                        Scalar x_NaCl;

             Brine_CO2::calculateMoleFractions(fluidState.temperature(),
                                        fluidState.pressure(nPhaseIdx),
                                        XlSalinity,
                                        /*knownPhaseIdx=*/-1,
                                        xlCO2,
                                        xgH2O,
                                        x_NaCl);
                // normalize the phase compositions
                xlCO2 = std::max(0.0, std::min(1.0, xlCO2));
                xgH2O = std::max(0.0, std::min(1.0, xgH2O));

//              Scalar xgO2 = 0;

    //            for (int compIdx=0; compIdx<numComponents + numSecComponents; ++compIdx)
                for (int compIdx=0; compIdx<numComponents; ++compIdx)
                {
                    fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
                    fluidState.setMoleFraction(nPhaseIdx, compIdx, 0);
                }
                fluidState.setMoleFraction(nPhaseIdx, wCompIdx, xgH2O);
//              fluidState.setMoleFraction(nPhaseIdx, O2Idx, xgO2);
//              fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-xgH2O-xgO2);
                fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-xgH2O);


        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);

            fluidState.setDensity(phaseIdx, rho);
            fluidState.setViscosity(phaseIdx, mu);
        }
    }
    Scalar salinity() const
    {
        std::cout <<"test: function Scalar salinity() used"<<std::endl;
        return salinity_; }

    Scalar moleFracSalinity() const
    { return moleFracSalinity_; }

    Scalar pH() const
    { return pH_; }

    Scalar solidity(int phaseIdx) const
    { return solidity_[phaseIdx - numPhases]; }

    Scalar InitialPorosity() const
    { return initialPorosity_;}

    Scalar porosity() const
    { return porosity_; }

    Scalar permFactor() const
    { return permFactor_; }

    Scalar moleFraction(int phaseIdx, int compIdx) const
    { return fluidState_.moleFraction(phaseIdx, compIdx); }

    Scalar massFraction(int phaseIdx, int compIdx) const
    { return fluidState_.massFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the phase state for the control-volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

//    /*!
//     * \copydoc 2pncMin::saturation
//     */
//    Scalar saturation(int phaseIdx) const
//    { return fluidState_.saturation(phaseIdx); }

    /*!
     * \copydoc 2pncMin::density
     */
    Scalar density(int phaseIdx) const
    {
        if (phaseIdx<numPhases)
            return fluidState_.density(phaseIdx);

        else if (phaseIdx<numPhases+numSPhases)
                return FluidSystem::solidPhaseDensity(phaseIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            return 1;
    }

    /*!
     * \copydoc 2pncMin::molarDensity
     */
    Scalar molarDensity(int phaseIdx) const
    {
        if (phaseIdx<numPhases)
            return fluidState_.molarDensity(phaseIdx);

        else if (phaseIdx<numPhases+numSPhases)
            return FluidSystem::solidPhaseDensity(phaseIdx)/FluidSystem::molarMassMineral(phaseIdx); //TODO
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            return 1;
    }

    /*!
     * \copydoc 2pncMin::pressure
     */
    Scalar pressure(int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \copydoc 2pncMin::temperature
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }


    /*!
     * \copydoc 2pncMin::capillaryPressure
     */
    Scalar capillaryPressure() const
    { return fluidState_.pressure(nPhaseIdx) - fluidState_.pressure(wPhaseIdx); }

//protected:

  static Scalar temperature_(const PrimaryVariables &priVars,
                            const Problem& problem,
                            const Element &element,
                            const FVElementGeometry &elemGeom,
                            int scvIdx)
    {
        return problem.temperature(element, elemGeom, scvIdx);
    }

    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            int phaseIdx)
    {
        return 0;
    }
protected:


    Scalar solidity_[numSPhases];
    FluidState fluidState_;
    Scalar salinity_;
    Scalar moleFracSalinity_;
    Scalar pH_;
    Scalar rprec_;
    Scalar rurea_;
    Scalar Appa_Ksp_;
    Scalar Omega_;
    Scalar initialPorosity_;
    Scalar porosity_;
    Scalar permFactor_;

private:
    Implementation &asImp()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp() const
    { return *static_cast<const Implementation*>(this); }

    static Scalar massToMoleFrac_(Scalar XlNaCl)
    {
       const Scalar Mw = FluidSystem::molarMass(wCompIdx);  // 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58e-3; /* molecular weight of NaCl  [kg/mol] */
       const Scalar X_NaCl = XlNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       const Scalar xlNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xlNaCl;
    }
};

} // end namespace

#endif
