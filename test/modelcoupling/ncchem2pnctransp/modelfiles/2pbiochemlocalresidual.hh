// $Id: 2p2clocalresidual.hh 3795 2010-06-25 16:08:04Z melanie $
/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Bernd Flemisch                               *
 *   Copyright (C) 2009-2010 by Andreas Lauser                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase biomineralisation box model.
 */

#ifndef DUMUX_2PBIOCHEM_LOCAL_RESIDUAL_BASE_HH
#define DUMUX_2PBIOCHEM_LOCAL_RESIDUAL_BASE_HH

//#include <dumux/boxmodels/common/boxmodel.hh>
//#include <dumux/implicit/box/boxlocalresidual.hh>
#include <dumux/common/math.hh>

#include "dumux/implicit/2pncmin/2pncminproperties.hh"
#include "dumux/implicit/2pncmin/2pncminindices.hh"

#include "dumux/implicit/2pbiomin/2pbiominvolumevariables.hh"

#include "dumux/implicit/2pbiomin/2pbiominfluxvariables.hh"
//#include "dumux/implicit/2pncmin/2pncminfluxvariables.hh"

//#include "dumux/implicit/2pncmin/2pncminnewtoncontroller.hh"
#include "dumux/implicit/2pncmin/2pncminlocalresidual.hh"

#include <iostream>
#include <vector>

//#define VELOCITY_OUTPUT 1 // uncomment this line if an output of the velocity is needed

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinModel
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase n-component mineralisation box model.
 *
 * This class is used to fill the gaps in 2pncMinLocalResidual for the 2PBioMin flow.
 */
template<class TypeTag>
class TwoPBioChemLocalResidual: public TwoPNCMinLocalResidual<TypeTag>
{
protected:
    typedef TwoPBioChemLocalResidual<TypeTag> ThisType;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(LocalResidual)) Implementation;
    typedef TwoPNCMinLocalResidual<TypeTag> ParentType;
    typedef BoxLocalResidual<TypeTag> BaseClassType;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Problem)) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridView)) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(SolutionVector)) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(ElementSolutionVector)) ElementSolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(PrimaryVariables)) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(BoundaryTypes)) BoundaryTypes;

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Indices)) Indices;

    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, PTAG(NumEq)),
        numPhases = GET_PROP_VALUE(TypeTag, PTAG(NumPhases)),
        numSPhases = GET_PROP_VALUE(TypeTag, PTAG(NumSPhases)),
        numComponents = GET_PROP_VALUE(TypeTag, PTAG(NumComponents)),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
//        bPhaseIdx = FluidSystem::bPhaseIdx,
//        cPhaseIdx = FluidSystem::cPhaseIdx,
//
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
//
//        BrineIdx = wCompIdx,
//        TCIdx = nCompIdx,
//        NaIdx = FluidSystem::NaIdx,
//        ClIdx = FluidSystem::ClIdx,
//        CaIdx = FluidSystem::CaIdx,
//        UreaIdx = FluidSystem::UreaIdx,
//        TNHIdx = FluidSystem::TNHIdx,
//        O2Idx = FluidSystem::O2Idx,
//        BiosubIdx = FluidSystem::BiosubIdx,
//        BiosuspIdx = FluidSystem::BiosuspIdx,
//
//        NH4Idx = FluidSystem::NH4Idx,
//        CO3Idx = FluidSystem::CO3Idx,
//        HCO3Idx = FluidSystem::HCO3Idx,
//        CO2Idx = FluidSystem::CO2Idx,
//        HIdx = FluidSystem::HIdx,
//        OHIdx = FluidSystem::OHIdx,
//
//        BiofilmIdx = FluidSystem::BiofilmIdx,
//        CalciteIdx = FluidSystem::CalciteIdx,
//
        conti0EqIdx = Indices::conti0EqIdx,
//
//        xlTCIdx = nCompIdx,
//        xlNaIdx = NaIdx,
//        xlClIdx = ClIdx,
//        xlCaIdx = CaIdx,
//        xlUreaIdx = UreaIdx,
//        xlTNHIdx = TNHIdx,
//        xlO2Idx = O2Idx,
//        xlBiosubIdx = BiosubIdx,
//        xlBiosuspIdx = BiosuspIdx,
//        phiBiofilmIdx = numComponents,
//        phiCalciteIdx = numComponents +1,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;


    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
//    typedef Dune::FieldVector<CoordScalar, numComponents> CompVector;
//    typedef Dune::FieldMatrix<CoordScalar, numPhases, numComponents> PhaseCompMatrix;
    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

public:
    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    TwoPBioChemLocalResidual()
    {
        // retrieve the upwind weight for the mass conservation equations. Use the value
        // specified via the property system as default, and overwrite
        // it by the run-time parameter from the Dune::ParameterTree
        massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
    };


//    /*!
//     * \brief Evaluate the storage term of the current solution in a
//     *        single phase.
//     *
//     * \param element The element
//     * \param phaseIdx The index of the fluid phase
//     */
//    void evalPhaseStorage(const Element &element, int phaseIdx)
//    {
//        FVElementGeometry fvGeometry;
//        fvGeometry.update(this->gridView_(), element);
//        ElementBoundaryTypes bcTypes;
//        bcTypes.update(this->problem_(), element, fvGeometry);
//        ElementVolumeVariables volVars;
//        volVars.update(this->problem_(), element, fvGeometry, false);
//
//        this->residual_.resize(fvGeometry.numScv);
//        this->residual_ = 0;
//
//        this->elemPtr_ = &element;
//        this->fvElemGeomPtr_ = &fvGeometry;
//        this->bcTypesPtr_ = &bcTypes;
//        this->prevVolVarsPtr_ = 0;
//        this->curVolVarsPtr_ = &volVars;
//        evalPhaseStorage_(phaseIdx);
//    }
//
//    /*!
//     * \brief Evaluate the amount all conservation quantities
//     *        (e.g. phase mass) within a sub-control volume.
//     *
//     * The result should be averaged over the volume (e.g. phase mass
//     * inside a sub control volume divided by the volume)
//     *
//     *  \param result The mass of the component within the sub-control volume
//     *  \param scvIdx The SCV (sub-control-volume) index
//     *  \param usePrevSol Evaluate function with solution of current or previous time step
//     */
//    void computeStorage(PrimaryVariables &storage, int scvIdx, bool usePrevSol) const
//    {
//        // if flag usePrevSol is set, the solution from the previous
//        // time step is used, otherwise the current solution is
//        // used. The secondary variables are used accordingly.  This
//        // is required to compute the derivative of the storage term
//        // using the implicit euler method.
//        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_()
//                : this->curVolVars_();
//        const VolumeVariables &volVars = elemVolVars[scvIdx];
//
//        // compute storage term of all fluid components in the fluid phases
//        storage = 0;
//        for (int phaseIdx = 0; phaseIdx < numPhases + numSPhases; ++phaseIdx)
//        {
//         if(phaseIdx< numPhases)
//          {
//            for (int compIdx = 0; compIdx < numComponents; ++compIdx) //Air, H2O, NaCl
//              {
//                  int eqIdx = conti0EqIdx + compIdx;
//
//                  storage[eqIdx] += volVars.molarDensity(phaseIdx)
//                                   * volVars.saturation(phaseIdx)
//                                   * volVars.moleFraction(phaseIdx, compIdx)
//                                   * volVars.porosity();
//              }
//          }
//       // compute storage term of all solid phases
//         if(phaseIdx>=numPhases)
//          {
//           int eqIdx = conti0EqIdx + numComponents-numPhases + phaseIdx;
//           storage[eqIdx] += volVars.solidity(phaseIdx)*volVars.molarDensity(phaseIdx);
//           }
//       }
//        Valgrind::CheckDefined(storage);
//    }

    /*!
     * the 2pBioChemModel does not account for any fluxes.
     */
    void computeFlux(PrimaryVariables &flux, const int faceIdx, bool onBoundary=false) const
    {
        FluxVariables vars(this->problem_(),
                      this->element_(),
                      this->fvGeometry_(),
                      faceIdx,
                      this->curVolVars_(),
                      onBoundary);


//        flux = 0;
//        asImp_()->computeAdvectiveFlux(flux, vars);
//        asImp_()->computeDiffusiveFlux(flux, vars);
//        Valgrind::CheckDefined(flux);
    }
//    /*!
//     * \copydoc 2pncMin::computeAdvectiveFlux
//     */
//    void computeAdvectiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
//    {
//      //the chemistry only model does not account for advective flux:
//        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
//        {
//         for (int compIdx = 0; compIdx < numComponents; ++compIdx)
//       {
//           flux[conti0EqIdx + compIdx] += 0;
//
//       }
//      }
//    }

//    /*!
//     * \copydoc 2pncMin::computeDiffusiveFlux
//     *
//     * \ for the 2pBioMin model, the diffusive flux is replaced by a dispersive flux
//     */
//    void computeDiffusiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
//    {
//        //the chemistry only model does not account for diffusive flux:
//        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
//          for (int compIdx = 0; compIdx < numComponents; ++compIdx)
//          {
//              flux[conti0EqIdx + compIdx] += 0;
//          }
//
//    }

    /*!
     * \copydoc 2pncMin::computeSource
     *
     * \additionally the sources due to reactions are calculated
     */
    void computeSource(PrimaryVariables &source, const Element &element, FVElementGeometry &fvGeometry, int localVertexIdx, VolumeVariables &volVars)
   {

//       const ElementVolumeVariables &elemVolVars = this->curVolVars_();
//       const VolumeVariables &volVars = elemVolVars[localVertexIdx];

        PrimaryVariables externalSource;
        PrimaryVariables reactionSource;

        this->problem_().source(externalSource,
                element,
                fvGeometry,
                localVertexIdx);
//      this->problem_().source(externalSource,
//                this->element_(),
//                this->fvGeometry_(),
//                  localVertexIdx);

//     FluxVariables fluxVars(this->problem_(),
//                                   this->element_(),
//                                   this->fvGeometry_(),       //TODO use fluxvars?
//                                   localVertexIdx,
//                                   volVars);
//
//     Scalar absgradpw = fluxVars.absgradp(wPhaseIdx);
        Scalar absgradpw = 100;
//     this->problem_().reactionSource(reactionSource,
//              this->element_(),
//              this->fvGeometry_(),
//              localVertexIdx,
//              volVars,
//              absgradpw); //,fluxVars);
     this->problem_().reactionSource(reactionSource,
        element,
        fvGeometry,
        localVertexIdx,
        volVars,
        absgradpw); //,fluxVars);

        source = externalSource + reactionSource;

      Valgrind::CheckDefined(source);
   }
    /*!
     * \brief Calculate the reactive source terms of the equations
     * \This is a quick and dirty way to decouple the reactions from transport as this is done only when the transport update succeeded.
     *
     * \param source The source/sink in the SCV for each component
     * \param localVertexIdx The index of the SCV
     * \param element The element
     * \param fvGeometry The Geometry of the Finite Volume Element
     * \param volVars The VolumeVariables of the SCV
     * \param mindDt determines whether the reactive sources are resticted to the timestepsize or not
     */
    void computeDecReactionSource(PrimaryVariables &source, Element &element, FVElementGeometry &fvGeometry, int localVertexIdx, VolumeVariables &volVars, bool mindDt)
    {
        Scalar absgradpw = 1000;
        if(mindDt==true)
        {
            FluxVariables fluxVars(this->problem_(),
                    this->element_(),
                    this->fvGeometry_(),
                    localVertexIdx,
                    this->curVolVars_());

            absgradpw = fluxVars.absgradp(wPhaseIdx);
        }
     this->problem_().decReactionSource(source,
                element,
                fvGeometry,
                localVertexIdx,
                volVars,
                absgradpw,
                mindDt); //,fluxVars);


      Valgrind::CheckDefined(source);
   }
    /*!
     * \copydoc 2pncMin::evalOutflowSegment
     */
    void evalOutflowSegment(const IntersectionIterator &isIt,
                            const int scvIdx,
                            const int boundaryFaceIdx)
    {
        const BoundaryTypes &bcTypes = this->bcTypes_(scvIdx);

        // deal with outflow boundaries
        if (bcTypes.hasOutflow())
        {
            PrimaryVariables values(0.0);
//            asImp_()->computeFlux(values, boundaryFaceIdx, true);
            asImp_().computeFlux(values, boundaryFaceIdx, true);
            Valgrind::CheckDefined(values);

            for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
            {
                if (!bcTypes.isOutflow(eqIdx) )
                    continue;
                this->residual_[scvIdx][eqIdx] += values[eqIdx];
            }
        }
    }

protected:

    void evalPhaseStorage_(int phaseIdx)
    {
        // evaluate the storage terms of a single phase
        for (int i=0; i < this->fvGeometry_().numScv; i++) {
            PrimaryVariables &result = this->residual_[i];
            const ElementVolumeVariables &elemVolVars = this->curVolVars_();
            const VolumeVariables &volVars = elemVolVars[i];

            // compute storage term of all fluid components within all phases
            result = 0;
            if(phaseIdx >= numPhases) //solid Phase
            {
                result[conti0EqIdx + numComponents - numPhases + phaseIdx] += volVars.solidity(phaseIdx)
                                                * volVars.density(phaseIdx);
            }

            else //fluid phase
             {
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                  result[conti0EqIdx + compIdx] += volVars.density(phaseIdx)
                                    * volVars.saturation(phaseIdx)
                                    * volVars.massFraction(phaseIdx, compIdx)
                                    * volVars.porosity();
                 }
             }
         result *= this->fvGeometry_().subContVol[i].volume;
      }
    }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

private:
   Scalar massUpwindWeight_;
};

} // end namepace

#endif
