// $Id: 2p2cmodel.hh 5093 2011-01-23 18:05:49Z claude $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
* \file
*
* \brief Adaption of the BOX scheme to the two-phase biomineralisation flow model.
*/

#ifndef DUMUX_2PBIOCHEM_MODEL_HH
#define DUMUX_2PBIOCHEM_MODEL_HH
#define VELOCITY_OUTPUT

#include "dumux/implicit/2pncmin/2pncminproperties.hh"
#include "2pbiochemlocalresidual.hh"
#include "dumux/implicit/2pncmin/2pncminlocalresidual.hh"
#include "dumux/implicit/2pncmin/2pncminpropertydefaults.hh"
#include "dumux/implicit/2pncmin/2pncminmodel.hh"

#include <boost/format.hpp>
#include <cmath>

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinModelBoxModels
 * \defgroup TwoPBioMinModel Two-phase biomineralisation box model
 */

/*!
 * \ingroup TwoPBioMinModel
 * \brief Adaption of the BOX scheme to the two-phase biomineralisation flow model.
 *
 * This model implements two-phase n-component mineralisation flow of two compressible and
 * partially miscible fluids \f$\alpha \in \{ w, n \}\f$ composed of the n components
 * \f$\kappa \in \{ w, a \}\f$. The standard multiphase Darcy
 * approach is used as the equation for the conservation of momentum:
 * \f[
 v_\alpha = - \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 \left(\text{grad}\, p_\alpha - \varrho_{\alpha} \mbox{\bf g} \right)
 * \f]
 *
 * By inserting this into the equations for the conservation of the
 * components, one gets one transport equation for each component
 * \f{eqnarray}
 && \phi \frac{\partial (\sum_\alpha \varrho_\alpha X_\alpha^\kappa S_\alpha )}
 {\partial t}
 - \sum_\alpha  \text{div} \left\{ \varrho_\alpha X_\alpha^\kappa
 \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 (\text{grad}\, p_\alpha - \varrho_{\alpha}  \mbox{\bf g}) \right\}
 \nonumber \\ \nonumber \\
    &-& \sum_\alpha \text{div} \left\{{\bf D_{\alpha, pm}^\kappa} \varrho_{\alpha} \text{grad}\, X^\kappa_{\alpha} \right\}
 - \sum_\alpha q_\alpha^\kappa = 0 \qquad \kappa \in \{w, a\} \, ,
 \alpha \in \{w, g\}
 \f}
 *
 * This is discretized using a fully-coupled vertex
 * centered finite volume (box) scheme as spatial and
 * the implicit Euler method as temporal discretization.
 *
 * By using constitutive relations for the capillary pressure \f$p_c =
 * p_n - p_w\f$ and relative permeability \f$k_{r\alpha}\f$ and taking
 * advantage of the fact that \f$S_w + S_n = 1\f$ and \f$X^\kappa_w + X^\kappa_n = 1\f$, the number of
 * unknowns can be reduced to two.
 * The used primary variables are, like in the two-phase model, either \f$p_w\f$ and \f$S_n\f$
 * or \f$p_n\f$ and \f$S_w\f$. The formulation which ought to be used can be
 * specified by setting the <tt>Formulation</tt> property to either
 * TwoPTwoCIndices::pWsN or TwoPTwoCIndices::pNsW. By
 * default, the model uses \f$p_w\f$ and \f$S_n\f$.
 * Moreover, the second primary variable depends on the phase state, since a
 * primary variable switch is included. The phase state is stored for all nodes
 * of the system. Following cases can be distinguished:
 * <ul>
 *  <li> Both phases are present: The saturation is used (either \f$S_n\f$ or \f$S_w\f$, dependent on the chosen <tt>Formulation</tt>),
 *      as long as \f$ 0 < S_\alpha < 1\f$</li>.
 *  <li> Only wetting phase is present: The mass fraction of, e.g., air in the wetting phase \f$X^a_w\f$ is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^a_w<X^a_{w,max}\f$)</li>
 *  <li> Only non-wetting phase is present: The mass fraction of, e.g., water in the non-wetting phase, \f$X^w_n\f$, is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^w_n<X^w_{n,max}\f$)</li>
 * </ul>
 */

template<class TypeTag>
class TwoPBioChemModel: public TwoPNCMinModel<TypeTag>
{
    typedef TwoPBioChemModel<TypeTag> ThisType;
    typedef TwoPNCMinModel<TypeTag> ParentType;
    typedef ImplicitModel<TypeTag> BaseClassType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VertexMapper) VertexMapper;
    typedef typename GET_PROP_TYPE(TypeTag, ElementMapper) ElementMapper;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, NewtonMethod) NewtonMethod;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) NewtonController;


    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::lPhaseIdx,
        nPhaseIdx = FluidSystem::gPhaseIdx,
        bPhaseIdx = FluidSystem::bPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        CO2Idx  = FluidSystem::CO2Idx,
        UreaIdx = FluidSystem::UreaIdx,
        CaIdx = FluidSystem::CaIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;
    typedef Dune::FieldVector<Scalar, numPhases> PhasesVector;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
        enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \copydoc 2pncMin::init
     */
    void init(Problem &problem)
    {
        ParentType::init(problem);
        try
    {
            plausibilityTolerance_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, PlausibilityTolerance);
            pHMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMax);
            pHMin_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMin);
            // retrieve the upwind weight for the mass conservation equations. Use the value
            // specified via the property system as default, and overwrite
            // it by the run-time parameter from the Dune::ParameterTree
            massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
    }

    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
        unsigned numDofs = this->numDofs();

        staticDat_.resize(numDofs);
        velocity_.resize(numDofs);

        setSwitched_(false);

        // check, if velocity output can be used (works only for cubes so far)
        velocityOutput_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, AddVelocity);
        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            if (elemIt->geometry().type().isCube() == false){
                velocityOutput_ = false;
            }

            if (!isBox) // i.e. cell-centered discretization
            {
                velocityOutput_ = false;

                int dofIdxGlobal = this->dofMapper().map(*elemIt);
                const GlobalPosition &globalPos = elemIt->geometry().center();

                // initialize phase presence
                staticDat_[dofIdxGlobal].phasePresence
                    = this->problem_().initialPhasePresence(*(this->gridView_().template begin<dim>()),
                                                            dofIdxGlobal, globalPos);
                staticDat_[dofIdxGlobal].wasSwitched = false;

                staticDat_[dofIdxGlobal].oldPhasePresence
                    = staticDat_[dofIdxGlobal].phasePresence;

                velocity_[dofIdxGlobal] = 0.0;
            }
        }

        if (velocityOutput_ != GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, AddVelocity))
            std::cout << "ATTENTION: Velocity output only works for cubes and is set to false for simplices\n";

        if (isBox) // i.e. vertex-centered discretization
        {
            VertexIterator vIt = this->gridView_().template begin<dim> ();
            const VertexIterator &vEndIt = this->gridView_().template end<dim> ();
            for (; vIt != vEndIt; ++vIt)
            {
                int dofIdxGlobal = this->dofMapper().map(*vIt);
                const GlobalPosition &globalPos = vIt->geometry().corner(0);

                // initialize phase presence
                staticDat_[dofIdxGlobal].phasePresence
                    = this->problem_().initialPhasePresence(*vIt, dofIdxGlobal,
                                                        globalPos);
                staticDat_[dofIdxGlobal].wasSwitched = false;

                staticDat_[dofIdxGlobal].oldPhasePresence
                    = staticDat_[dofIdxGlobal].phasePresence;

                velocity_[dofIdxGlobal] = 0.0;
            }
        }
    }

    /*!
     * \copydoc 2pncMin::globalPhaseStorage
     */
    void globalPhaseStorage(PrimaryVariables &dest, int phaseIdx)
    {
        dest = 0;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        const ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            this->localResidual().evalPhaseStorage(*elemIt, phaseIdx);
            for (int i = 0; i < elemIt->template count<dim>(); ++i)
                dest += this->localResidual().residual(i);
        };

        this->gridView_().comm().sum(dest);
    }

     /*!
     * \copydoc Implicit::relativeErrorDof
     */
    Scalar relativeErrorDof(const int dofIdx,
                            const PrimaryVariables &priVars1,
                            const PrimaryVariables &priVars2)
    {
        Scalar result = 0.0;
        for (int j = 0; j < numEq; ++j) {
//            Scalar eqErr = std::log(std::abs(priVars1[j] - priVars2[j]));
            Scalar eqErr = std::abs(priVars1[j] - priVars2[j]);
            Scalar refValue = this->problem_().refValue(j);
//            eqErr /= std::max<Scalar>(1.0, std::log(std::abs(priVars1[j] + priVars2[j])/2));
//            eqErr /= std::max<Scalar>(1.0, std::abs(priVars1[j] + priVars2[j])/2);
            eqErr /= std::max<Scalar>(std::min<Scalar>(1.0, refValue), std::abs(priVars1[j] + priVars2[j])/2);
//              eqErr /= std::abs(priVars1[j] + priVars2[j])/2;
            result = std::max(result, eqErr);
        }
        return result;
    }


    /*!
     * \copydoc Implicit::checkPlausibility
     */
    void checkPlausibility() const
    {
         // Looping over all elements of the domain
                ElementIterator eEndIt = this->problem_().gridView().template end<0>();
                for (ElementIterator eIt = this->problem_().gridView().template begin<0>() ; eIt not_eq eEndIt; ++eIt)
                {
                    ElementVolumeVariables elemVolVars;
                    FVElementGeometry fvGeometry;

                    // updating the volume variables
                    fvGeometry.update(this->problem_().gridView(), *eIt);
                    elemVolVars.update(this->problem_(), *eIt, fvGeometry, false);

                    std::stringstream  message ;
                    // number of scv
                    const unsigned int numScv = fvGeometry.numScv; // box: numSCV, cc:1

                    for (unsigned int scvIdx = 0; scvIdx < numScv; ++scvIdx) {

                        const FluidState & fluidState = elemVolVars[scvIdx].fluidState();

                        // mass Check
                        const Scalar eps = plausibilityTolerance_ ;
                        for (int compIdx=0; compIdx< numComponents+numSecComponents; ++ compIdx){
                            const Scalar xTest = elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx);

                            if (not std::isfinite(xTest) or xTest < 0.-eps or xTest > 1.+eps ){
                                message <<"\nUnphysical Value in Mass: \n";

                                message << "\tx" <<"_w"
                                        <<"^"<<FluidSystem::componentName(compIdx)<<"="
                                        << elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx) <<"\n";
                            }
                        }

                        // check chemistry by pH
                        Scalar pH = elemVolVars[scvIdx].pH();
                        if (not std::isfinite(pH) or pH < pHMin_ or pH > pHMax_ ){
                                                        message <<"\nUnphysical Value in pH: \n";

                                                        message << "pH = " << elemVolVars[scvIdx].pH() <<"\n";
                        }

                        // Some check wrote into the error-message, add some additional information and throw
                        if (not message.str().empty()){
                            // Getting the spatial coordinate
                            const GlobalPosition & globalPosCurrent = fvGeometry.subContVol[scvIdx].global;
                            std::stringstream positionString ;

                            // Add physical location
                            positionString << "Here:";
                            for(int i=0; i<dim; i++)
                                positionString << " x"<< (i+1) << "="  << globalPosCurrent[i] << " "   ;
                            message << "Unphysical value found! \n" ;
                            message << positionString.str() ;
                            message << "\n";

                            message << " Here come the primary Variables:" << "\n" ;
                            for(unsigned int priVarIdx =0 ; priVarIdx<numEq; ++priVarIdx){
                                message << "priVar[" << priVarIdx << "]=" << elemVolVars[scvIdx].priVar(priVarIdx) << "\n";
                            }
                            DUNE_THROW(NumericalProblem, message.str());
                        }
                    } // end scv-loop
                } // end element loop


    }

    /*!
     * \brief Try to progress the model to the next timestep.
     *
     * \param solver The non-linear solver
     * \param controller The controller which specifies the behaviour
     *                   of the non-linear solver
     */
    bool update(NewtonMethod &solver,
                NewtonController &controller)
    {
#if HAVE_VALGRIND
        for (size_t i = 0; i < this->curSol().size(); ++i)
            Valgrind::CheckDefined(this->curSol()[i]);
#endif // HAVE_VALGRIND
            //original from implicitmodel:
//        asImp_().updateBegin();
//
//        bool converged = solver.execute(controller);
//        if (converged) {
//            asImp_().updateSuccessful();
//        }
//        else
//            asImp_().updateFailed();
        SolutionVector source = this->curSol();

//        std::cout<<"source = curSol =\n"<<source<<std::endl;

        for (unsigned i = 0; i < staticDat_.size(); ++i)
            staticDat_[i].visited = false;

        bool converged = true;

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        const ElementIterator elemEndIt = this->gridView_().template end<0>();

        for (; elemIt != elemEndIt; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);

            int numScv = elemIt->template count<dim> ();
            for (int i = 0; i < numScv; ++i)
            {

                int dofIdxGlobal = this->vertexMapper().map(*elemIt, i, dim);

                if (staticDat_[dofIdxGlobal].visited)
                    continue;

                volVars.update(source[dofIdxGlobal],        //update with the solution from the transport computation
                   this->problem_(),
                   *elemIt,
                   fvGeometry,
                   i,
                   false);

                this->localResidual_().computeSource(source[dofIdxGlobal], //compute the chemistry of the current solution
                                                    *elemIt,
                                                    fvGeometry,
                                                    i,
                                                    volVars);

                //sources are calculated as mol/m³/s --> source for one timestep *= dt/(molarDensity(wPhase)*Sw*porosity
                Scalar dt = this->problem_().timeManager().timeStepSize();
                Scalar molarDensity = volVars.molarDensity(wPhaseIdx);
                Scalar porosity = volVars.porosity();
                Scalar Sw = volVars.saturation(wPhaseIdx);

                for (int compIdx=wCompIdx; compIdx < numComponents; ++compIdx)  //nosource and sink for water!
                {
                    source[dofIdxGlobal][compIdx] *= dt/(molarDensity*Sw*porosity);
                    if(this->curSol()[dofIdxGlobal][compIdx] + source[dofIdxGlobal][compIdx]< 0.0)
                    {
                        converged = false;
                    }
                }

                //the solid components are single component phases, thus their aggregated source for one timestep is *= dt/(molarDensity(solidPhase)
                source[dofIdxGlobal][numComponents] *= dt/volVars.molarDensity(bPhaseIdx);
                source[dofIdxGlobal][numComponents+1] *= dt/volVars.molarDensity(cPhaseIdx);

                staticDat_[dofIdxGlobal].visited = true;
            }
        }

        this->curSol() += source;

//        std::cout<<"source = real source =\n"<<source<<std::endl;

//      bool converged = true;
//      for (; elemIt != elemEndIt; ++elemIt)
//      {
//          if (converged==false)
//              continue;
//
//          fvGeometry.update(this->gridView_(), *elemIt);
//
//          int numVerts = elemIt->template count<dim> ();
//          for (int i = 0; i < numVerts; ++i)
//          {
//              int dofIdxGlobal = this->vertexMapper().map(*elemIt, i, dim);
//                if (staticDat_[dofIdxGlobal].visited)
//                    continue;
//
//              for (int compIdx = 0; compIdx<numComponents+numSPhases; ++compIdx)
//              {
//                  if(this->curSol()[dofIdxGlobal][compIdx] < 0.0)
//                      converged = false;
//              }
//
//              staticDat_[dofIdxGlobal].visited = true;
//          }
//      }
        if (!converged)
            updateFailed();

#if HAVE_VALGRIND
        for (size_t i = 0; i < this->curSol().size(); ++i) {
            Valgrind::CheckDefined(this->curSol()[i]);
        }
#endif // HAVE_VALGRIND

        return converged;
    }


    /*!
     * \copydoc 2pncMin::updateFailed
     */
    void updateFailed()
    {
        ParentType::updateFailed();

        setSwitched_(false);
        resetPhasePresence_();
        /*this->localJacobian().updateStaticData(this->curSolFunction(),
         this->prevSolFunction());
         */
    };

    /*!
     * \copydoc 2pncMin::advanceTimeLevel
     */
    void advanceTimeLevel()
    {
        ParentType::advanceTimeLevel();

        // update the phase state
        updateOldPhasePresence_();
        setSwitched_(false);
    }

    /*!
     * \copydoc 2pncMin::switched
     */
    bool switched() const
    {
        return switchFlag_;
    }

    /*!
     * \copydoc 2pncMin::phasePresence
     */
    int phasePresence(int dofIdxGlobal, bool oldSol) const
    {
        return oldSol ? staticDat_[dofIdxGlobal].oldPhasePresence
                : staticDat_[dofIdxGlobal].phasePresence;
    }

    /*!
     * \returns the velocity to be used in the VolumeVariables to calculate the detachment rate for the output
     */
    Scalar velocity(int dofIdxGlobal) const
    {
        Scalar v = 0;
        for (int i=0;i<dim;i++)
        {
            v += velocity_[dofIdxGlobal] * velocity_[dofIdxGlobal];
        }
        return sqrt(v);
    }

    /*!
     * \copydoc 2pncMin::addOutputVtkFields
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
//        typedef Dune::BlockVector<Dune::FieldVector<double, dim> > VectorField;

        // get the number of degrees of freedom
        unsigned numDofs = this->numDofs();

        // velocity output currently only works for the box discretization
        if (!isBox)
           velocityOutput_ = false;

        // create the required scalar fields
//        unsigned numScv = this->problem_().gridView().size(dim);

        ScalarField *Sg            = writer.allocateManagedBuffer (numDofs);
        ScalarField *Sl            = writer.allocateManagedBuffer (numDofs);
        ScalarField *pg            = writer.allocateManagedBuffer (numDofs);
        ScalarField *pl            = writer.allocateManagedBuffer (numDofs);
        ScalarField *pc            = writer.allocateManagedBuffer (numDofs);
        ScalarField *rhoL          = writer.allocateManagedBuffer (numDofs);
        ScalarField *rhoG          = writer.allocateManagedBuffer (numDofs);
        ScalarField *mobL          = writer.allocateManagedBuffer (numDofs);
        ScalarField *mobG          = writer.allocateManagedBuffer (numDofs);
        ScalarField *phasePresence = writer.allocateManagedBuffer (numDofs);
        ScalarField *temperature   = writer.allocateManagedBuffer (numDofs);
        ScalarField *poro          = writer.allocateManagedBuffer (numDofs);
        ScalarField *boxVolume     = writer.allocateManagedBuffer (numDofs);
        ScalarField *potential     = writer.allocateManagedBuffer (numDofs);
        ScalarField *pH            = writer.allocateManagedBuffer (numDofs);

        ScalarField *Omega             = writer.allocateManagedBuffer (numDofs);
//        ScalarField *Appa_Ksp            = writer.allocateManagedBuffer (numDofs);
//        ScalarField *rprec               = writer.allocateManagedBuffer (numDofs);
        ScalarField *rurea             = writer.allocateManagedBuffer (numDofs);
////        ScalarField *mue               = writer.allocateManagedBuffer (numDofs);

        ScalarField *rattachChem= writer.allocateManagedBuffer (numDofs);
        ScalarField *rdetachChem= writer.allocateManagedBuffer (numDofs);
        ScalarField *rgrowthChem= writer.allocateManagedBuffer (numDofs);
        ScalarField *rdecayChem= writer.allocateManagedBuffer (numDofs);

        ScalarField *solidity[numSPhases] ;
        for (int phaseIdx = 0; phaseIdx < numSPhases; ++phaseIdx)
        {
            solidity[phaseIdx]= writer.allocateManagedBuffer (numDofs);
        }

        ScalarField *massFraction[numPhases][numComponents + numSecComponents];
        for (int i = 0; i < numPhases; ++i)
            for (int j = 0; j < numComponents + numSecComponents; ++j)
                massFraction[i][j] = writer.allocateManagedBuffer (numDofs);

        ScalarField *molarity[numComponents + numSecComponents];
        for (int j = 0; j < numComponents + numSecComponents ; ++j)
            molarity[j] = writer.allocateManagedBuffer (numDofs);

        ScalarField *intrinsicPerm = writer.allocateManagedBuffer (numDofs);
//        ScalarField *intrinsicPerm[dim];
//        for (int j = 0; j < dim; ++j) //Permeability only in main directions xx and yy
//            intrinsicPerm[j] = writer.allocateManagedBuffer (numDofs);

        ScalarField *Da_urea = writer.allocateManagedBuffer (numDofs);
        ScalarField *Da_prec = writer.allocateManagedBuffer (numDofs);

        *boxVolume = 0;

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded

#warning potential memory leak is possible here if the reserved memory for z velocity is not set free
        ScalarField *velocityX = writer.allocateManagedBuffer (numDofs);
        ScalarField *velocityY = writer.allocateManagedBuffer (numDofs);
        ScalarField *velocityZ = writer.allocateManagedBuffer (numDofs);

        // initialize velocity fields
        Scalar boxSurface[numDofs];
        for (int i = 0; i < numDofs; ++i)
        {
            velocity_[i] = 0;
            (*velocityX)[i] = 0;
            if (dim > 1)
            (*velocityY)[i] = 0;
            if (dim > 2)
            (*velocityZ)[i] = 0;
            boxSurface[i] = 0.0; // initialize the boundary surface of the fv-boxes

            (*Da_urea)[i] = 0.0;
            (*Da_prec)[i] = 0.0;
        }
#endif

        unsigned numElements = this->gridView_().size(0);
        ScalarField *rank =
                writer.allocateManagedBuffer (numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            if(elemIt->partitionType() == Dune::InteriorEntity)
            {
                int idx = this->problem_().elementMapper().map(*elemIt);
                (*rank)[idx] = this->gridView_().comm().rank();
                fvGeometry.update(this->gridView_(), *elemIt);

                int numVerts = elemIt->template count<dim> ();
                for (int i = 0; i < numVerts; ++i)
                {
                    int dofIdxGlobal = this->vertexMapper().map(*elemIt, i, dim);
                    volVars.update(sol[dofIdxGlobal],
                                   this->problem_(),
                                   *elemIt,
                                   fvGeometry,
                                   i,
                                   false);

                GlobalPosition globalPos = fvGeometry.subContVol[i].global;
                Scalar zmax = this->problem_().bBoxMax()[dim-1];
                (*Sg)[dofIdxGlobal]              = volVars.saturation(nPhaseIdx);
                (*Sl)[dofIdxGlobal]              = volVars.saturation(wPhaseIdx);
                (*pg)[dofIdxGlobal]              = volVars.pressure(nPhaseIdx);
                (*pl)[dofIdxGlobal]              = volVars.pressure(wPhaseIdx);
                (*pc)[dofIdxGlobal]          = volVars.capillaryPressure();
                (*rhoL)[dofIdxGlobal]            = volVars.density(wPhaseIdx);
                (*rhoG)[dofIdxGlobal]            = volVars.density(nPhaseIdx);
                (*mobL)[dofIdxGlobal]            = volVars.mobility(wPhaseIdx);
                (*mobG)[dofIdxGlobal]            = volVars.mobility(nPhaseIdx);
                (*boxVolume)[dofIdxGlobal]      += fvGeometry.subContVol[i].volume;
                (*poro)[dofIdxGlobal]            = volVars.porosity();
                (*pH)[dofIdxGlobal]              = volVars.pH();

                (*Omega)[dofIdxGlobal]           = volVars.Omega();
//                (*Appa_Ksp)[dofIdxGlobal]              = volVars.Appa_KSP();
//                (*rprec)[dofIdxGlobal]             = volVars.rprec();
                (*rurea)[dofIdxGlobal]           = volVars.rurea();
////                (*mue)[dofIdxGlobal]             = volVars.mue();
                    (*rattachChem)[dofIdxGlobal]             = volVars.rattachChem();
                    (*rdetachChem)[dofIdxGlobal]             = volVars.rdetachChem();
                    (*rgrowthChem)[dofIdxGlobal]             = volVars.rgrowthChem();
                    (*rdecayChem)[dofIdxGlobal]              = volVars.rdecayChem();

                for (int phaseIdx =  0; phaseIdx < numSPhases; ++phaseIdx)
                {
                    (*solidity[phaseIdx])[dofIdxGlobal]= volVars.precipitateVolumeFraction(phaseIdx + numPhases);
                }
//                (*NaClMolality)[dofIdxGlobal]   = volVars.molalityNaCl();
                (*temperature)[dofIdxGlobal]     = volVars.temperature();
                (*phasePresence)[dofIdxGlobal]  = staticDat_[dofIdxGlobal].phasePresence;
                (*potential)[dofIdxGlobal]      = (volVars.pressure(wPhaseIdx)-1e5)
                                                /volVars.density(wPhaseIdx)
                                                /9.81
                                                - (zmax - globalPos[dim-1]);

                for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                    for (int compIdx = 0; compIdx < numComponents + numSecComponents; ++compIdx)
                    {
                        (*massFraction[phaseIdx][compIdx])[dofIdxGlobal]= volVars.massFraction(phaseIdx,compIdx);

                        Valgrind::CheckDefined((*massFraction[phaseIdx][compIdx])[dofIdxGlobal]);

//                        if (volVars.salinity() > 0.28)
//                                  DUNE_THROW(Dune::InvalidStateException, "MassFracNaCl is way large : " <<  volVars.salinity() << "Saturation"<< volVars.saturation(wPhaseIdx));
                    }
                for (int compIdx = 0; compIdx < numComponents + numSecComponents; ++compIdx)
                        (*molarity[compIdx])[dofIdxGlobal] = (volVars.molarity(wPhaseIdx, compIdx));


                const Tensor &Perm = this->problem_().spatialParams().intrinsicPermeability(*elemIt, fvGeometry, i);
                (*intrinsicPerm)[dofIdxGlobal] = Perm[0][0] * volVars.permeabilityFactor();
//                (*intrinsicPerm)[dofIdxGlobal] = this->problem_().spatialParams().intrinsicPermeability(*elemIt, fvGeometry, i) * volVars.permFactor();
//                for (int j = 0; j<dim; ++j)
//                    (*intrinsicPerm[j])[dofIdxGlobal] = Perm[j][j] * volVars.permFactor();
            };

    #ifdef VELOCITY_OUTPUT // check if velocity output is demanded
                // In the box method, the velocity is evaluated on the FE-Grid. However, to get an
                // average apparent velocity at the vertex, all contributing velocities have to be interpolated.
                GlobalPosition velocity(0.);
                ElementVolumeVariables elemVolVars;

                elemVolVars.update(this->problem_(),
                                   *elemIt,
                                   fvGeometry,
                                   false /* isOldSol? */);

                // loop over the phases
                for (int faceIdx = 0; faceIdx< fvGeometry.numScvf; faceIdx++)
                {
                    //prepare the flux calculations (set up and prepare geometry, FE gradients)
                    FluxVariables fluxDat(this->problem_(),
                                     *elemIt,
                                     fvGeometry,
                                     faceIdx,
                                     elemVolVars);

                    // choose phase of interest. Alternatively, a loop over all phases would be possible.
                    int phaseIdx = wPhaseIdx;

                    // get darcy velocity
                    velocity = fluxDat.Kmvp(phaseIdx); // mind the sign: vDarcy = kf grad p

                    // up+downstream mobility
                    const VolumeVariables &up = elemVolVars[fluxDat.upstreamIdx(phaseIdx)];
                    const VolumeVariables &dn = elemVolVars[fluxDat.downstreamIdx(phaseIdx)];
                    Scalar scvfArea = fluxDat.face().normal.two_norm(); //get surface area to weight velocity at the IP with the surface area
                    velocity *= (massUpwindWeight_*up.mobility(phaseIdx) + (1-massUpwindWeight_)*dn.mobility(phaseIdx))* scvfArea;

                    int vertIIdx = this->problem_().vertexMapper().map(*elemIt,
                            fluxDat.face().i,
                            dim);
                    int vertJIdx = this->problem_().vertexMapper().map(*elemIt,
                            fluxDat.face().j,
                            dim);
                    // add surface area for weighting purposes
                    boxSurface[vertIIdx] += scvfArea;
                    boxSurface[vertJIdx] += scvfArea;

                    // Add velocity to upstream and downstream vertex.
                    // Beware: velocity has to be substracted because of the (wrong) sign of vDarcy

                    velocity_[vertIIdx] -= velocity;
                    velocity_[vertJIdx] -= velocity;

                    (*velocityX)[vertIIdx] -= velocity[0];
                    (*velocityX)[vertJIdx] -= velocity[0];
                    if (dim >= 2)
                    {
                        (*velocityY)[vertIIdx] -= velocity[1];
                        (*velocityY)[vertJIdx] -= velocity[1];
                    }
                    if (dim == 3)
                    {
                        (*velocityZ)[vertIIdx] -= velocity[2];
                        (*velocityZ)[vertJIdx] -= velocity[2];
                    }
                }
            }
    #endif
        }

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded
        // normalize the velocities at the vertices
        for (int i = 0; i < numDofs; ++i)
        {
            velocity_[i] /= boxSurface[i];
//          std::cout << "velocity_["<<i <<"] = "<<velocity_[i]<<std::endl;

            (*velocityX)[i] /= boxSurface[i];
            Scalar abs_v = sqrt((*velocityX)[i]*(*velocityX)[i]);
            if (dim >= 2)
            {
                (*velocityY)[i] /= boxSurface[i];
                abs_v = sqrt((*velocityX)[i]*(*velocityX)[i] + (*velocityY)[i]*(*velocityY)[i]);
            }
            if (dim == 3)
            {
                (*velocityZ)[i] /= boxSurface[i];
                abs_v = sqrt((*velocityX)[i]*(*velocityX)[i] + (*velocityY)[i]*(*velocityY)[i] + (*velocityZ)[i]*(*velocityZ)[i]);
            }

            if (abs_v==0 )
            {
                (*Da_urea)[i] /= (1e-10);
                (*Da_prec)[i] /= (1e-10);
            }
            else
            {
                (*Da_urea)[i] /= abs_v;
                (*Da_prec)[i] /= abs_v;
            }
        }
#endif

        writer.attachVertexData(*Sg, "Sg");
        writer.attachVertexData(*Sl, "Sl");
        writer.attachVertexData(*pg, "pg");
        writer.attachVertexData(*pl, "pl");
        writer.attachVertexData(*pc, "pc");
        writer.attachVertexData(*rhoL, "rhoL");
        writer.attachVertexData(*rhoG, "rhoG");
        writer.attachVertexData(*mobL, "mobL");
        writer.attachVertexData(*mobG, "mobG");
        writer.attachVertexData(*poro, "porosity");
        writer.attachVertexData(*temperature, "temperature");
        writer.attachVertexData(*phasePresence, "phase presence");
        writer.attachVertexData(*potential, "potential");
        writer.attachVertexData(*boxVolume, "boxVolume");
        writer.attachVertexData(*pH, "pH");

        writer.attachVertexData(*Omega, "Omega");
//        writer.attachVertexData(*Appa_Ksp, "Apparent Calcite Solubility Product");
//        writer.attachVertexData(*rprec, "rprec");
        writer.attachVertexData(*rurea, "rurea");
//        writer.attachVertexData(*mue, "mue");

        writer.attachVertexData(*rattachChem, "Attachment_rate");
        writer.attachVertexData(*rdetachChem, "Detachment_rate");
        writer.attachVertexData(*rgrowthChem, "Growth_rate");
        writer.attachVertexData(*rdecayChem, "Decay_rate");

        for (int i = 0; i < numSPhases; ++i)
        {
                        std::ostringstream oss;
            oss << "Volume_Fraction_"
                << FluidSystem::componentName(numComponents + numSecComponents + i);
            writer.attachDofData(*solidity[i], oss.str().c_str(), isBox);
//            std::string name = (boost::format("Volume_Fraction_%s")
//                                % FluidSystem::componentName(numComponents + numSecComponents + phaseIdx)).str();     //TODO componentName(numComponents + numSecComponents -1 + phaseIdx)).str() for vishal
//            writer.attachVertexData(*solidity[phaseIdx], name.c_str());
        }
//        writer.attachVertexData(*NaClMolality, "MolalityNaCl");

        writer.attachVertexData(*intrinsicPerm, "Kxx");
//        writer.attachVertexData(*intrinsicPerm[0], "Kxx");
//        if (dim >= 2)
//            writer.attachVertexData(*intrinsicPerm[1], "Kyy");
//        if (dim == 3)
//            writer.attachVertexData(*intrinsicPerm[2], "Kzz");

        for (int i = 0; i < numPhases; ++i)
        {
            for (int j = 0; j < numComponents + numSecComponents; ++j)
            {
                std::ostringstream oss;
                oss << "X_"
                    << ((i == wPhaseIdx) ? "l" : "g")   //FluidSystem::phaseName(i)
                    //<< "_"
                    << FluidSystem::componentName(j);
                writer.attachVertexData(*massFraction[i][j], oss.str().c_str());
//                std::string name = (boost::format("X_%s%s")
//                                    % ((i == wPhaseIdx) ? "l" : "g")
//                                    % FluidSystem::componentName(j)).str();
//                writer.attachVertexData(*massFraction[i][j], name.c_str());
            }
        }

        for (int j = 0; j < numComponents + numSecComponents; ++j)
        {
            std::ostringstream oss;
            oss << "Molarity_"
                << FluidSystem::componentName(j);
            writer.attachVertexData(*molarity[j], oss.str().c_str());
//            std::string name = (boost::format("Molarity_%s")
//                    % FluidSystem::componentName(j)).str();
//            writer.attachVertexData(*molarity[j], name.c_str());
        }

        writer.attachVertexData(*Da_urea, "Da_ureolysis");
        writer.attachVertexData(*Da_prec, "Da_precipitation");

#ifdef VELOCITY_OUTPUT // check if velocity output is demanded
        writer.attachVertexData(*velocityX, "Vx");
        if (dim >= 2)
        writer.attachVertexData(*velocityY, "Vy");
        if (dim == 3)
        writer.attachVertexData(*velocityZ, "Vz");

#endif
        writer.attachCellData(*rank, "process rank");
    }

    /*!
     * \brief Write the current solution to a restart file.
     *
     * \param outStream The output stream of one vertex for the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void serializeEntity(std::ostream &outStream, const Entity &entity)
    {
        // write primary variables
        ParentType::serializeEntity(outStream, entity);

        int vertIdx = this->dofMapper().map(entity);
        if (!outStream.good())
            DUNE_THROW(Dune::IOError, "Could not serialize vertex " << vertIdx);

        outStream << staticDat_[vertIdx].phasePresence << " ";
    }

    /*!
     * \brief Reads the current solution for a vertex from a restart
     *        file.
     *
     * \param inStream The input stream of one vertex from the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void deserializeEntity(std::istream &inStream, const Entity &entity)
    {
        // read primary variables
        ParentType::deserializeEntity(inStream, entity);

        // read phase presence
        int vertIdx = this->dofMapper().map(entity);
        if (!inStream.good())
            DUNE_THROW(Dune::IOError,
                       "Could not deserialize vertex " << vertIdx);

        inStream >> staticDat_[vertIdx].phasePresence;
        staticDat_[vertIdx].oldPhasePresence
                = staticDat_[vertIdx].phasePresence;

    }

    /*!
     * \copydoc 2pncMin::updateStaticData
     */
    void updateStaticData(SolutionVector &curGlobalSol,
                          const SolutionVector &oldGlobalSol)
    {
        bool wasSwitched = false;

        for (unsigned i = 0; i < staticDat_.size(); ++i)
            staticDat_[i].visited = false;

        FVElementGeometry fvGeometry;
        static VolumeVariables volVars;
        ElementIterator it = this->gridView_().template begin<0> ();
        const ElementIterator &endit = this->gridView_().template end<0> ();
        for (; it != endit; ++it)
        {
            fvGeometry.update(this->gridView_(), *it);
            for (int i = 0; i < fvGeometry.numScv; ++i)
            {
                int dofIdxGlobal = this->vertexMapper().map(*it, i, dim);

                if (staticDat_[dofIdxGlobal].visited)
                    continue;

                staticDat_[dofIdxGlobal].visited = true;
                volVars.update(curGlobalSol[dofIdxGlobal],
                               this->problem_(),
                               *it,
                               fvGeometry,
                               i,
                               false);
                const GlobalPosition &global = it->geometry().corner(i);
                if (primaryVarSwitch_(curGlobalSol,
                                      volVars,
                                      dofIdxGlobal,
                                      global))
                    wasSwitched = true;
            }
        }

        // make sure that if there was a variable switch in an
        // other partition we will also set the switch flag
        // for our partition.
        wasSwitched = this->gridView_().comm().max(wasSwitched);

        setSwitched_(wasSwitched);
    }

protected:
    /*!
     * \copydoc 2pncMin::StaticVars
     */
    struct StaticVars
    {
        int phasePresence;
        bool wasSwitched;

        int oldPhasePresence;
        bool visited;
    };

    /*!
     * \copydoc 2pncMin::resetPhasePresence_
     */
    void resetPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].phasePresence
                    = staticDat_[i].oldPhasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::updateOldPhasePresence_
     */
    void updateOldPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].oldPhasePresence
                    = staticDat_[i].phasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::setSwitched_
     */
    void setSwitched_(bool yesno)
    {
        switchFlag_ = yesno;
    }

    //  perform variable switch at a vertex; Returns true if a
    //  variable switch was performed.
    bool primaryVarSwitch_(SolutionVector &globalSol,
                           const VolumeVariables &volVars, int dofIdxGlobal,
                           const GlobalPosition &globalPos)
    {
        // evaluate primary variable switch
        bool wouldSwitch = false;
        int phasePresence = staticDat_[dofIdxGlobal].phasePresence;
        int newPhasePresence = phasePresence;

        // check if a primary var switch is necessary
        if (phasePresence == nPhaseOnly)
        {
            // calculate mole fraction in the hypothetic liquid phase
//            Scalar xll = volVars.moleFraction(wPhaseIdx, wCompIdx);
//            Scalar xlg = volVars.moleFraction(wPhaseIdx, nCompIdx);
//            Scalar xlNaCl = volVars.moleFraction(wPhaseIdx, NaClIdx);

            Scalar xlMax = 1.0;
            Scalar sumxl  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                sumxl += volVars.moleFraction(wPhaseIdx, compIdx);
            }

            if (sumxl > xlMax)
                wouldSwitch = true;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                xlMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, liquid phase appears
            if (sumxl > xlMax)
            {
                // liquid phase appears
                std::cout << "liquid phase appears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", sum of xl: "
                        << sumxl << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pgSl)
                    globalSol[dofIdxGlobal][switchIdx] = 0.0;
                else if (formulation == plSg)
                    globalSol[dofIdxGlobal][switchIdx] = 1.0;
            };
        }
        else if (phasePresence == wPhaseOnly)
        {
            // calculate fractions of the partial pressures in the
            // hypothetic gas phase
//            Scalar xgl = volVars.moleFraction(nPhaseIdx, wCompIdx);
//            Scalar xgg = volVars.moleFraction(nPhaseIdx, nCompIdx);
//            Scalar xgNaCl = volVars.moleFraction(nPhaseIdx, NaClIdx);

            Scalar xgMax = 1.0;
            Scalar sumxg  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                sumxg += volVars.moleFraction(nPhaseIdx, compIdx);
            }
            if (sumxg > xgMax)
                wouldSwitch = true;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                xgMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, gas phase appears
            if (sumxg > xgMax)
            {
                // gas phase appears
                std::cout << "gas phase appears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", sum of xg: "
                        << sumxg << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pgSl)
                    globalSol[dofIdxGlobal][switchIdx] = 0.999;
                else if (formulation == plSg)
                    globalSol[dofIdxGlobal][switchIdx] = 0.001;
            }
        }
        else if (phasePresence == bothPhases)
        {
            Scalar Smin = 0.0;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                Smin = -0.01;

            if (volVars.saturation(nPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // gas phase disappears
                std::cout << "Gas phase disappears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", Sg: "
                        << volVars.saturation(nPhaseIdx) << std::endl;
                newPhasePresence = wPhaseOnly;

                globalSol[dofIdxGlobal][switchIdx]
                        = volVars.moleFraction(wPhaseIdx, nCompIdx);
            }

            else if (volVars.saturation(wPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // liquid phase disappears
                std::cout << "Liquid phase disappears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", Sl: "
                        << volVars.saturation(wPhaseIdx) << std::endl;
                newPhasePresence = nPhaseOnly;

                globalSol[dofIdxGlobal][switchIdx]
                        = volVars.moleFraction(nPhaseIdx, wCompIdx);
            }
        }

        staticDat_[dofIdxGlobal].phasePresence = newPhasePresence;
        staticDat_[dofIdxGlobal].wasSwitched = wouldSwitch;
        return phasePresence != newPhasePresence;
    }

    // parameters given in constructor
    std::vector<StaticVars> staticDat_;
    bool switchFlag_;
    bool velocityOutput_;

    Scalar plausibilityTolerance_;
    Scalar pHMax_;
    Scalar pHMin_;
    std::vector<Dune::FieldVector<Scalar, dim> > velocity_;
    Scalar massUpwindWeight_;

};

}

#include "dumux/implicit/2pncmin/2pncminpropertydefaults.hh"

#endif
