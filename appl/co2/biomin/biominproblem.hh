// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BIOMINPROBLEM_HH
#define DUMUX_BIOMINPROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//#include <dune/grid/io/file/dgfparser/dgfug.hh>
//#include <dune/grid/io/file/dgfparser/dgfs.hh>
//#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
//#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#include <dune/grid/uggrid.hh>
#include<dumux/material/fluidsystems/biofluidsystem.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/2pncmin/2pncminmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominfluxvariables.hh>
#include <dumux/implicit/2pbiomin/2pbiominlocalresidual.hh>
#include <dumux/implicit/2pbiomin/2pbiominvolumevariables.hh>
#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include <dumux/material/chemistry/biogeochemistry/biocarbonicacid.hh>

#include "biominspatialparams.hh"
#include "bioco2tables.hh"
//#include "co2values.inc"
//#include "dumux/material/components/co2tablereader.hh"
//#include "dumux/material/components/co2tables.inc"

#include "dumux/linear/seqsolverbackend.hh"


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class BioMinProblem;

namespace Properties
{
NEW_TYPE_TAG(BioMinProblem, INHERITS_FROM(BoxTwoPNCMin, BioMinSpatialParams));
//NEW_TYPE_TAG(BioMinProblem, INHERITS_FROM(BoxTwoBioMin, BioMinSpatialParams));

// Set the grid type
SET_PROP(BioMinProblem, Grid)
{
//  typedef Dune::UGGrid<2> type;
   typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(BioMinProblem, Problem)
{
    typedef Dumux::BioMinProblem<TTAG(BioMinProblem)> type;
};

SET_PROP(BioMinProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::BioFluid<TypeTag, Scalar, CO2Tables, H2O_Tabulated, useComplexRelations> type;
};

//! the BioMin Flux VolumeVariables and LocalResidual properties

SET_TYPE_PROP(BioMinProblem, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);///TODO
SET_TYPE_PROP(BioMinProblem, VolumeVariables, TwoPBioMinVolumeVariables<TypeTag>);
SET_TYPE_PROP(BioMinProblem, LocalResidual, TwoPBioMinLocalResidual<TypeTag>);
//SET_TYPE_PROP(BioMinProblem, PropertyDefaults, TwoPBioMinPropertyDefaults<TypeTag>);
SET_TYPE_PROP(BioMinProblem, Model, TwoPBioMinModel<TypeTag>);


//SET_TYPE_PROP(BioMinProblem, LinearSolver, SuperLUBackend<TypeTag>);


SET_PROP(BioMinProblem, Chemistry)
{
//  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//  typedef Dumux::TabulatedCO2Properties<Scalar> CO2Tables;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::BioCarbonicAcid<TypeTag, CO2Tables> type;
//  typedef Dumux::BioCarbonicAcid<TypeTag, Dumux::BioMin::CO2Tables> type;

//    typedef Dumux::NoChemistry<TypeTag> type;

};

// Set the spatial parameters
SET_TYPE_PROP(BioMinProblem,
              SpatialParams,
              Dumux::BioMinSpatialParams<TypeTag>);
}


/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(BioMinProblem) >
class BioMinProblem : public ImplicitPorousMediaProblem<TypeTag>
//class BioMinProblem : public TwoPBioMinProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef BioMinProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numComponents = FluidSystem::numComponents,
        numSecComponents = FluidSystem::numSecComponents,
        numSPhases = FluidSystem::numSPhases,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xlNaIdx = FluidSystem::NaIdx,
        xlClIdx = FluidSystem::ClIdx,
        xlCaIdx = FluidSystem::CaIdx,
        xlUreaIdx = FluidSystem::UreaIdx,
        xlTNHIdx = FluidSystem::TNHIdx,
        xlO2Idx = FluidSystem::O2Idx,
        xlBiosubIdx = FluidSystem::BiosubIdx,
        xlBiosuspIdx = FluidSystem::BiosuspIdx,
        phiBiofilmIdx = numComponents,//FluidSystem::BiofilmIdx,
        phiCalciteIdx = numComponents + 1,//FluidSystem::CalciteIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        UreaIdx = FluidSystem::UreaIdx,
        TNHIdx = FluidSystem::TNHIdx,
        O2Idx = FluidSystem::O2Idx,
        BiosubIdx = FluidSystem::BiosubIdx,
        BiosuspIdx = FluidSystem::BiosuspIdx,

        NH4Idx = FluidSystem::NH4Idx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        OHIdx = FluidSystem::OHIdx,

        BiofilmIdx = FluidSystem::BiofilmIdx,
        CalciteIdx = FluidSystem::CalciteIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        bPhaseIdx = FluidSystem::bPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
   // typedef Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, Dumux::BioMin::CO2Tables, true> Brine_CO2;

//    typedef Dune::FieldVector<Scalar, numComponents + numSecComponents> CompVector;

public:
    BioMinProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
              try
    {
            name_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
            inverseModel_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Inverse);

            dtmax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtMax);

            //initial values
            densityW_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initDensityW);
            initPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPressure);

            initxlTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTC);
            initxlNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlNa);
            initxlCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCl);
            initxlCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlCa);
            initxlUrea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlUrea);
            initxlTNH_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlTNH);
            initxlO2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlO2);
            initxlBiosub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlSubstrate);
            initxlBiosusp_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxlBiosusp);

            initBiofilm_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initBiofilm);
            initCalcite_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initCalcite);

            initPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);

            xlNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlNaCorr);
            xlClCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, xlClCorr);

            //injection values
            injQ_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injVolumeflux);
            injTC_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTC);
            injNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injNa);
//          injCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCl);
            injCa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCa);
            injUrea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injUrea);
            injTNH_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTNH);
            injO2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injO2);
            injSub_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injSub);
            injBiosusp_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injBiosusp);
            injNaCorr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injNaCorr);
//            numInjections_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, numInjections);
//            injectionParameters_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Injection, InjectionParamFile);
    }

    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
        FluidSystem::init();
        this->timeManager().startNextEpisode(EpisodeEnd(1)-EpisodeEnd(0));
        this->newtonController().setMaxSteps(8);
    }
    /*!
      * \brief Returns true if a output file should be written to
      *        disk.
      *
      * The default behaviour is to write one output file every time
      * step.
      */
    bool shouldWriteOutput() const
        {
            return
//                  this->timeManager().timeStepIndex() % 1 == 0 ||         //output every timestep
                    this->timeManager().timeStepIndex() % 1000 == 0 ||          //output every 1000 timesteps
                this->timeManager().timeStepIndex() == 0 ||
                this->timeManager().episodeWillBeOver() ||
                this->timeManager().willBeFinished();
        }

    void preTimeStep()
    {
        if (this->timeManager().timeStepSize()>dtmax_) //Anozie hat auf 210s = Tepisodemin/2 begrenzt!
        {
            this->timeManager().setTimeStepSize(dtmax_);
        }
    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    { return "name_"; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 25; // -> 25 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar zmax = this->bBoxMax()[dim - 1];
        values.setAllNeumann();
        if(globalPos[1] > zmax - eps_)
            {
            values.setDirichlet(pressureIdx);
            for (int i=switchIdx; i< numEq - numSPhases; ++i)
            {
//              values.setOutflow(i);
                values.setDirichlet(i);
            }
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar zmax = this->bBoxMax()[dim - 1];
        if(globalPos[1] > zmax - eps_)
        {
            values[pressureIdx] =  1e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
            values[switchIdx] = initxlTC_;
            values[xlNaIdx] = initxlNa_ + xlNaCorr_;
            values[xlClIdx] = initxlCl_ + initxlTNH_ + 2*initxlCa_ + xlClCorr_;
            values[xlCaIdx] = initxlCa_;
            values[xlUreaIdx] = 0.0; //initxlUrea_;
            values[xlTNHIdx] = initxlTNH_;
            values[xlO2Idx] = 0.0; //initxlO2_;
            values[xlBiosubIdx] = 0.0; //initxlBiosub_;
            values[xlBiosuspIdx] = 0.0; //initxlBiosusp_;
            values[phiBiofilmIdx] = 0.00; // [kg/m^3]
            values[phiCalciteIdx] = 0.00000;
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      int scvIdx,
                      int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        Scalar waterflux = 0.929 * 3.2892e-4; //[m/s]               //TODO      TEST!!!!!!!!!!!!!!!!!!!!

        int episodeIdx = this->timeManager().episodeIndex();
        int InjProcess = Injection(episodeIdx);

//         negative values for injection
        if(globalPos[1]<= eps_)
        {
        //basic rinse injection (InjProcess == -1 )
        values[conti0EqIdx + wCompIdx] = -waterflux * 996/FluidSystem::molarMass(wCompIdx);
        values[conti0EqIdx + nCompIdx] = -waterflux * injTC_*996 /FluidSystem::molarMass(nCompIdx);
        values[conti0EqIdx + xlCaIdx] = 0;
        values[conti0EqIdx + xlBiosuspIdx] = 0;
        values[conti0EqIdx + xlBiosubIdx] = -waterflux * injSub_ /FluidSystem::molarMass(xlBiosubIdx);
        values[conti0EqIdx + xlO2Idx] = -waterflux * injO2_ /FluidSystem::molarMass(O2Idx);
        values[conti0EqIdx + xlUreaIdx] = 0;
        values[conti0EqIdx + xlTNHIdx] = -waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx);
        values[conti0EqIdx + phiCalciteIdx] = 0;
        values[conti0EqIdx + phiBiofilmIdx] = 0;
        values[conti0EqIdx + xlNaIdx] = -waterflux * (injNa_ + injNaCorr_) /FluidSystem::molarMass(NaIdx);
        values[conti0EqIdx + xlClIdx] = -waterflux *injTNH_ /FluidSystem::molarMass(TNHIdx)     //NH4Cl --->  mol Cl = mol NH4
                                        -waterflux *injNa_ /FluidSystem::molarMass(NaIdx);      //NaCl ---> mol Cl = mol Na

        if (InjProcess == -1)   // rinse, used as standard injection fluid
        {
            //          do not change anything.
        }

        else if (InjProcess == -99) // no injection
        {
            values = 0.0; //mol/m²/s
        }

        else if (InjProcess == 1)       //ca-rich injection: ca and urea injected additionally to rinse-fluid, Na (pH) and Cl are also different(CaCl2)
        {
            values[conti0EqIdx + wCompIdx] = - waterflux * 0.8716 * densityW_ /FluidSystem::molarMass(wCompIdx);    //TODO 0.8716 check factor!!!
            values[conti0EqIdx + nCompIdx] = - waterflux * injTC_ * densityW_ /FluidSystem::molarMass(nCompIdx);
            values[conti0EqIdx + xlCaIdx] = - waterflux * injCa_/FluidSystem::molarMass(CaIdx);
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
            values[conti0EqIdx + xlNaIdx] = - waterflux * injNa_ /FluidSystem::molarMass(NaIdx) * 0.032;//TODO
            values[conti0EqIdx + xlClIdx] = - waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx)               //NH4Cl --->  mol Cl = mol NH4
                                            - waterflux * 2 * injCa_/FluidSystem::molarMass(CaIdx);             //+CaCl2 --->  mol Cl = mol Ca*2
        }

        else if (InjProcess == 0 || InjProcess == 3 )   //urea-injections: urea is injected additionally to rinse-fluid
        {
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
        }

        else if(InjProcess == 2)        //inoculation: same as rinse, but with bacteria
        {
            values[conti0EqIdx + xlBiosuspIdx] = -waterflux * injBiosusp_ /FluidSystem::molarMass(xlBiosuspIdx);
        }


            else
            {
                DUNE_THROW(Dune::InvalidStateException, "Invalid injection process " << InjProcess);
            }
        }
        else
        {
            values = 0.0; //mol/m²/s
        }
  }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &q,
              const Element &element,
              const FVElementGeometry &fvGeometry,
                int scvIdx) const
    {
        q = 0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void reactionSource(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                const VolumeVariables &volVars,
                const Scalar absgradpw) const
    {

        q = 0;

        Scalar dt = this->timeManager().timeStepSize();

        Chemistry chemistry;
        chemistry.reactionSource(q,
                        volVars,
                        absgradpw,
                        dt);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
//      return bothPhases;
    }

    /*!
     * \brief Return a reference value for each EqIdx for an alternative
     * Netwon convergence criterion in the 2pbiominmodel.
     */
    Scalar refValue(int EqIdx)
    {
        Scalar refValue = 0;
        Scalar mH2O = 55.508; //molH2O/kg(l)H2O
        switch (EqIdx) {
            case pressureIdx: refValue = initPressure_;break;
            case switchIdx: refValue = injTC_ /(FluidSystem::molarMass(nCompIdx) * mH2O);break;
            case CaIdx: refValue = injCa_/(1000 * FluidSystem::molarMass(CaIdx) * mH2O);break;
            case NaIdx: refValue = (injNa_+injNaCorr_)/(1000 * FluidSystem::molarMass(NaIdx) * mH2O);break;
            case ClIdx: refValue = 2*injCa_/(1000 * FluidSystem::molarMass(CaIdx) * mH2O)
                                    + injTNH_/(1000 * FluidSystem::molarMass(TNHIdx) * mH2O)
                                    + injNa_/(1000 * FluidSystem::molarMass(NaIdx) * mH2O);break;
            case BiosuspIdx: refValue = injBiosusp_/(1000 * FluidSystem::molarMass(BiosuspIdx) * mH2O);break;
            case BiosubIdx: refValue = injSub_/(1000 * FluidSystem::molarMass(BiosubIdx) * mH2O);break;
            case O2Idx: refValue = injO2_/(1000 * FluidSystem::molarMass(O2Idx) * mH2O);break;
            case UreaIdx: refValue = injUrea_/(1000 * FluidSystem::molarMass(UreaIdx) * mH2O);break;
            case TNHIdx: refValue = injTNH_/(1000 * FluidSystem::molarMass(TNHIdx) * mH2O);break;
            case phiCalciteIdx: refValue = 0.01;break;
            case phiBiofilmIdx: refValue = 0.01;break;
        }
        return refValue;
    }

    void postTimeStep()
    {
        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();
        int episodeIdx = this->timeManager().episodeIndex()-1;

        if (this->gridView().comm().rank() == 0)
        {

           std::cout<< " Time: "<<time+dt<< ", Episode: "<<episodeIdx+1<<", injection process: "<<injType_[episodeIdx]<<", injection of "<<injQ_*60*1e6<<" [ml/min]"<<std::endl;
           std::cout<<"==============================================******==================================================="<<std::endl;
       }
    }

    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        //if(this->timeManager().episodeIndex() ==1 )


     int episodeIdx = this->timeManager().episodeIndex()-1;
     Scalar tEpisode= EpisodeEnd(episodeIdx + 1)-EpisodeEnd(episodeIdx);

        this->timeManager().startNextEpisode(tEpisode);
        this->timeManager().setTimeStepSize(10);
         std::cout<< "\n  episode number  " << episodeIdx << " done, starting next episode  "
        	<<episodeIdx + 1 << ", InjectionProcess scheme is : "<<  injType_[episodeIdx + 1]
			<< "\n"  <<std::endl;
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[1];
        values[pressureIdx] = initPressure_ ; //70e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        values[switchIdx] = initxlTC_;
        values[xlNaIdx] = initxlNa_ + xlNaCorr_;
        values[xlClIdx] = initxlCl_ + initxlTNH_ + 2*initxlCa_ + xlClCorr_;
        values[xlCaIdx] = initxlCa_;
        values[xlUreaIdx] = initxlUrea_;
        values[xlTNHIdx] = initxlTNH_;
        values[xlO2Idx] = initxlO2_;
        values[xlBiosubIdx] = initxlBiosub_;
        values[xlBiosuspIdx] = initxlBiosusp_;
        values[phiBiofilmIdx] = initBiofilm_; // [m^3/m^3]
        values[phiCalciteIdx] = initCalcite_; // [m^3/m^3]

#if !ISOTHERMAL
        values[temperatureIdx] = 283.0 + (depthBOR_ - globalPos[1])*0.03;
#endif

    }

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XlNaCl the XlNaCl [kg NaCl / kg solution]
     */
    static Scalar massTomoleFrac_(Scalar XlNaCl)
    {
       const Scalar Mw = FluidSystem::molarMass(wCompIdx);  // 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = FluidSystem::molarMass(NaIdx) + FluidSystem::molarMass(ClIdx); // 58.44e-3; /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XlNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       const Scalar xlNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xlNaCl;
    }
    static const int Injection (int episodeIdx)
    {
        int Inj [111]=
        {-99,                                                           //0
        2,  -99,    3,  -99,    -1, 1,   0, -99,    -1, 1,   0, -99,    //12
        -1, 1,      0,  -99,    -1, 1,   0, -99,    3,  -99, 3, -99,    //24
        3,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //36
        0,  -99,    -1, 1,      0,  -99, 3, -99,    3,  -99, -1,  1,    //48
        0,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //60
        0,  -99,    -1, 1,      0,  -99,  3,-99,    3,  -99, -1,  1,    //72
        0,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //84
        0,  -99,    -1, 1,      0,  -99,  3, -99,   3,  -99, -1,  1,    //96
        0,  -99,    -1, 1,      0,  -99, -1,   1,   0,  -99, -1,  1,    //108
        0,  -99};                                                       //110

    return Inj[episodeIdx];
    }
    static const Scalar EpisodeEnd (int episodeIdx)
    {
        //End times of the episodes in minutes.
         Scalar EpiEnd[111] = {0,   //10e20};                                                                           //0
                 15,    420,    1530,   2730,   2765,   2795,   2801,   3960,   3997,   4027,   4033,   5550,   //12
                 5587,  5617,   5623,   6941,   6978,   7008,   7014,   8366,   8396,   9836,   9866,   11036,  //24
                 11066, 12746,  12783,  12813,  12819,  15626,  15661,  15691,  15697,  17086,  17124,  17154,  //36
                 17160, 18506,  18538,  18568,  18574,  19853,  19883,  21248,  21278,  22868,  22903,  22933,  //48
                 22939, 24368,  24405,  24435,  24441,  25808,  25843,  25873,  25879,  27188,  27225,  27255,  //60
                 27261, 28628,  28663,  28693,  28699,  29998,  30028,  31568,  31598,  32978,  33013,  33043,  //72
                 33049, 34373,  34410,  34440,  34446,  35873,  35908,  35938,  35944,  37373,  37410,  37440,  //84
                 37446, 38873,  38908,  38938,  38944,  40005,  40035,  41610,  41640,  43290,  43327,  43357,  //96
                 43363, 44702,  44737,  44767,  44773,  46104,  46139,  46169,  46175,  47339,  47374,  47404,  //108
                 47410, 48850};                                                                                 //110

    return 60 * EpiEnd[episodeIdx];
    }

    static constexpr Scalar eps_ = 1e-6;

   Scalar newtonTolerance_;
    Scalar dtmax_;

    Scalar initPressure_;
    Scalar densityW_;//1087; // rhow=1087;

    Scalar initxlTC_;//2.3864e-7;       // [mol/mol]
    Scalar initxlNa_;//0;
    Scalar initxlCl_;//0;
    Scalar initxlCa_;//0;
    Scalar initxlUrea_;//0;
    Scalar initxlTNH_;//3.341641e-3;
    Scalar initxlO2_;//4.4686e-6;
    Scalar initxlBiosub_;//2.97638e-4;
    Scalar initxlBiosusp_;//0;
    Scalar xlNaCorr_;//2.9466e-6;
    Scalar xlClCorr_;//0;

    Scalar initBiofilm_;
    Scalar initCalcite_;

    Scalar initPorosity_;

    Scalar injQ_;

    Scalar injTC_;//5.8e-7;             // [kg/kg]
    Scalar injNa_;//0.00379;                // [kg/m³]
    Scalar injCa_;//50.44778166222;//50.433;                // [kg/m³]      //computed from 139.7 g/l CaCl2
    Scalar injUrea_;//20;                   // [kg/m³]
    Scalar injTNH_;//3.183840574;//3.184;               // [kg/m³]      //computed from 10 g/l NH4Cl
    Scalar injO2_;//0.008;                  // [kg/m³]
    Scalar injSub_;//3;                 // [kg/m³]
    Scalar injBiosusp_;//0.00325;           // [kg/m³]
    Scalar injNaCorr_;

    Scalar numInjections_;
    std::string injectionParameters_;

    std::vector<Scalar> epiEnd_;
    std::vector<int> injType_;
    std::string name_;

    Scalar inverseModel_;
    bool useInverseModel_ = false;

};
} //end namespace

#endif
