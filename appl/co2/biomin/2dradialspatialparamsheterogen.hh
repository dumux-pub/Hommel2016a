/*****************************************************************************
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BIOMIN_SPATIAL_PARAMS_HH
#define DUMUX_BIOMIN_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/material/spatialparameters/gstatrandompermeability.hh>


namespace Dumux
{
//forward declaration
template<class TypeTag>
class BioMinSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(BioMinSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(BioMinSpatialParams, SpatialParams, Dumux::BioMinSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(BioMinSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
}

/**
 * \brief Definition of the spatial parameters for the brine-co2 problem
 *
 */
template<class TypeTag>
class BioMinSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename Grid::ctype CoordScalar;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
    };

//    typedef typename GET_PROP_TYPE(TypeTag, TwoPNCMinIndices)) Indices;
    enum {
        lPhaseIdx = FluidSystem::lPhaseIdx,
        gPhaseIdx = FluidSystem::gPhaseIdx,
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> Vector;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef std::vector<Scalar> PermeabilityType;
    typedef std::vector<Tensor> PermeabilityTypeTensor;

public:
    BioMinSpatialParams(const GridView &gridView)
        : ParentType(gridView),
          heterogeneousPermeability_(gridView.size(dim), 0.0),
      RandomK_(gridView.size(dim),(0.0)),
//          RandomK_(Dune::FieldMatrix<Scalar, dim, dim>(0.0)),
          indexSet_(gridView.indexSet()),
     eps_(0.001)
    {


      try
      {
      porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
      critPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, CritPorosity);
      ScalarK_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
      highPermeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, HighPermeability);
      rmin_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, rmin);
      medium_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Medium);
      inletPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, InletPorosity);

//      permeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
      useHeterogeneousSoil_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, UseHeterogeneousSoil);


      }
        catch (Dumux::ParameterException &e)
        {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }

      // intrinsic permeabilities

    SandK_ = Scalar(0.0);
//    highPermeability_= 0.00001;
//    ScalarK_ = 1.82e-10;
    for (int i = 0; i < dim; i++) {

      Unity_[i][i] = 1;
      highInletK_[i][i] = highPermeability_;
      SandK_[i][i] = ScalarK_;
    }


        // porosity:
//          porosity_ = 0.18;
//          critPorosity_ = 0.108;

        // residual saturations
        columnMaterialParams_.setSwr(0.2);
        columnMaterialParams_.setSnr(0.05);

        // parameters for the Brooks-Corey law
        columnMaterialParams_.setPe(1e4);
        columnMaterialParams_.setLambda(2.0);
    }

    ~BioMinSpatialParams()
    {}


    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution The global solution vector
     */
    void update(const SolutionVector &globalSolution)
    {
    };

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element The current finite element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvfIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
  const Tensor intrinsicPermeability(const Element &element,
    const FVElementGeometry &fvElemGeom, int scvIdx) const {
  //        for (int i = 0; i < dim; i++)
  //        {
  //          RandomK_[i][i] = heterogeneousPermeability_[indexSet_.index(*(element.template subEntity<dim> (scvIdx)))];
  //        }
  //        return RandomK_;
  //      return Unity_ * heterogeneousPermeability_[indexSet_.index(*(element.template subEntity<dim> (scvIdx)))];
  const GlobalPosition &pos = element.geometry().corner(scvIdx);
  double radius = sqrt(pos[0] * pos[0] + pos[1] * pos[1]);
  if (radius > rmin_+ eps_) {
    if (useHeterogeneousSoil_) {
      return RandomK_[indexSet_.index(
          *(element.template subEntity < dim > (scvIdx)))];
    } else {
    return SandK_;
    }
  } else {
  return highInletK_;
  }
  }



  const Scalar &intrinsicPermeabilityScalar(const Element &element,
    const FVElementGeometry &fvElemGeom, int scvIdx) const {
  // heterogeneous parameter field computed with GSTAT

  const GlobalPosition &pos = element.geometry().corner(scvIdx);
  double radius = sqrt(pos[0] * pos[0] + pos[1] * pos[1]);
  if (radius > rmin_ + eps_) {
    //          ScalarRandomK_ = heterogeneousPermeability_[indexSet_.index(*(element.template subEntity<dim> (scvIdx)))];
    //      return ScalarRandomK_;
    if (useHeterogeneousSoil_) {
      return heterogeneousPermeability_[indexSet_.index(
          *(element.template subEntity < dim > (scvIdx)))];
    } else {
      return ScalarK_;
    }
  } else {
    return highPermeability_;
  }
  }

    /*!
 * \brief Define the minimum porosity \f$[-]\f$ after salt precipitation
 *
 * \param elemVolVars The data defined on the sub-control volume
 * \param element The finite element
 * \param fvGeometry The finite volume geometry
 * \param scvIdx The local index of the sub-control volume where
 *                    the porosity needs to be defined
 */
double porosityMin(const Element &element,
                   const FVElementGeometry &fvGeometry,
                   int scvIdx) const
 {
    return 0.0;
 }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param vDat The data defined on the sub-control volume
     * \param element The finite element
     * \param fvElemGeom The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    double solidity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
    const GlobalPosition &pos = element.geometry().corner(scvIdx);
    double radius = sqrt(pos[0] * pos[0] + pos[1] * pos[1]);
    if (radius > rmin_ + eps_) {
        if (useHeterogeneousSoil_) {
          if (medium_ ==0){
             return 1 - (pow(heterogeneousPermeability_[indexSet_.index(
              *(element.template subEntity < dim > (scvIdx)))] / ScalarK_, 1.0/3.0) *porosity_);
          } else if (medium_ ==1){
            return 1 - (1/(37.6362)*log(heterogeneousPermeability_[indexSet_.index(
                  *(element.template subEntity < dim > (scvIdx)))]/(1.8478e-15)));
          }
        }else{
          return 1 - porosity_;
        }
      } else {
        return 0.5;
      }
    }
    const Scalar porosity(const Element &element,        //TODO sonst gibt es einen Fehler: Dune reported error: Dune::InvalidStateException [porosityAtPos:/temp/hommel/DUMUX/dumux/dumux/material/spatialparams/boxspatialparams1p.hh:167]: The spatial parameters do not provide a porosityAtPos() method.
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
          //          ScalarRandomK_ = heterogeneousPermeability_[indexSet_.index(*(element.template subEntity<dim> (scvIdx)))];
          //      return ScalarRandomK_;
      const GlobalPosition &pos = element.geometry().corner(scvIdx);
      double radius = sqrt(pos[0] * pos[0] + pos[1] * pos[1]);
      if (radius > rmin_ + eps_) {
          if (useHeterogeneousSoil_) {
            if (medium_ ==0){
            return pow(heterogeneousPermeability_[indexSet_.index(
                *(element.template subEntity < dim > (scvIdx)))]/ ScalarK_, 1.0/3.0)*porosity_ ;
            } else if (medium_ ==1){
              return 1/(37.6362)*log(heterogeneousPermeability_[indexSet_.index(
                *(element.template subEntity < dim > (scvIdx)))]/(1.8478e-15));
            }

          } else {
            return porosity_;
          }
      }else {
      return porosity_;

      }
    }

    const Scalar critPorosity(const Element &element,
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
        return critPorosity_;
    }
//    Scalar porosityAtPos(const GlobalPosition& globalPos) const
//    {
//        DUNE_THROW(Dune::InvalidStateException,
//                   "The spatial parameters do not provide "
//                   "a porosityAtPos() method.");
//    }
    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
            return columnMaterialParams_;
    }

    void loadIntrinsicPermeability(const GridView& gridView)
       {
           // only load random field, if soilType is set to 0
           if (!useHeterogeneousSoil_)
               return;

           const unsigned size = gridView.size(dim);
           heterogeneousPermeability_.resize(size, 0.0);
           RandomK_.resize(size, 0.0);
//           columnMaterialParams_.resize(size);

           bool create = true;
           std::string gStatControlFileName("gstatControl_2D.txt");
           std::string gStatInputFileName("gstatInput.txt");
           std::string permeabilityFileName("permeab.dat");
           try {
               create = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool , Gstat, GenerateNewPermeability);
               gStatControlFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, ControlFileName);
               gStatInputFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, InputFileName);
               permeabilityFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, PermeabilityInputFileName);
           }
           catch (Dumux::ParameterException &e) {
               std::cerr << e << ". Abort!\n";
               exit(1) ;
           }
           catch (...) {
               std::cerr << "Unknown exception thrown!\n";
               exit(1);
           }

           // create random permeability object
           GstatRandomPermeability<GridView, Scalar> randomPermeability(gridView,
                                                                        create,
                                                                        permeabilityFileName.c_str(),
                                                                        gStatControlFileName.c_str(),
                                                                        gStatInputFileName.c_str());
//std::cout<<"randomPermeability = " <<randomPermeability<<std::endl;
           Scalar totalVolume = 0;
           Scalar meanPermeability = 0;

           //TODO: only for 2D elements and rectangular grids
           const unsigned numVertices = 4;
           for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
           {
               ElementIterator eItEnd = gridView.template end<0> ();
               for (ElementIterator eIt = gridView.template begin<0> (); eIt
                        != eItEnd; ++eIt)
               {
                   // for isotropic media
                   const Scalar perm = randomPermeability.K(*eIt)[0][0];
                   heterogeneousPermeability_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                       = perm;

                   const Scalar volume = eIt->geometry().volume();
                   meanPermeability += volume / perm;
                   totalVolume += volume;
               }
           }
           meanPermeability /= totalVolume;
           meanPermeability = 1.0 / meanPermeability;

           ElementIterator eItEnd = gridView.template end<0> ();
           for (ElementIterator eIt = gridView.template begin<0> (); eIt
                     != eItEnd; ++eIt)
           {
               for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
               {

                  heterogeneousPermeability_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                     = randomPermeability.K(*eIt)[0][0];
//                  for (int i = 0; i < dim; i++)
//               {
//                  RandomK_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))][i][i]
//                     = randomPermeability.K(*eIt)[0][0];
//               }
//                  std::cout <<"HETEROGENE PERMEABILITAET: "<< heterogeneousPermeability_;

                  RandomK_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                     = randomPermeability.K(*eIt);
//                  std::cout <<"HETEROGENE PERMEABILITAET Tensor: "<< RandomK_ << std::endl;
               }
           }
//           std::cout <<"HETEROGENE PERMEABILITAET: "<< heterogeneousPermeability_ << std::endl;

           Dune::VTKWriter<GridView> vtkwriter(gridView);
           vtkwriter.addVertexData(heterogeneousPermeability_, "absolute permeability");
           PermeabilityType logPerm(size);
           for (unsigned int i = 0; i < size; i++)
               logPerm[i] = log10(heterogeneousPermeability_[i]);
           vtkwriter.addVertexData(logPerm, "logarithm of permeability");
           vtkwriter.write("permeability", Dune::VTK::OutputType::ascii);
       }

private:

    Dune::FieldMatrix<Scalar,dim,dim> SandK_;
    Dune::FieldMatrix<Scalar,dim,dim>highInletK_;
//    Dune::FieldMatrix<Scalar,dim,dim> RandomK_;
    Dune::FieldMatrix<Scalar,dim,dim> Unity_;

    Scalar porosity_;
    Scalar critPorosity_;
    Scalar ScalarK_;
    Scalar ScalarRandomK_;
    Scalar highPermeability_;
    Scalar rmin_;
    Scalar eps_;
    Scalar medium_;
    Scalar inletPorosity_;
//    Scalar heterogeneousPorosity_;
//  Scalar heterogeneousSolidity_;

    MaterialLawParams columnMaterialParams_;

    bool useHeterogeneousSoil_;
    PermeabilityType heterogeneousPermeability_;
    PermeabilityTypeTensor RandomK_;
    const IndexSet& indexSet_;
};

}

#endif
