// $Id: waterairproblem.hh 4185 2010-08-26 15:49:58Z lauser $
/*****************************************************************************
 *   Copyright (C) 2009 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FRACCOREPROBLEM_HH
#define DUMUX_FERACCOREPROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

//#include <dune/grid/io/file/dgfparser/dgfug.hh>
//#include <dune/grid/io/file/dgfparser/dgfs.hh>
//#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
//#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#include <dune/grid/uggrid.hh>
#include<dumux/material/fluidsystems/biofluidsystem.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/2pncmin/2pncminmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominmodel.hh>
#include <dumux/implicit/2pbiomin/2pbiominfluxvariables.hh>
#include <dumux/implicit/2pbiomin/2pbiominlocalresidual.hh>
#include <dumux/implicit/2pbiomin/2pbiominvolumevariables.hh>
#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

#include <dumux/material/binarycoefficients/brine_co2_varSal.hh>
#include <dumux/material/chemistry/biogeochemistry/biocarbonicacid.hh>

#include "fraccorespatialparams.hh"
#include "bioco2tables.hh"

#include "dumux/linear/seqsolverbackend.hh"


#define ISOTHERMAL 1

/*!
 * \ingroup BoxProblems
 * \brief TwoPNCMinBoxProblems  two-phase n-component mineralisation box problems
 */

namespace Dumux
{
template <class TypeTag>
class BioMinProblem;

namespace Properties
{
NEW_TYPE_TAG(BioMinProblem, INHERITS_FROM(BoxTwoPNCMin, FracCoreSpatialParams));
//NEW_TYPE_TAG(BioMinProblem, INHERITS_FROM(BoxTwoBioMin, FracCoreSpatialParams));

// Set the grid type
SET_PROP(BioMinProblem, Grid)
{
    typedef Dune::UGGrid<3> type;
   //typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(BioMinProblem, Problem)
{
    typedef Dumux::BioMinProblem<TTAG(BioMinProblem)> type;
};

SET_PROP(BioMinProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2O_Tabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::BioFluid<TypeTag, Scalar, CO2Tables, H2O_Tabulated, useComplexRelations> type;
};

//! the BioMin Flux VolumeVariables and LocalResidual properties

SET_TYPE_PROP(BioMinProblem, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);
SET_TYPE_PROP(BioMinProblem, VolumeVariables, TwoPBioMinVolumeVariables<TypeTag>);
SET_TYPE_PROP(BioMinProblem, LocalResidual, TwoPBioMinLocalResidual<TypeTag>);
//SET_TYPE_PROP(BioMinProblem, PropertyDefaults, TwoPBioMinPropertyDefaults<TypeTag>);
SET_TYPE_PROP(BioMinProblem, Model, TwoPBioMinModel<TypeTag>);


//SET_TYPE_PROP(BioMinProblem, LinearSolver, SuperLUBackend<TypeTag>);


SET_PROP(BioMinProblem, Chemistry)
{
    typedef Dumux::BioCarbonicAcid<TypeTag, Dumux::BioMin::CO2Tables> type;

//    typedef Dumux::NoChemistry<TypeTag> type;

};

// Set the spatial parameters
SET_TYPE_PROP(BioMinProblem,
              SpatialParams,
              Dumux::FracCoreSpatialParams<TypeTag>);





}


/*!
 * \ingroup TwoPTwoCNIBoxProblems
 * \brief Measurement of Brine displacement due to CO2 injection
 *  */
template <class TypeTag = TTAG(BioMinProblem) >
class BioMinProblem : public ImplicitPorousMediaProblem<TypeTag>
//class BioMinProblem : public TwoPBioMinProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef BioMinProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numComponents = FluidSystem::numComponents,
        numSecComponents = FluidSystem::numSecComponents,
        numSPhases = FluidSystem::numSPhases,

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx, //Saturation
        xlNaIdx = FluidSystem::NaIdx,
        xlClIdx = FluidSystem::ClIdx,
        xlCaIdx = FluidSystem::CaIdx,
        xlUreaIdx = FluidSystem::UreaIdx,
        xlTNHIdx = FluidSystem::TNHIdx,
        xlO2Idx = FluidSystem::O2Idx,
        xlBiosubIdx = FluidSystem::BiosubIdx,
        xlBiosuspIdx = FluidSystem::BiosuspIdx,
        phiBiofilmIdx = numComponents,//FluidSystem::BiofilmIdx,
        phiCalciteIdx = numComponents + 1,//FluidSystem::CalciteIdx,

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Indices of the components
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        UreaIdx = FluidSystem::UreaIdx,
        TNHIdx = FluidSystem::TNHIdx,
        O2Idx = FluidSystem::O2Idx,
        BiosubIdx = FluidSystem::BiosubIdx,
        BiosuspIdx = FluidSystem::BiosuspIdx,

        NH4Idx = FluidSystem::NH4Idx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        OHIdx = FluidSystem::OHIdx,

        BiofilmIdx = FluidSystem::BiofilmIdx,
        CalciteIdx = FluidSystem::CalciteIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        bPhaseIdx = FluidSystem::bPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,

        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
   // typedef Dune::FieldVector<Scalar, numComponents> ComponentVector;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, Dumux::BioMin::CO2Tables, true> Brine_CO2;

//    typedef Dune::FieldVector<Scalar, numComponents + numSecComponents> CompVector;

public:
    BioMinProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        FluidSystem::init();
        this->timeManager().startNextEpisode(EpisodeEnd(1)-EpisodeEnd(0));
        this->newtonController().setMaxSteps(8);
        this->newtonController().setRelTolerance(1e-8);
    }
    /*!
      * \brief Returns true if a output file should be written to
      *        disk.
      *
      * The default behaviour is to write one output file every time
      * step.
      */

    bool shouldWriteOutput() const
        {
            return
//              this->timeManager().timeStepIndex() % 1 == 0 ||         //output every timestep
                this->timeManager().timeStepIndex() % 100 == 0 ||
                this->timeManager().timeStepIndex() == 0 ||
                this->timeManager().episodeWillBeOver() ||
                this->timeManager().willBeFinished();
        }
    /*!
      * \brief Returns true if a restart file should be written to
      *        disk.
      *
      * The default behaviour is to write one restart file every 10 time
      * steps. This file is intented to be overwritten by the
      * implementation.
      */
     bool shouldWriteRestartFile() const
     {
//         return false;
         return this->timeManager().episodeIsOver();
     }
    void preTimeStep()
    {
//        int episodeIdx = this->timeManager().episodeIndex();
//      Scalar InjProcess = Injection(episodeIdx);
//
//      if (InjProcess != -99)
//      if (this->timeManager().episodeIndex() > 3 )
        if (this->timeManager().timeStepSize()>50) //Anozie hat auf 210s = Tepisodemin/2 begrenzt!
        {
            this->timeManager().setTimeStepSize(50);
//          this->newtonController().setRelTolerance(1e-4);
        }
    }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    { return "Biomineralisation"; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature(const Element &element,
                       const FVElementGeometry &fvGeometry,
                       int scvIdx) const
    {
        return 273.15 + 25; // -> 25 deg C
    };
#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar zmax = this->bBoxMax()[dim - 1];

//        Scalar xmax = this->bBoxMax()[0];
//        Scalar xmin = this->bBoxMin()[0];
//        Scalar ymax = this->bBoxMax()[1];
//        Scalar ymin = this->bBoxMin()[1];
//
//        std::cout << "x max, min = " << xmax <<", " << xmin << "y max, min = " << ymax <<", " << ymin << std::endl;

        Scalar rmax = this->bBoxMax()[0]; //0.381;//this->bBoxMax()[0];
        Scalar rmin = 0.0254;

//        values.setAllDirichlet();
        values.setAllNeumann();

        if(globalPos[dim-1] > zmax - eps_)
        {
            values.setAllDirichlet();
////            for (int i=switchIdx; i< numEq - numSPhases; ++i)
////            {
//////              values.setOutflow(i);
////                values.setDirichlet(i);
////            }
        }
        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1]) > rmax - eps_)
        {
            values.setAllDirichlet();
        }

//        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1])<= rmin + 0.01 && globalPos[dim-1] <= hInj_)
//        {
//          values.setAllDirichlet();
//        }
//        if(globalPos[1] <  eps_)
//           values.setAllDirichlet();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

//      Scalar zmax = this->bBoxMax()[dim - 1];
//      Scalar rmax = 0.381;  //this->bBoxMax()[dim - 2];
        Scalar rmin = 0.0254;
        initial_(values, globalPos);

//      if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1])<= rmin + 0.01 && globalPos[dim-1] <= hInj_ )
//      {
//          values[pressureIdx] =  2e5;//(std::min(std::max(1e5,100*this->timeManager().time()+9e4), 2e5)); // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
//////            values[switchIdx] = initxlTC_;
//            values[xlNaIdx] = initxlNa_ + 2*xlNacorr_;
//            values[xlClIdx] = initxlCl_ + initxlTNH_ + 2*initxlCa_ + xlClcorr_ + xlNacorr_;
////            values[xlCaIdx] += 4*initxlCa_;
//////            values[xlUreaIdx] = initxlUrea_;
//////            values[xlTNHIdx] = initxlTNH_;
//////            values[xlO2Idx] = initxlO2_;
//////            values[xlBiosubIdx] = initxlBiosub_;
////            values[xlBiosuspIdx] = (std::min(std::max(0.0,1e-2*this->timeManager().time()-2e-2), 0.0001));
//////            values[phiBiofilmIdx] = 0.000; // [m^3/m^3]
//////            values[phiCalciteIdx] = 0.00000; // [m^3/m^3]
//      }

//      else
//      initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      int scvIdx,
                      int boundaryFaceIdx) const

    {
        Scalar rmin = 0.0254;
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1])<= rmin + 0.01 && globalPos[dim-1] <= hInj_)
        {

            int episodeIdx = this->timeManager().episodeIndex();
            int InjProcess = Injection(episodeIdx);
            //          Scalar influx = -(std::min(std::max(0.0,0.00001*this->timeManager().time()), 0.05));
                        //inflow 20ml/min = 1/3e-6m³/s
                        //Assumption: only flow into the fracture as it is highly conductive, the domain contains 1/4 of the fracture
                        Scalar waterflux = 0.25*0.3333333333333333e-6;//[m³/s]
                        // This mass flux is injected over a height of hJnj_ in a 40°degree slice
                        waterflux /= rmin * 3.14 * 40/180 * hInj_;  //[m/s]

     //basic rinse injection (InjProcess == -1 )
        values[conti0EqIdx + wCompIdx] = -waterflux * 996/FluidSystem::molarMass(wCompIdx);
        values[conti0EqIdx + nCompIdx] = -waterflux * injTC_*996 /FluidSystem::molarMass(nCompIdx);
        values[conti0EqIdx + xlCaIdx] = 0;
        values[conti0EqIdx + xlBiosuspIdx] = 0;
        values[conti0EqIdx + xlBiosubIdx] = -waterflux * injSub_ /FluidSystem::molarMass(xlBiosubIdx);
        values[conti0EqIdx + xlO2Idx] = -waterflux * injO2_ /FluidSystem::molarMass(O2Idx);
        values[conti0EqIdx + xlUreaIdx] = 0;
        values[conti0EqIdx + xlTNHIdx] = -waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx);
        values[conti0EqIdx + phiCalciteIdx] = 0;
        values[conti0EqIdx + phiBiofilmIdx] = 0;
        values[conti0EqIdx + xlNaIdx] = -waterflux * injNa_ /FluidSystem::molarMass(NaIdx);
        values[conti0EqIdx + xlClIdx] = -waterflux *injTNH_ /FluidSystem::molarMass(TNHIdx);        //NH4Cl --->  mol Cl = mol NH4

        if (InjProcess == -1)   // rinse, used as standard injection fluid
        {
            //          do not change anything.
        }

        else if (InjProcess == -99) // no injection
        {
            values = 0.0; //mol/m²/s
        }

        else if (InjProcess == 1)       //ca-rich injection: ca and urea injected additionally to rinse-fluid, Na (pH) and Cl are also different(CaCl2)
        {
            values[conti0EqIdx + wCompIdx] = - waterflux * 0.8716 * densityW_ /FluidSystem::molarMass(wCompIdx);    //TODO 0.8716 check factor!!!
            values[conti0EqIdx + nCompIdx] = - waterflux * injTC_ * densityW_ /FluidSystem::molarMass(nCompIdx);
            values[conti0EqIdx + xlCaIdx] = - waterflux * injCa_/FluidSystem::molarMass(CaIdx);
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
            values[conti0EqIdx + xlNaIdx] = - waterflux * injNa_ /FluidSystem::molarMass(NaIdx) * 0.032;
            values[conti0EqIdx + xlClIdx] = - waterflux * injTNH_ /FluidSystem::molarMass(TNHIdx)               //NH4Cl --->  mol Cl = mol NH4
                                            - waterflux * 2 * injCa_/FluidSystem::molarMass(CaIdx);             //+CaCl2 --->  mol Cl = mol Ca*2
        }

        else if (InjProcess == 0 || InjProcess == 3 )   //urea-injections: urea is injected additionally to rinse-fluid
        {
            values[conti0EqIdx + xlUreaIdx] = - waterflux * injUrea_ /FluidSystem::molarMass(UreaIdx);
        }

        else if(InjProcess == 2)        //inoculation: same as rinse, but with bacteria
        {
            values[conti0EqIdx + xlBiosuspIdx] = -waterflux * injBiosusp_ /FluidSystem::molarMass(xlBiosuspIdx);
        }
    }
    else
        values = 0.0; //mol/m²/s
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &q,
              const Element &element,
              const FVElementGeometry &fvGeometry,
                int scvIdx) const
    {
        q = 0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void reactionSource(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                const VolumeVariables &volVars,
                const Scalar absgradpw) const
    {
                q = 0;
        Scalar dt = this->timeManager().timeStepSize();
        Chemistry chemistry;
        chemistry.reactionSource(q,
                        volVars,
                        absgradpw,
                        dt);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
//      return bothPhases;
    }


    void postTimeStep()
    {
        // Calculate masses and flux over a layer
        //PrimaryVariables mass;
//        PrimaryVariables massGas(0.0);
//        PrimaryVariables massH2O(0.0);
//        PrimaryVariables massBiofilm(0.0);
//        PrimaryVariables massCalcite(0.0);

        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();

//        this->model().globalPhaseStorage(massH2O, wPhaseIdx);
//        this->model().globalPhaseStorage(massGas, nPhaseIdx);
//        this->model().globalPhaseStorage(massBiofilm, bPhaseIdx);
//        this->model().globalPhaseStorage(massCalcite, cPhaseIdx);
//
        if (this->gridView().comm().rank() == 0)
        {
//          std::cout<<"==============================================******==================================================="<<std::endl;
//          std::cout<< " MassH2O_l:   "; std::cout.width(11); std::cout<< massH2O[wCompIdx] <<" kg, "
//                     << " MassCO2_l:   "; std::cout.width(11); std::cout<< massH2O[nCompIdx] <<" kg, "
//                     << " MassBio_l:    "; std::cout.width(11); std::cout<< massH2O[BiosuspIdx] <<" kg"
//                     << " MassCa_l:    "; std::cout.width(11); std::cout<< massH2O[CaIdx] <<" kg"
//                     << " MassSub_l:    "; std::cout.width(11); std::cout<< massH2O[BiosubIdx] <<" kg"
//                     << " MassO2_l:    "; std::cout.width(11); std::cout<< massH2O[O2Idx] <<" kg"
//                     << " MassN_Urea_l:    "; std::cout.width(11); std::cout<< massH2O[UreaIdx]*2*14e-3/FluidSystem::molarMass(UreaIdx) <<" kg"
//                     << " MassN_TNH_l:    "; std::cout.width(11); std::cout<< massH2O[TNHIdx]*14e-3/FluidSystem::molarMass(TNHIdx) <<" kg"
//                     <<std::endl;
//
//            std::cout<< " MassH2O_g:   "; std::cout.width(11); std::cout << massGas[wCompIdx] << " kg, "
//                     << " MassCO2_g:   "; std::cout.width(11); std::cout << massGas[nCompIdx] << " kg, "
//                     << " MassBio_g:    "; std::cout.width(11); std::cout << massGas[BiosuspIdx] << " kg"
//                     << " MassCa_g:    "; std::cout.width(11); std::cout << massGas[CaIdx] << " kg"
//                     <<std::endl;
//
//            std::cout
//                     << " MassH2O_b:   "; std::cout.width(11); std::cout << massBiofilm[wCompIdx] << " kg, "
//                   << " MassCO2_b:   "; std::cout.width(11); std::cout << massBiofilm[nCompIdx] << " kg, "
//                   << " MassBio_b:   "; std::cout.width(11); std::cout << massBiofilm[phiBiofilmIdx] << " kg"
//                   << " MassCa_b:   "; std::cout.width(11); std::cout << massBiofilm[phiCalciteIdx] << " kg"
//                   <<std::endl;
////
//            std::cout<< " MassH2O_c:   "; std::cout.width(11); std::cout << massCalcite[wCompIdx] << " kg, "
//                   << " MassCO2_c:   "; std::cout.width(11); std::cout << massCalcite[nCompIdx]*FluidSystem::molarMass(nCompIdx)/FluidSystem::molarMass(CalciteIdx) << " kg, "
//                   << " MassBio_c:   "; std::cout.width(11); std::cout << massCalcite[phiBiofilmIdx] << " kg"
//                   << " MassCa_c:   "; std::cout.width(11); std::cout << massCalcite[phiCalciteIdx]*FluidSystem::molarMass(CaIdx)/FluidSystem::molarMass(CalciteIdx) << " kg"
//                   <<std::endl;
//
//            std::cout<< " MassH2O_Tot: "; std::cout.width(11); std::cout << massH2O[wCompIdx] + massGas[wCompIdx]<< " kg, "
//                     << " MassCO2_Tot: "; std::cout.width(11); std::cout << massH2O[nCompIdx] + massGas[nCompIdx]<< " kg, "
//                     << " MassBio_Tot:  "; std::cout.width(11); std::cout << massH2O[BiosuspIdx] +  massGas[BiosuspIdx] + massBiofilm[phiBiofilmIdx] << " kg" // + massCalcite[BiofilmIdx]<< " kg"
//                     << " MassCa_Tot:  "; std::cout.width(11); std::cout << massH2O[CaIdx] +  massGas[CaIdx] + massCalcite[phiCalciteIdx]*FluidSystem::molarMass(CaIdx)/FluidSystem::molarMass(CalciteIdx)<< " kg"
//                     << " MassN_Totl:    "; std::cout.width(11); std::cout<< massH2O[TNHIdx]*14e-3/FluidSystem::molarMass(TNHIdx) + massH2O[UreaIdx]*2*14e-3/FluidSystem::molarMass(UreaIdx)<<" kg"
//                     <<std::endl;

           std::cout<< " Time: "<<time+dt<<std::endl;
           std::cout<<"==============================================******==================================================="<<std::endl;
       }
    }

    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        //if(this->timeManager().episodeIndex() ==1 )


     int episodeIdx = this->timeManager().episodeIndex();
     Scalar tEpisode= EpisodeEnd(episodeIdx + 1)-EpisodeEnd(episodeIdx);

        this->timeManager().startNextEpisode(tEpisode);
//            this->timeManager().setTimeStepSize(tEpisode / 100);
//        if (Injection(episodeIdx + 1) == -1)
//              this->timeManager().setTimeStepSize(0.01);
//        else if (Injection(episodeIdx + 1) == 0 || Injection(episodeIdx + 1) == 3)
                this->timeManager().setTimeStepSize(0.1);
//        else
//          this->timeManager().setTimeStepSize(10);

        std::cout<< "\n  episode number  " << episodeIdx << " done, starting next episode number  " <<episodeIdx + 1 << ", Injection scheme is : "<<  Injection(episodeIdx + 1) << "\n"  <<std::endl;

    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
//      Scalar zmax = this->bBoxMax()[1];
        values[pressureIdx] =  1e5; // - (maxHeight - globalPos[1])*densityW_*9.81; //p_atm + rho*g*h
        values[switchIdx] = initxlTC_;
        values[xlNaIdx] = initxlNa_ + xlNacorr_;
        values[xlClIdx] = initxlCl_ + initxlTNH_ + 2*initxlCa_ + xlClcorr_;
        values[xlCaIdx] = initxlCa_;
        values[xlUreaIdx] = initxlUrea_;
        values[xlTNHIdx] = initxlTNH_;
        values[xlO2Idx] = initxlO2_;
        values[xlBiosubIdx] = initxlBiosub_;
        values[xlBiosuspIdx] = initxlBiosusp_;
        values[phiBiofilmIdx] = initPhiBiofilm_; // [m^3/m^3]
        values[phiCalciteIdx] = initPhiCalcite_; // [m^3/m^3]

#if !ISOTHERMAL
        values[temperatureIdx] = 283.0 + (depthBOR_ - globalPos[1])*0.03;
#endif

    }

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XlNaCl the XlNaCl [kg NaCl / kg solution]
     */
    static Scalar massTomoleFrac_(Scalar XlNaCl)
    {
       const Scalar Mw = FluidSystem::molarMass(wCompIdx);  // 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = FluidSystem::molarMass(NaIdx) + FluidSystem::molarMass(ClIdx); // 58.44e-3; /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XlNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       const Scalar xlNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xlNaCl;
    }
    static const int Injection (int episodeIdx)
    {
        int Inj [111]=
        {-99,                                                           //0
        2,
        1, -99, 1,  -99, 1, -99
        };
////                1,  -99,    3,  -99,    -1, 1,   0, -99,    -1, 1,   0, -99,    //TEST!!!!!!
//      2,  -99,    3,  -99,    -1, 1,   0, -99,    -1, 1,   0, -99,    //12
//      -1, 1,      0,  -99,    -1, 1,   0, -99,    3,  -99, 3, -99,    //24
//      3,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //36
//      0,  -99,    -1, 1,      0,  -99, 3, -99,    3,  -99, -1,  1,    //48
//      0,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //60
//      0,  -99,    -1, 1,      0,  -99,  3,-99,    3,  -99, -1,  1,    //72
//      0,  -99,    -1, 1,      0,  -99, -1,  1,    0,  -99, -1,  1,    //84
//      0,  -99,    -1, 1,      0,  -99,  3, -99,   3,  -99, -1,  1,    //96
//      0,  -99,    -1, 1,      0,  -99, -1,   1,   0,  -99, -1,  1,    //108
//      0,  -99};                                                       //110

    return Inj[episodeIdx];
    }
    static const Scalar EpisodeEnd (int episodeIdx)
    {
        //End times of the episodes in minutes.
         Scalar EpiEnd[111] = {0,   //10e20};                                                                           //0
            1, 5, 5+18, 23+5, 28+18, 46+5, 10e20
         };
//               15,    420,    1530,   2730,   2765,   2795,   2801,   3960,   3997,   4027,   4033,   5550,   //12
//               5587,  5617,   5623,   6941,   6978,   7008,   7014,   8366,   8396,   9836,   9866,   11036,  //24
//               11066, 12746,  12783,  12813,  12819,  15626,  15661,  15691,  15697,  17086,  17124,  17154,  //36
//               17160, 18506,  18538,  18568,  18574,  19853,  19883,  21248,  21278,  22868,  22903,  22933,  //48
//               22939, 24368,  24405,  24435,  24441,  25808,  25843,  25873,  25879,  27188,  27225,  27255,  //60
//               27261, 28628,  28663,  28693,  28699,  29998,  30028,  31568,  31598,  32978,  33013,  33043,  //72
//               33049, 34373,  34410,  34440,  34446,  35873,  35908,  35938,  35944,  37373,  37410,  37440,  //84
//               37446, 38873,  38908,  38938,  38944,  40005,  40035,  41610,  41640,  43290,  43327,  43357,  //96
//               43363, 44702,  44737,  44767,  44773,  46104,  46139,  46169,  46175,  47339,  47374,  47404,  //108
//               47410, 48850};                                                                                 //110

    return 60 * EpiEnd[episodeIdx];
    }
    static constexpr Scalar hInj_ = 0.0508; //0.1016; //[m]
    static constexpr Scalar eps_ = 1e-6;
//    static constexpr Scalar densityg_ = 1.20; //kg/m3
    static constexpr Scalar densityW_ = 1087; // rhow=1087;
//    static constexpr Scalar gasSaturation_ = 1e-1;
//    static constexpr Scalar liquidSaturation_ = (1 - gasSaturation_);

    static constexpr Scalar initxlTC_ = 2.3864e-7;      // [mol/mol]
    static constexpr Scalar initxlNa_ = 0.0;
    static constexpr Scalar initxlCl_ = 0.0;
    static constexpr Scalar initxlCa_ = 0.0;//1e-8; //0.0;//1e-20;
    static constexpr Scalar initxlUrea_ = 0.0;//1e-10;
    static constexpr Scalar initxlTNH_ = 0.0;// 3.341641e-3;
    static constexpr Scalar initxlO2_ = 0.0;//4.4686e-6;
    static constexpr Scalar initxlBiosub_ = 0.0;//2.97638e-4;
    static constexpr Scalar initxlBiosusp_ = 0.0;//1e-20;
    static constexpr Scalar xlNacorr_ = 0.0;//2.9466e-6;
    static constexpr Scalar xlClcorr_ = 0.0;
    static constexpr Scalar initPhiBiofilm_ = 0.0;//1e-20;
    static constexpr Scalar initPhiCalcite_ = 0.0;//1e-20;//0.0;

    static constexpr Scalar injTC_ = 5.8e-7;                // [kg/kg]
    static constexpr Scalar injNa_ = 0.0;//0.00379;             // [kg/m³] static constexpr Scalar Nacorr = 3.79e-3; //[g/l]bzw[kg/m³]
    static constexpr Scalar injCa_ = 0.0;//13.3593333333;           // [kg/m³]      //computed from 0.333mol/l CaCl2
    static constexpr Scalar injUrea_ = 0.0;//20;                    // [kg/m³]
    static constexpr Scalar injTNH_ = 0.0;//3.183840574;//3.184;                // [kg/m³]      //computed from 10 g/l NH4Cl
    static constexpr Scalar injO2_ = 0.008;                 // [kg/m³]
    static constexpr Scalar injSub_ = 3;                    // [kg/m³]
    static constexpr Scalar injBiosusp_ = 0.0;//5.75e-3;            // [kg/m³]      // 2.3e7 cfu/ml (Phillips et al 2012) C_bio_inj calculated with: (column1 exp. 4e9 cfu/ml and C_bio 1 g/l)

};
} //end namespace

#endif
