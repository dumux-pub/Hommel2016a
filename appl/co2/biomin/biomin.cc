//#include "config.h"
/* A calcite-co2-only problem setup without the bio-components*/
//#include "calciteproblem.hh"

/*A general biomineralization problem, corresponding to the column #4 from Ebigbo et al. 2012*/
// #include "biominproblem.hh"

/* A 3D radial problem setup with biomineralization in a fractured 80 cm diameter sandstone core*/
//#include "fraccoreproblem.hh"

/* Pseudo 1D column setups.
* The old numbered do not use injection files,
* the new columnproblemgeneral uses injection files and may be used for all column simulations.
* The new columnproblemgeneral is used in Hommel et al. 2015a (WRR, model improvement)
* and Hommel et al. 2015b (TiPM, initial biomass and injection investigation)
* The initialbiofilmcolumnproblem assumes an initial distribution of biofilm
* and neglects the component suspended biomass.
* The initialbiofilmcolumnproblem is used in Hommel et al. 2015b (TiPM, initial biomass and injection investigation)
*/
//OLD:
//#include "column1biominproblem.hh"
//#include "column2biominproblem.hh"
//#include "column3biominproblem.hh"
//#include "column4biominproblem.hh"
//NEW:
//#include "columnproblemgeneral.hh"
//#include "initialbiofilmcolumnproblem.hh"

/* Setup for the high-pressure sandstone-core experiments from Hommel et al. 2013 (Philipps 2014)*/
//#include "hpcoreproblem.hh"

/* 2D radial setup corresponding to the bike rim reactor (Philipps 2014) used in
* Hommel et al. 2015a (WRR, model improvement) and for the efficiency study.
* 2dradialproblem is the general case using the full complexity model,
* the others are special problem setups.
* 2dradialproblemheterogen uses a random permeability field,
* 2dradialproblemsimplechemistry a simplified chemistry with the main feature r_prec=r_urea
* 2dradialinitialbiofilmproblem assumes pre-established biofilm neglecting the component suspended biomass as in the initialbiofilmcolumnproblem
*/
//#include "2dradialproblem.hh"
#include "2dradialproblemheterogen.hh"
// #include "2dradialproblemsimplechemistry.hh"
//  #include "2dradialinitialbiofilmproblem.hh"

/* Setups for the different injection scenarios for the field test at Gorgas.*/
//#include "gorgasproblem.hh"
//#include "gorgasproblem_2_28.hh"
//#include "gorgasproblem_real.hh"

//#include "gorgascasing2dproblem.hh"
//#include "gorgascasing3dproblem.hh"

#include <dune/grid/io/file/dgfparser.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <iostream>

#include <dumux/common/start.hh>

void usage(const char *progName, const std::string &iDontKnowWhatThisIsGoodFor)
{
}

/*!
 * \brief Print a usage string for simulations.
 *
 * \param progname The name of the executable
 */
void printUsage(const char *progName)
{
    std::cout << "usage: " << progName
            << " [--restart restartTime] -parameterFile biomin.input\n";
    exit(1);
}

template <class TypeTag>
int start_(int argc,
           char **argv)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
//    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
//    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
//    typedef Dune::GridPtr<Grid> GridPointer;

    ////////////////////////////////////////////////////////////
    // Load the input parameters
    ////////////////////////////////////////////////////////////

    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    Dune::ParameterTreeParser::readOptions(argc, argv, ParameterTree::tree());

    if (ParameterTree::tree().hasKey("parameterFile") or argc==1)
    {
        // read input file, but do not overwrite options specified
        // on the command line, since the latter have precedence.
        std::string inputFileName ;
        if(argc==1) // if there are no arguments given (and there is a file ./<programname>.input) we use it as input file
        {
            std::cout<< "\nNo parameter file given. \n"
                     << "Defaulting to '"
                     << argv[0]
                     << ".input' for input file.\n";
            inputFileName = argv[0];
            inputFileName += ".input";
        }
        else
            inputFileName = GET_RUNTIME_PARAM(TypeTag, std::string, parameterFile); // otherwise we read from the command line

        std::ifstream parameterFile;

        // check whether the parameter file exists.
        parameterFile.open(inputFileName.c_str());
        if (not parameterFile.is_open()){
            std::cout<< "\n\t -> Could not open file"
                     << inputFileName
                     << ". <- \n\n\n\n";
            printUsage(argv[0]);
            return 1;
        }
        parameterFile.close();

        Dune::ParameterTreeParser::readINITree(inputFileName,
                                               ParameterTree::tree(),
                                               /*overwrite=*/false);
    }

    // deal with the restart stuff
    int argIdx = 1;
    bool restart = false;
    double tStart = 0.0;
    if (argc > 1 && std::string("--restart") == argv[argIdx])
    {
        restart = true;
        ++argIdx;

        std::istringstream(argv[argIdx++]) >> tStart;
    }

    std::string dgfFileName;
    Scalar dt, tEnd;

    try
    {
        dgfFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
        dt = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInit);
        tEnd = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, TEnd);
    }
    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
    catch (...) {
        std::cerr << "Unknown exception thrown!\n";
        exit(1);
    }
//    std::cout << "Starting with timestep size = " << dt << "s, simulation end = " << tEnd << "s\n";
//
//    // Instantiate the time manager
//    TimeManager timeManager;
//
//    Dumux::Parameters::print<TypeTag>();
//
////    // run the simulation
////    timeManager.init(problem,
////                     tStart, // initial time
////                     dt, // initial time step
////                     tEnd, // final time
////                     restart);
//
//    // print all properties
//    Dumux::Properties::print<TypeTag>();
//
//    timeManager.run();

    return 0;
}

/*!
 * \ingroup Start
 *
 * \brief Provides a main function which reads in parameters from the
 *        command line and a parameter file.
 *
 *        In this function only the differentiation between debugger
 *        or not is made.
 *
 * \tparam TypeTag  The type tag of the problem which needs to be solved
 *
 * \param argc  The number of command line arguments of the program
 * \param argv  The contents of the command line arguments of the program
 * \param usage Callback function for printing the usage message
 */
template <class TypeTag>
int start(int argc,
          char **argv)
{
    try {
        return start_<TypeTag>(argc, argv);
    }
    catch (Dumux::ParameterException &e)
    {
       std::cerr << e << ". Abort!\n";
       printUsage(argv[0]);
       return 1;
    }
    catch (Dune::Exception &e) {
        std::cerr << "Dune reported error: " << e << std::endl;
        return 2;
    }
    catch (...) {
        std::cerr << "Unknown exception thrown!\n";
        return 3;
    }
}

int main(int argc, char** argv)
{
//  typedef TTAG(CalciteProblem) ProblemTypeTag;
    typedef TTAG(BioMinProblem) ProblemTypeTag;
//  typedef TTAG(GorgasProblem) ProblemTypeTag;
    return Dumux::start<ProblemTypeTag>(argc, argv, usage);//printUsage
}

