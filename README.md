Summary
=======
Readme for the dumux-pub module of the Dissertation of Johannes Hommel


Bibtex entry:
@PHDTHESIS{Hommel2016diss,
  author = {Johannes Hommel},
  title = {{Modeling biogeochemical and mass transport processes in the subsurface:
Investigation of microbially induced calcite precipitation}},
  school = {{University of Stuttgart}},
  year = {2016},
  month = {February},
  owner = {hommel},
  timestamp = {2016.03.11}
}

Installation
============

The easiest way to install this module is to create a new folder and to execute the file 
[installHommel2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2016a/raw/master/installHommel2016a.sh) 
in this folder.

```bash
mkdir -p Hommel2016a && cd Hommel2016a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2016a/raw/master/installHommel2016a.sh
sh ./installHommel2016a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/biomin,    # all investigations except the sequential model
  test/modelcoupling/ncchem2pnctransp/, # the sequential model

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/biomin/biomin.cc,
  appl/co2/biomin/test_biomin.cc,

  test/modelcoupling/ncchem2pnctransp/test_ncchem2ptransp.cc,
  test/modelcoupling/ncchem2pnctransp/test_sequential_biomin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.

- DUNE 2.4 (common, grid, istl, geometry, localfunctions)
- dumux-stable 2.8
- external/ug 3.12.1

The folder Results contains the reference solutions and the input files used to 
create them ordered by Chapters/Sections where they are used in the Dissertation:

Results/efficiency_simplified_sequential 
--> used for chapter 6: Perspectives for efficient solution strategies

Results/Field-scale_Gorgas
--> used for Section 5.2: Application of the model on the field scale
--> See also 
[dumux-pub/Shigorina2014a](https://git.iws.uni-stuttgart.de/dumux-pub/Shigorina2014a/raw/master/installShigorina2014a.sh) 

Results/initial_biomass-vs-injection
--> used for Section 5.1: investigation of the influence of initial biomass and injection strategy
--> See also 
[dumux-pub/Hommel2015a](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2015a/raw/master/installHommel2015a.sh)

Results/Porosity-permeability-hp-core
--> used for Section 4.1:Improvement of the porosity-permeability relation

Results/ureolysis
--> used for Section 4.2:Improvement of the ureolysis rate equation
--> See also 
[dumux-pub/Hommel2014a](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2014a/raw/master/installHommel2014a.sh)
