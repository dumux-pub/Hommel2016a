#!/bin/bash

# download Dune core modules 
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
cd ..


### DUMUX
git clone -b releases/2.8 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2016a.git Hommel2016a

# External software: 
cp Hommel2016a/installExternals.sh .
chmod u+x installExternals.sh
./installExternals.sh ug

### run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/UGDIR=.*/UGDIR=$EXTPATH\/external/" Hommel2016a/optim.opts > optim_used.opts
./dune-common/bin/dunecontrol --opts=optim_used.opts all
