
install(FILES
        biocarbonicacid.hh
        biocarbonicacidnoattachment.hh
        biocarbonicacidnosuspendedbiomass.hh
        biocarbonicacidrprecequalsrurea.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/chemistry/biogeochemistry)
